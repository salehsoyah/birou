import 'dart:async';

import 'package:birou/views/auth/login.dart';
import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:adobe_xd/page_link.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override

  setSplashScreen() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setBool("splashScreen", true);
  }

  @override
  void initState() {
    setSplashScreen().then((value){
      print(value);
    });

    Timer(
        Duration(seconds: 3),
            () =>
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => Login())));
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Pinned.fromPins(
            Pin(start: 0.0, end: 0.0),
            Pin(start: 0.0, end: 0.0),
            child: Container(
              decoration: BoxDecoration(
                color: const Color(0xff1a8dff),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 119.1, start: 0.0),
            Pin(size: 320.3, end: 0.0),
            child: SvgPicture.string(
              _svg_wqm6d,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(start: 73.0, end: 0.0),
            Pin(start: 0.0, end: 0.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.Fade,
                  ease: Curves.easeOut,
                  duration: 0.3,
                ),
              ],
              child: SvgPicture.string(
                _svg_lqjpnf,
                allowDrawingOutsideViewBox: true,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 243.1, start: -170.5),
            Pin(size: 317.1, end: -76.6),
            child: SvgPicture.string(
              _svg_klqtp,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(start: 73.0, end: 73.0),
            Pin(size: 229.0, middle: 0.5009),
            child:
            // Adobe XD layer: 'logo-social-app-whi…' (shape)
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage('assets/images/logo.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 47.0, middle: 0.6341),
            Pin(size: 17.0, middle: 0.6201),
            child: Text(
              'Mobile',
              style: TextStyle(
                fontFamily: 'Helvetica Neue',
                color: const Color(0xbaffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 123.0, middle: 0.5198),
            Pin(size: 14.0, end: 14.0),
            child: Text(
              '© Birou Suite - Birou.tn',
              style: TextStyle(
                fontFamily: 'Helvetica Neue',
                fontSize: 12,
                color: const Color(0xbaffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}


const String _svg_wqm6d =
    '<svg viewBox="0.0 491.7 119.1 320.3" ><path transform="translate(-73.0, 491.66)" d="M 73.0137939453125 104.0406494140625 L 192.1479644775391 0 L 192.1479644775391 320.3356018066406 L 73.0137939453125 320.3356018066406 L 73.0137939453125 104.0406494140625 Z" fill="#2383e2" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_lqjpnf =
    '<svg viewBox="73.0 0.0 302.0 812.0" ><path  d="M 73.0137939453125 263.7265625 L 375 0 L 375 812 L 73.0137939453125 812 L 73.0137939453125 263.7265625 Z" fill="#0e78e0" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_klqtp =
    '<svg viewBox="-170.5 571.5 243.1 317.1" ><path transform="translate(-243.53, 571.45)" d="M 73.0137939453125 212.2577514648438 L 316.064453125 0 L 316.064453125 123.9847030639648 L 73.0137939453125 317.1463623046875 L 73.0137939453125 212.2577514648438 Z" fill="#1a8dff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';

import 'dart:convert';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

registration(String email, String password, String name) async {
  var queryParameters = {
    'email': '$email',
    'password': '$password',
    'name': '$name',
  };

  Response response =
      await http.post(Uri.parse('https://birou.tn/fr/api/private/user/register'),
          // Send authorization headers to the backend.
          headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
          body: queryParameters);

  print(response.body);

  return jsonDecode(response.body);
}

social_registration_login(
    String provider,
    String email,
    String name,
    String uuid,
    String avatar,
    String platform,
    int os_version,
    String model,
    String mobile_token) async {

  var queryParameters = {
    'email': '$email',
    'name': '$name',
    'uuid': '$uuid',
    'avatar': '$avatar',
    'platform': '$platform',
    'os_version': '$os_version',
    'model': '$model',
    'mobile_token': '$mobile_token',
  };

  Response response = await http.post(
      Uri.parse('https://birou.tn/fr/api/private/user/social/$provider'),
      // Send authorization headers to the backend.
      headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
      body: queryParameters);

  // print(response.body);
  return jsonDecode(response.body);
}

emailVerify(
  int code,
  String email,
) async {
  var queryParameters = {
    'code': '$code',
    'email': '$email',
  };
  Response response =
      await http.post(Uri.parse('https://birou.tn/fr/api/private/user/email/validate'),
          // Send authorization headers to the backend.
          headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
          body: queryParameters);

  print(response.body);
  return jsonDecode(response.body);

}

login(String email, String password, String uuid, String platform,
    int os_version, String model, String mobile_token) async {
  var queryParameters = {
    'email': '$email',
    'password': '$password',
    'uuid': '$uuid',
    'platform': '$platform',
    'os_version': '$os_version',
    'model': '$model',
    'mobile_token': '$mobile_token',
  };

  Response response =
      await http.post(Uri.parse('https://birou.tn/fr/api/private/user/login'),
          // Send authorization headers to the backend.
          headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
          body: queryParameters);

  print(response.body);
  return jsonDecode(response.body);
}

// Future authFacebook() async {
//   FacebookLogin facebookSignIn = new FacebookLogin();
//
//   final FacebookLoginResult result = await facebookSignIn.logIn(['email']);
//
//   switch (result.status) {
//     case FacebookLoginStatus.loggedIn:
//       final FacebookAccessToken accessToken = result.accessToken;
//
//       final graphResponse = await http.get(Uri.parse(
//           'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture&access_token=${accessToken.token}'));
//       final profile = json.decode(graphResponse.body);
//       print('''
//          Logged in!
//
//          Token: ${accessToken.token}
//          User id: ${accessToken.userId}
//          Expires: ${accessToken.expires}
//          Permissions: ${accessToken.permissions}
//          Declined permissions: ${accessToken.declinedPermissions}
//          ''');
//
//       return profile;
//       break;
//     case FacebookLoginStatus.cancelledByUser:
//       print('Login cancelled by the user.');
//       break;
//     case FacebookLoginStatus.error:
//       print('Something went wrong with the login process.\n'
//           'Here\'s the error Facebook gave us: ${result.errorMessage}');
//       break;
//   }
// }

Future authGoogle() async {
  final googleSignIn = GoogleSignIn();
  bool isSigningIn = false;

  final user = await googleSignIn.signIn();

  if (user == null) {
    return isSigningIn;
  } else {
    final googleAuth = await user.authentication;

    final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
    await FirebaseAuth.instance.signInWithCredential(credential);
    final person = FirebaseAuth.instance.currentUser;

    return person;
  }
}

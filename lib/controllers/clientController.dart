import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

newClient(
    String first_name,
    String last_name,
    String company,
    int display_name,
    String delivery_address,
    String delivery_country,
    String delivery_state,
    String delivery_zip,
    String bill_address,
    String bill_country,
    String bill_state,
    String bill_zip,
    String email,
    String fiscal_id,
    int phone,
    String note,
    Uri website,
    String activity_id,
    String currency_id,
    String deadline_id,
    int title,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'first_name': '$first_name',
    'last_name': '$last_name',
    'company': '$company',
    'display_name': '$display_name',
    'delivery_address': '$delivery_address',
    'delivery_country': '$delivery_country',
    'delivery_state': '$delivery_state',
    'delivery_zip': '$delivery_zip',
    'bill_address': '$bill_address',
    'bill_country': '$bill_country',
    'bill_state': '$bill_state',
    'bill_zip': '$bill_zip',
    'email': '$email',
    'fiscal_id': '$fiscal_id',
    'phone': '$phone',
    'note': '$note',
    'website': '$website',
    'activity_id': '$activity_id',
    'currency_id': '$currency_id',
    'title': '$title',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/client/new'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);
        print(jsonDecode(response.body));
  return jsonDecode(response.body);
}

getClient(String companyId, String clientId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/client/$clientId/synthesis'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

editClient(
    String first_name,
    String last_name,
    String company,
    int display_name,
    String delivery_address,
    String delivery_country,
    String delivery_state,
    String delivery_zip,
    String bill_address,
    String bill_country,
    String bill_state,
    String bill_zip,
    String email,
    String fiscal_id,
    int phone,
    String note,
    Uri website,
    String activity_id,
    String currency_id,
    String deadline_id,
    int title,
    String clientId,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'first_name': '$first_name',
    'last_name': '$last_name',
    'company': '$company',
    'display_name': '$display_name',
    'delivery_address': '$delivery_address',
    'delivery_country': '$delivery_country',
    'delivery_state': '$delivery_state',
    'delivery_zip': '$delivery_zip',
    'bill_address': '$bill_address',
    'bill_country': '$bill_country',
    'bill_state': '$bill_state',
    'bill_zip': '$bill_zip',
    'email': '$email',
    'fiscal_id': '$fiscal_id',
    'phone': '$phone',
    'note': '$note',
    'website': '$website',
    'activity_id': '$activity_id',
    'currency_id': '$currency_id',
    'title': '$title',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/client/$clientId/edit'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

deleteClient(String companyId, int clientId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/client/$clientId/delete'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

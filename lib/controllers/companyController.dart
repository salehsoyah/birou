import 'dart:convert';

import 'package:birou/models/activity.dart';
import 'package:birou/models/bank.dart';
import 'package:birou/models/collaborator.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/currency.dart';
import 'package:birou/models/deadline.dart';
import 'package:birou/models/exercice.dart';
import 'package:birou/models/input.dart';
import 'package:birou/models/log.dart';
import 'package:birou/models/method.dart';
import 'package:birou/models/tax.dart';
import 'package:birou/models/unity.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

activitiesList() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
      await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/activities'),
          // Send authorization headers to the backend.
          headers: headers);
  var jsonData = json.decode(response.body);
  print(jsonData);
  List<Activity> activities = [];

  for (var u in jsonData["data"]["activities"]) {
    Activity activity = Activity(u['title'], u['hashed_id']);
    activities.add(activity);
  }

  return activities;
}

exercicesList() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
      await http.get(Uri.parse('https://birou.tn/$lang/api/private/general/accounting'),
          // Send authorization headers to the backend.
          headers: headers);
  var jsonData = json.decode(response.body);

  List<Exercice> exercices = [];

  for (var u in jsonData["data"]["accountingPeriods"]) {
    Exercice exercice = Exercice(u['title'], u['hashed_id']);
    exercices.add(exercice);
  }

  return exercices;
}

currencyList() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
      await http.get(Uri.parse('https://birou.tn/$lang/api/private/general/currencies'),
          // Send authorization headers to the backend.
          headers: headers);
  var jsonData = json.decode(response.body);

  List<Currency> currencies = [];

  for (var u in jsonData["data"]["currencies"]) {
    Currency currency = Currency(u['title'], u['hashed_id']);
    currencies.add(currency);
  }

  return currencies;
}

countryList() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
      await http.get(Uri.parse('https://birou.tn/$lang/api/private/general/countries'),
          // Send authorization headers to the backend.
          headers: headers);
  var jsonData = json.decode(response.body);

  List<Country> countries = [];

  for (var u in jsonData["data"]["countries"]) {
    Country country = Country(u['title'], u['hashed_id']);
    countries.add(country);
  }

  return countries;
}

uploadLogo(String logo) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'logo': '$logo',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
      await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/upload/logo'),
          // Send authorization headers to the backend.
          headers: headers,
          body: queryParameters);

  return jsonDecode(response.body);
}

newCompany(
    String title,
    Uri website,
    String country_id,
    String address,
    String state,
    String zip_code,
    String language,
    String logo,
    String activity_id,
    String fiscal_id,
    int phone,
    String currency_id,
    String accounting_period_id,
    String bank,
    String bs,
    String rib,
    String iban) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'title': '$title',
    'website': '$website',
    'country_id': '$country_id',
    'address': '$address',
    'state': '$state',
    'zip_code': '$zip_code',
    'language': '$language',
    'logo': '$logo',
    'activity_id': '$activity_id',
    'fiscal_id': '$fiscal_id',
    'phone': '$phone',
    'currency_id': '$currency_id',
    'accounting_period_id': '$accounting_period_id',
    'bank': '$bank',
    'bs': '$bs',
    'rib': '$rib',
    'iban': '$iban',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
      await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/new'),
          // Send authorization headers to the backend.
          headers: headers,
          body: queryParameters);

  return jsonDecode(response.body);
}

updateCompany(
    String companyId,
    String title,
    Uri website,
    String country_id,
    String address,
    String state,
    String zip_code,
    String language,
    String logo,
    String activity_id,
    String fiscal_id,
    int phone,
    String currency_id,
    String accounting_period_id,
    String bank,
    String bs,
    String rib,
    String iban) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'title': '$title',
    'website': '$website',
    'country_id': '$country_id',
    'address': '$address',
    'state': '$state',
    'zip_code': '$zip_code',
    'language': '$language',
    'logo': '$logo',
    'activity_id': '$activity_id',
    'fiscal_id': '$fiscal_id',
    'phone': '$phone',
    'currency_id': '$currency_id',
    'accounting_period_id': '$accounting_period_id',
    'bank': '$bank',
    'bs': '$bs',
    'rib': '$rib',
    'iban': '$iban',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/parameters/update'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}


getCompany(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");


  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

userCompanies() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/user/companies/list/'),
      // Send authorization headers to the backend.
      headers: headers);
  var jsonData = json.decode(response.body);

  List<Company> companies = [];

  for (var u in jsonData["data"]["ownedCompanies"]) {
    Company company = Company(u['title'], u['hashed_id'], sharedPreferences.getString("fullName"), u['logo']);
    companies.add(company);
  }

  for (var u in jsonData["data"]["joinedCompanies"]) {
    Company company = Company(u['title'], u['hashed_id'], "", u['logo']);
    companies.add(company);
  }

  return companies;
}


companyDeadlines(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  // String lastVisitedCompanyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/fr/api/private/company/$companyId/deadlines'),
      // Send authorization headers to the backend.
      headers: headers);
  var jsonData = json.decode(response.body);

  List<Deadline> deadlines = [];

  for (var u in jsonData["data"]["invoiceDeadlines"]) {
    Deadline deadline = Deadline(u['title'], u['days'], u['hashed_id']);
    deadlines.add(deadline);
  }

  return deadlines;
}

companyActivities(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/activities'),
      // Send authorization headers to the backend.
      headers: headers);
  var jsonData = json.decode(response.body);

  List<Activity> activities = [];

  for (var u in jsonData["data"]["activities"]) {
    Activity activity = Activity(u['title'], u['hashed_id']);
    activities.add(activity);
  }

  return activities;
}

companyMethods(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/methods'),
      // Send authorization headers to the backend.
      headers: headers);
  var jsonData = json.decode(response.body);

  List<Method> methods = [];

  for (var u in jsonData["data"]["paymentMethods"]) {
    Method method = Method(u['title'], u['default'], u['type'], u['hashed_id']);
    methods.add(method);
  }

  return methods;
}


newMethod(String companyId, String title) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  // String lastVisitedCompanyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;
  var queryParameters = {
    'title': '$title',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/methods/addVoucher'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

editMethod(String companyId, String title, String methodId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  // String lastVisitedCompanyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;
  var queryParameters = {
    'title': '$title',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/methods/editDelivery/$methodId'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

deleteMethod(String companyId, String methodId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  // String lastVisitedCompanyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/methods/delete/$methodId'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}


companyTax() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/tax'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Tax> taxes = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["taxes"]) {
    Tax tax = Tax(u['title'], u['rate'], u['hashed_id'], u['is_special']);
    taxes.add(tax);
  }

  return taxes;
}

newTax(String title, double rate) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;
  var queryParameters = {
    'title': '$title',
    'rate' : '$rate'
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/tax/new'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

editTax(String title, double rate, String taxId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;
  var queryParameters = {
    'title': '$title',
    'rate': '$rate'
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/tax/edit/$taxId'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

deleteTax(String taxId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/tax/delete/$taxId'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}


companyInputs() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/inputs'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Input> inputs = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["inputs"]) {
    Input input = Input(u['title'], u['default_value'], u['type'], u['use'], u['conditional_country_id'], u['conditional_currency_id'], u['hashed_id'], u['condition']);
    inputs.add(input);
  }

  return inputs;
}

newInput(String title, int use, int type,
    double value, int condition, String condition_country, String condition_currency ) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;
  var queryParameters = {
    'title': '$title',
    'use' : '$use',
    'type': '$type',
    'value': '$value',
    'condition': '$condition',
    'condition_country':'$condition_country',
    'condition_currency': '$condition_currency'
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/inputs/new'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

editInput(String title, int use, int type,
    double value, int condition, String condition_country, String condition_currency, String inputId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;
  var queryParameters = {
    'title': '$title',
    'use' : '$use',
    'type': '$type',
    'value': '$value',
    'condition': '$condition',
    'condition_country':'$condition_country',
    'condition_currency': '$condition_currency'
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/inputs/edit/$inputId'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}


deleteInput(String inputId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/inputs/delete/$inputId'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

companyLogs() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/logs'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Log> logs = [];

  var jsonData = json.decode(response.body);
print(jsonData);
  for (var u in jsonData["data"]["logs"]) {
    Log log = Log(u['user_id'], u['module'], u['element'], u['action']);
    logs.add(log);
  }

  return logs;
}

companyCountries() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  // String lastVisitedCompanyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
    await http.get(Uri.parse('https://birou.tn/fr/api/private/general/countries'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Country> countries = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["countries"]) {
    Country country = Country(u['title'], u['hashed_id']);
    countries.add(country);
  }

  return countries;
}

companyCurrencies() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/general/currencies'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Currency> currencies = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["currencies"]) {
    Currency currency = Currency(u['title'], u['hashed_id']);
    currencies.add(currency);
  }

  return currencies;
}

companyUnities(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  // String lastVisitedCompanyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/general/items/unities'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Unity> unities = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["unities"]) {
    Unity unity = Unity(u['title'], u['hashed_id']);
    unities.add(unity);
  }

  return unities;
}

bankList(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/banks'),
      // Send authorization headers to the backend.
      headers: headers);
  var jsonData = json.decode(response.body);

  List<Bank> banks = [];

  for (var u in jsonData["data"]["banks"]) {
    Bank bank = Bank(u['bank'], u['hashed_id']);
    banks.add(bank);
  }

  return banks;
}


companyCollaborators() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/users'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Collaborator> collaborators = [];

  var jsonData = json.decode(response.body);
  for (var u in jsonData["data"]["users"]) {
    Collaborator collaborator = Collaborator(u['hashed_id'], u['name'], u['email'], u['pivot']['role']);
    collaborators.add(collaborator);
  }

  return collaborators;
}


newCollaborator(String name, String email) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  String companyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;
  var queryParameters = {
    'name': '$name',
    'email' : '$email'
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.post(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/users/invite'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}



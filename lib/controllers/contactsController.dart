import 'package:birou/models/client.dart';
import 'package:birou/models/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

 clientsList(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };


  Response response = await http.get(
      Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/client'),
      // Send authorization headers to the backend.
      headers: headers);

  var jsonData = json.decode(response.body);
  print(jsonData);
  List<Clientt> clients = [];

  for (var u in jsonData["data"]["clients"]) {
    Clientt client = Clientt(u['display_name'], u['email'], u['organisation'], u['hashed_id']);
    clients.add(client);
  }

  return clients;

}

 providersList(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  print("token $mobile_token");
  print("id $companyId");

  Response response = await http.get(
      Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/provider'),
      // Send authorization headers to the backend.
      headers: headers);

  var jsonData = json.decode(response.body);

  List<Providerr> providers = [];

  for (var u in jsonData["data"]["providers"]) {
    Providerr provider = Providerr(u['display_name'], u['email'], u['organisation'], u['hashed_id']);
    providers.add(provider);
  }

  return providers;

}

import 'dart:convert';
import 'dart:ffi';

import 'package:birou/models/estimate.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future getEstimates(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");
  // String lastVisitedCompanyId = sharedPreferences.getString("lastVisitedCompanyId");
  // String company = lastVisitedCompanyId == null ? companyId : lastVisitedCompanyId;

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/estimate'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Estimate> estimates = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["estimates"]) {
    Estimate estimate = Estimate(u['date'], u['estimate_number'], u['status'],
        jsonDecode(u['totals'])['total'], u['hashed_id']);
    estimates.add(estimate);
  }

  return estimates;
}

checkEstimateNumber(
    String companyId, int estimateNumber, String currentEstimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/invoice/estimate/check/$estimateNumber/$currentEstimateId'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

markEstimate(String companyId, String estimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/invoice/estimate/$estimateId/mark'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

newEstimate(
    String date,
    String dueDate,
    String delivery_number,
    String conditions,
    String reference,
    String notes,
    double discount,
    int tax_type,
    String client_id,
    int use_conditions,
    int show_stamp,
    int show_billing,
    int show_delivery,
    int show_bank,
    String bank_id,
    int show_conditions,
    int choice,
    int currency_rate,
    String language,
    var items,
    var use_input,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'date': date,
    'dueDate': dueDate,
    'estimate_number': delivery_number,
    'conditions': conditions,
    'reference': reference,
    'notes': notes,
    'discount': discount,
    'tax_type': tax_type,
    'client_id': client_id,
    'use_conditions': use_conditions,
    'show_stamp': show_stamp,
    'show_billing': show_billing,
    'show_delivery': show_delivery,
    'show_bank': show_bank,
    'bank_id': bank_id,
    'show_conditions': show_conditions,
    'choice': choice,
    'show_conditions': show_conditions,
    'choice': choice,
    'currency_rate': currency_rate,
    'language': language,
    'items': items,
    'use_input': use_input,
  };

  Map<String, String> headers = {
    "Content-type": "application/json",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/estimate/new'),
      // Send authorization headers to the backend.
      headers: headers,
      body: jsonEncode(queryParameters));

  return jsonDecode(response.body);
}

nextEstimate(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/estimate/next'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

editEstimate(
    String date,
    String dueDate,
    String delivery_number,
    String conditions,
    String reference,
    String notes,
    double discount,
    int tax_type,
    String client_id,
    int use_conditions,
    int show_stamp,
    int show_billing,
    int show_delivery,
    int show_bank,
    String bank_id,
    int show_conditions,
    int choice,
    int currency_rate,
    String language,
    var items,
    var use_input,
    String estimate_number,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'date': date,
    'due': dueDate,
    'estimate_number': delivery_number,
    'conditions': conditions,
    'reference': reference,
    'notes': notes,
    'discount': discount,
    'tax_type': tax_type,
    'client_id': client_id,
    'use_conditions': use_conditions,
    'show_stamp': show_stamp,
    'show_billing': show_billing,
    'show_delivery': show_delivery,
    'show_bank': show_bank,
    'bank_id': bank_id,
    'show_conditions': show_conditions,
    'choice': choice,
    'currency_rate': currency_rate,
    'language': language,
    'items': items,
    'use_input': use_input,
  };

  Map<String, String> headers = {
    "Content-type": "application/json",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/estimate/$estimate_number/edit'),
      // Send authorization headers to the backend.
      headers: headers,
      body: jsonEncode(queryParameters));

  return jsonDecode(response.body);
}

Future<void> downloadEstimate(String companyId, estimateId, fileName) async {

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> header = {
    "Content-type": "application/pdf",
    "Authorization": "$mobile_token"
  };

  final status = await Permission.storage.request();

  if(status.isGranted){
    final baseStorage = await getExternalStorageDirectory();
    final id = await FlutterDownloader.enqueue(url: 'https://birou.tn/$lang/api/private/company/$companyId/sales/estimate/$estimateId/download',headers: header, savedDir: baseStorage.path, fileName: fileName);
  }else{
    print('No permission');
  }
}

deleteEstimate(String companyId, String estimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/estimate/$estimateId/delete'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

estimateInvoice(String companyId, String estimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/estimate/$estimateId/invoice'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

getEstimate(String companyId, String estimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/estimate/$estimateId/synthesis'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

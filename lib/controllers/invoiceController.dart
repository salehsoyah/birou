import 'dart:convert';
import 'dart:ffi';

import 'package:birou/models/delivery.dart';
import 'package:birou/models/invoice.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

Future getInvoices(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/invoice'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Invoice> invoices = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["invoices"]) {
    Invoice invoice = Invoice(u['date'], u['invoice_number'], u['status'],
        (jsonDecode(u['totals'])['total']).toString(), u['hashed_id']);
    invoices.add(invoice);
  }

  return invoices;
}

newInvoice(
    String date,
    String due,
    String invoice_number,
    String notes,
    String conditions,
    String reference,
    double discount,
    int tax_type,
    String client_id,
    int use_conditions,
    int show_stamp,
    int show_billing,
    int show_delivery,
    int show_bank,
    String bank_id,
    int show_conditions,
    int choice,
    int currency_rate,
    String language,
    var items,
    var use_input,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'date': date,
    'due': due,
    'invoice_number': invoice_number,
    'conditions': conditions,
    'reference': reference,
    'discount': discount,
    'tax_type': tax_type,
    'client_id': client_id,
    'use_conditions': use_conditions,
    'show_stamp': show_stamp,
    'show_billing': show_billing,
    'show_delivery': show_delivery,
    'show_bank': show_bank,
    'bank_id': bank_id,
    'show_conditions': show_conditions,
    'choice': choice,
    'show_conditions': show_conditions,
    'choice': choice,
    'currency_rate': currency_rate,
    'language': language,
    'items': items,
    'use_input': use_input,
  };

  Map<String, String> headers = {
    "Content-type": "application/json",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/invoice/new'),
      // Send authorization headers to the backend.
      headers: headers,
      body: jsonEncode(queryParameters));

  return jsonDecode(response.body);
}

nextInvoice(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/invoice/next'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

Future<void> downloadInvoice(String companyId, invoiceId, fileName) async {

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> header = {
    "Content-type": "application/pdf",
    "Authorization": "$mobile_token"
  };

  final status = await Permission.storage.request();

  if(status.isGranted){
    final baseStorage = await getExternalStorageDirectory();
    final id = await FlutterDownloader.enqueue(url: 'https://birou.tn/$lang/api/private/company/$companyId/sales/invoice/$invoiceId/download',headers: header, savedDir: baseStorage.path, fileName: fileName);
  }else{
    print('No permission');
  }
}


editInvoice(
    String date,
    String dueDate,
    String delivery_number,
    String conditions,
    String reference,
    String notes,
    double discount,
    int tax_type,
    String client_id,
    int use_conditions,
    int show_stamp,
    int show_billing,
    int show_delivery,
    int show_bank,
    String bank_id,
    int show_conditions,
    int choice,
    int currency_rate,
    String language,
    var items,
    var use_input,
    String estimate_number,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'date': date,
    'due': dueDate,
    'invoice_number': delivery_number,
    'conditions': conditions,
    'reference': reference,
    'notes': notes,
    'discount': discount,
    'tax_type': tax_type,
    'client_id': client_id,
    'use_conditions': use_conditions,
    'show_stamp': show_stamp,
    'show_billing': show_billing,
    'show_delivery': show_delivery,
    'show_bank': show_bank,
    'bank_id': bank_id,
    'show_conditions': show_conditions,
    'choice': choice,
    'currency_rate': currency_rate,
    'language': language,
    'items': items,
    'use_input': use_input,
  };

  Map<String, String> headers = {
    "Content-type": "application/json",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/invoice/$estimate_number/edit'),
      // Send authorization headers to the backend.
      headers: headers,
      body: jsonEncode(queryParameters));

  return jsonDecode(response.body);
}


getInvoice(String companyId, String invoiceId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/json",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/invoice/$invoiceId/synthesis'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}


deleteInvoice(String companyId, String invoiceId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/invoice/$invoiceId/delete'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}


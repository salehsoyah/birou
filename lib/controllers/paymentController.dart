import 'dart:convert';
import 'dart:ffi';

import 'package:birou/models/client.dart';
import 'package:birou/models/delivery.dart';
import 'package:birou/models/invoice.dart';
import 'package:birou/models/payment.dart';
import 'package:birou/models/paymentItem.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

getPayments(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/payment'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Payment> payments = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["payments"]) {
    Payment payment = Payment(u['date'], u['payment_number'], u['status'],
        u['total'], u['hashed_id']);
    payments.add(payment);
  }

  return payments;
}

nextPayment(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  // String companyId = sharedPreferences.getString("companyId");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/payment/next'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

clientsPaymentList(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/payment/available'),
      // Send authorization headers to the backend.
      headers: headers);

  var jsonData = json.decode(response.body);
  List<Clientt> clients = [];

  for (var u in jsonData["data"]["clients"]) {
    Clientt client = Clientt(
        u['display_name'], u['email'], u['organisation'], u['hashed_id']);
    clients.add(client);
  }

  return clients;
}

getClientPayments(String companyId, String clientId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/payment/client/$clientId/documents'),
      // Send authorization headers to the backend.
      headers: headers);

  List<PaymentItem> payments = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["invoices"]) {
    PaymentItem payment = PaymentItem(u['hashed_id'], "", 0,
        u['invoice_number'], u['formattedTotal'], u['toPay']);
    payments.add(payment);
  }

  return payments;
}

newPayment(
    String date,
    String payment_number,
    String notes,
    String reference,
    double bank_fee,
    String bank_id,
    String payment_method,
    String client_id,
    int currency_rate,
    String language,
    var invoices,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'date': date,
    'payment_number': payment_number,
    'notes': notes,
    'reference': reference,
    'bank_fee': bank_fee,
    'bank_id': bank_id,
    'payment_method': payment_method,
    'client_id': client_id,
    'currency_rate': currency_rate,
    'language': language,
    'pay': invoices,
  };

  Map<String, String> headers = {
    "Content-type": "application/json",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/payment/new'),
      // Send authorization headers to the backend.
      headers: headers,
      body: jsonEncode(queryParameters));
  print(queryParameters);
  return jsonDecode(response.body);
}

deletePayment(String companyId, String paymentId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/payment/$paymentId/delete'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

Future<void> downloadPayment(String companyId, deliveryId, fileName) async {

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> header = {
    "Content-type": "application/pdf",
    "Authorization": "$mobile_token"
  };

  final status = await Permission.storage.request();

  if(status.isGranted){
    final baseStorage = await getExternalStorageDirectory();
    final id = await FlutterDownloader.enqueue(url: 'https://birou.tn/$lang/api/private/company/$companyId/sales/delivery/$deliveryId/download',headers: header, savedDir: baseStorage.path, fileName: fileName);
  }else{
    print('No permission');
  }
}

getPayment(String companyId, String paymentId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/payment/$paymentId/synthesis'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}


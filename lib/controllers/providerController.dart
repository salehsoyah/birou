import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

newProvider(
    String first_name,
    String last_name,
    String company,
    int display_name,
    String delivery_address,
    String delivery_country,
    String delivery_state,
    String delivery_zip,
    String bill_address,
    String bill_country,
    String bill_state,
    String bill_zip,
    String email,
    String fiscal_id,
    int phone,
    String note,
    Uri website,
    String activity_id,
    String currency_id,
    int title,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'first_name': '$first_name',
    'last_name': '$last_name',
    'company': '$company',
    'display_name': '$display_name',
    'delivery_address': '$delivery_address',
    'delivery_country': '$delivery_country',
    'delivery_state': '$delivery_state',
    'delivery_zip': '$delivery_zip',
    'bill_address': '$bill_address',
    'bill_country': '$bill_country',
    'bill_state': '$bill_state',
    'bill_zip': '$bill_zip',
    'email': '$email',
    'fiscal_id': '$fiscal_id',
    'phone': '$phone',
    'note': '$note',
    'website': '$website',
    'activity_id': '$activity_id',
    'currency_id': '$currency_id',
    'title': '$title',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/provider/new'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

getProvider(String companyId, String providerId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/provider/$providerId/synthesis'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

editProvider(
    String first_name,
    String last_name,
    String company,
    int display_name,
    String delivery_address,
    String delivery_country,
    String delivery_state,
    String delivery_zip,
    String bill_address,
    String bill_country,
    String bill_state,
    String bill_zip,
    String email,
    String fiscal_id,
    int phone,
    String note,
    Uri website,
    String activity_id,
    String currency_id,
    int title,
    String providerId,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'first_name': '$first_name',
    'last_name': '$last_name',
    'company': '$company',
    'display_name': '$display_name',
    'delivery_address': '$delivery_address',
    'delivery_country': '$delivery_country',
    'delivery_state': '$delivery_state',
    'delivery_zip': '$delivery_zip',
    'bill_address': '$bill_address',
    'bill_country': '$bill_country',
    'bill_state': '$bill_state',
    'bill_zip': '$bill_zip',
    'email': '$email',
    'fiscal_id': '$fiscal_id',
    'phone': '$phone',
    'note': '$note',
    'website': '$website',
    'activity_id': '$activity_id',
    'currency_id': '$currency_id',
    'title': '$title',
  };

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/provider/$providerId/editDelivery'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

deleteProvider(String companyId, int providerId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/provider/$providerId/delete'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

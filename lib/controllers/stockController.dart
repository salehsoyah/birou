import 'dart:convert';
import 'dart:ffi';

import 'package:birou/models/article.dart';
import 'package:birou/models/estimate.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';


getStock(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response =
  await http.get(Uri.parse('https://birou.tn/$lang/api/private/company/$companyId/item'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Article> articles = [];

  var jsonData = json.decode(response.body);

    for (var u in jsonData["data"]["items"]) {
      Article article = Article(u['title'], u['hashed_id']);
      articles.add(article);
  }

  return articles;
}
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

subscriptionPlan(
    int type,
    int quantity,
    String operation,
    String plan_month,
    String plan_year,
    String discount,
    String company,
    String address,
    String state,
    String zip_code,
    String country_id) async {
  var queryParameters = {
    'type': '$type',
    'quantity': '$quantity',
    'operation': '$operation',
    'plan_month': '$plan_month',
    'plan_year': '$plan_year',
    'discount': '$discount',
    'company': '$company',
    'address': '$address',
    'state': '$state',
    'zip_code': '$zip_code',
    'country_id': '$country_id',
  };
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse('https://birou.tn/$lang/api/user/subscription/payment/go'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return jsonDecode(response.body);
}

subscriptionCallBack(String state, String payment_token,
    String reference) async {
  var queryParameters = {
    'payment_token': '$payment_token',
    'reference': '$reference',
  };
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/user/subscription/payment/$state/callback'),
      // Send authorization headers to the backend.
      headers: headers,
      body: queryParameters);

  return response.body;
}

discount(
  String code,
) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");
  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse('https://birou.tn/$lang/api/user/subscription/discount/$code'),
      // Send authorization headers to the backend.
      headers: headers);

  return response.body;
}

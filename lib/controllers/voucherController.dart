import 'dart:convert';
import 'dart:ffi';

import 'package:birou/models/delivery.dart';
import 'package:birou/models/voucher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

getVouchers(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/invoice/exit'),
      // Send authorization headers to the backend.
      headers: headers);

  List<Voucher> vouchers = [];

  var jsonData = json.decode(response.body);

  for (var u in jsonData["data"]["deliveries"]) {
    Voucher voucher = Voucher(u['date'], u['hashed_contact_id'], u['status'],
        jsonDecode(u['totals'])['total'], u['hashed_id']);
    vouchers.add(voucher);
  }

  return vouchers;
}

checkVoucherNumber(
    String companyId, int estimateNumber, String currentEstimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/invoice/exit/check/$estimateNumber/$currentEstimateId'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

markVoucher(String companyId, String estimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/invoice/exit/$estimateId/mark'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

newVoucher(
    String date,
    String delivery_number,
    String conditions,
    String reference,
    String notes,
    double discount,
    int tax_type,
    String client_id,
    int use_conditions,
    int show_stamp,
    int show_billing,
    int show_delivery,
    int show_bank,
    String bank_id,
    int show_conditions,
    int choice,
    int currency_rate,
    String language,
    var items,
    var use_input,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'date': date,
    'exit_voucher_number': delivery_number,
    'conditions': conditions,
    'reference': reference,
    'notes': notes,
    'discount': discount,
    'tax_type': tax_type,
    'client_id': client_id,
    'use_conditions': use_conditions,
    'show_stamp': show_stamp,
    'show_billing': show_billing,
    'show_delivery': show_delivery,
    'show_bank': show_bank,
    'bank_id': bank_id,
    'show_conditions': show_conditions,
    'choice': choice,
    'show_conditions': show_conditions,
    'choice': choice,
    'currency_rate': currency_rate,
    'language': language,
    'items': items,
    'use_input': use_input,
  };

  Map<String, String> headers = {
    "Content-type": "application/json",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/exit/new'),
      // Send authorization headers to the backend.
      headers: headers,
      body: jsonEncode(queryParameters));

  return jsonDecode(response.body);
}

nextVoucher(String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/exit/next'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

getExit(String companyId, String exitId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/exit/$exitId/synthesis'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

editVoucher(
    String date,
    String delivery_number,
    String conditions,
    String reference,
    String notes,
    double discount,
    int tax_type,
    String client_id,
    int use_conditions,
    int show_stamp,
    int show_billing,
    int show_delivery,
    int show_bank,
    String bank_id,
    int show_conditions,
    int choice,
    int currency_rate,
    String language,
    var items,
    var use_input,
    String exitNumber,
    String companyId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  var queryParameters = {
    'date': date,
    'exit_voucher_number': delivery_number,
    'conditions': conditions,
    'reference': reference,
    'notes': notes,
    'discount': discount,
    'tax_type': tax_type,
    'client_id': client_id,
    'use_conditions': use_conditions,
    'show_stamp': show_stamp,
    'show_billing': show_billing,
    'show_delivery': show_delivery,
    'show_bank': show_bank,
    'bank_id': bank_id,
    'show_conditions': show_conditions,
    'choice': choice,
    'show_conditions': show_conditions,
    'choice': choice,
    'currency_rate': currency_rate,
    'language': language,
    'items': items,
    'use_input': use_input,
  };

  Map<String, String> headers = {
    "Content-type": "application/json",
    "Authorization": "$mobile_token"
  };

  Response response = await http.post(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/sales/exit/$exitNumber/edit'),
      // Send authorization headers to the backend.
      headers: headers,
      body: jsonEncode(queryParameters));

  return jsonDecode(response.body);
}

downloadEstimate(String companyId, String estimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/invoice/exit/$estimateId/download'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

deleteEstimate(String companyId, String estimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/invoice/exit/$estimateId/delete'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

estimateInvoice(String companyId, String estimateId) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String mobile_token = sharedPreferences.getString("access_token");
  String lang = sharedPreferences.getString("lang");

  Map<String, String> headers = {
    "Content-type": "application/x-www-form-urlencoded",
    "Authorization": "$mobile_token"
  };

  Response response = await http.get(
      Uri.parse(
          'https://birou.tn/$lang/api/private/company/$companyId/invoice/exit/$estimateId/invoice'),
      // Send authorization headers to the backend.
      headers: headers);

  return jsonDecode(response.body);
}

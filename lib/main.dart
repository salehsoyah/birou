import 'dart:convert';

import 'package:birou/SplashScreen.dart';
import 'package:birou/lang/l10n/l10n.dart';
import 'package:birou/models/company.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/auth/registration.dart';
import 'package:birou/views/company/addCompany/accounting_information.dart';
import 'package:birou/views/company/collaborators/collaborators.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:birou/views/sales/payment/addPayment/add.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:birou/providers/local_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_downloader/flutter_downloader.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await FlutterDownloader.initialize(debug: true);

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var status = sharedPreferences.getBool('isLoggedIn');
  // print(status);

  String lastVisitedCompany =
      sharedPreferences.getString("lastVisitedCompanyId");

  List<Company> companies = [];
  if (sharedPreferences.getString("companiesList") != null) {
    for (var u in jsonDecode(sharedPreferences.getString("companiesList"))) {
      Company company = Company(u['title'], u['hashed_id'], u['user_name'], u['logo']);
      companies.add(company);
    }
  }

  var splashScreen = sharedPreferences.getBool("splashScreen");
  runApp(MyApp(
    widget: splashScreen == true ? (status == true
        ? MainMenu()
        : Login()) : SplashScreen(),
  ));
}

class MyApp extends StatelessWidget {
  Widget widget;

  MyApp({Key key, @required this.widget});

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
      create: (context) => LocaleProvider(),
      builder: (context, child) {
        final provider = Provider.of<LocaleProvider>(context);
        return MaterialApp(
          locale: provider.locale,
          supportedLocales: L10n.all,
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.blue,
          ),
          home: widget,
        );
      });
}

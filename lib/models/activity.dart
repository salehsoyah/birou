class Activity{
  final String title;
  final String hashed_id;

  Activity(this.title, this.hashed_id);
}
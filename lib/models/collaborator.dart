class Collaborator{
  final String id;
  final String name;
  final String email;
  final int role;

  Collaborator(this.id, this.name, this.email, this.role);
}
class Company {
  final String title;
  final String hashed_id;
  final String user_name;
  final String logo;
  Map<String, dynamic> toJson() => {
        'title': title,
        'hashed_id': hashed_id,
        'user_name': user_name,
      };


  Company(this.title, this.hashed_id, this.user_name, this.logo);
}

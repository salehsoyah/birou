class Condition{
  final String title;
  final int condition;

  Condition(this.title, this.condition);
}
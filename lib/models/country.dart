class Country{
  final String title;
  final String hashed_id;

  Country(this.title, this.hashed_id);
}
class Deadline{
  final String title;
  final int days;
  final String id;

  Deadline(this.title, this.days, this.id);
}
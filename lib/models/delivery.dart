class Delivery{
  final String date;
  final String nb;
  final int status;
  final String total;
  final String id;

  Delivery(this.date, this.nb, this.status, this.total, this.id);
}
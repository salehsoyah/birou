class Device{
  final String uuid;
  final String platform;
  final int os_version;
  final String model;
  final String mobile_token;

  Device(this.uuid, this.platform, this.os_version, this.model, this.mobile_token);


  getUuid(){
    return uuid;
  }

  getPlatform(){
    return platform;
  }

  getOs_version(){
    return os_version;
  }

  getModel(){
    return model;
  }

  getMobile_token(){
    return mobile_token;
  }
}
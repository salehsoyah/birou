class Input{
  final String title;
  final String default_value;
  final int type;
  final int use;
  final int conditional_country_id;
  final int conditional_currency_id;
  final String hashed_id;
  final int condition;

  Input(this.title, this.default_value, this.type, this.use, this.conditional_country_id, this.conditional_currency_id, this.hashed_id, this.condition);
}
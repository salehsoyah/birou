class InputAdd{

   final String input_hashed_id;
   final String default_value;

   InputAdd(this.input_hashed_id, this.default_value);

  Map<String, dynamic> toJson() => {
    'input_hashed_id': input_hashed_id,
    'value': default_value
  };
}


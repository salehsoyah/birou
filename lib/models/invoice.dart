class Invoice{
  final String date;
  final String nb;
  final int status;
  final String total;
  final String id;

  Invoice(this.date, this.nb, this.status, this.total, this.id);
}
import 'package:birou/models/taxAdd.dart';

class Item{
   String item;
   int qte;
   double price;
   List<TaxAdd>  taxes = [];
   String item_hashed_id;

  Item({
    this.item_hashed_id = '', this.item = '', this.qte = 0, this.price = 0, this.taxes
});

   Map<String, dynamic> toJson() => {
     'item_hashed_id': item_hashed_id,
     'item': item,
     'qte': qte,
     'price': price,
     'taxes': taxes,
   };
  
}
class Language{
  final String title;
  final String hashed_id;

  Language(this.title, this.hashed_id);
}
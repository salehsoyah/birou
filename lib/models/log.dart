class Log{
  final String user_id;
  final String module;
  final String element;
  final String action;

  Log(this.user_id, this.module, this.element, this.action);
}
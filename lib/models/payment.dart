class Payment{
  final String date;
  final String nb;
  final int status;
  final String total;
  final String id;

  Payment(this.date, this.nb, this.status, this.total, this.id);
}
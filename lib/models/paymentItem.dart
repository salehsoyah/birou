import 'package:birou/models/taxAdd.dart';

class PaymentItem {
  String invoice_hashed_id;
  String disbursement_hashed_id;
  double amount;

  String invoice_id;
  String total;
  String to_pay;

  PaymentItem(
      this.invoice_hashed_id,
      this.disbursement_hashed_id,
      this.amount,
      this.invoice_id,
      this.total,
      this.to_pay);

  Map<String, dynamic> toJson() => {
        'invoice_hashed_id': invoice_hashed_id,
        'disbursement_hashed_id': disbursement_hashed_id,
        'amount': amount,
      };
}

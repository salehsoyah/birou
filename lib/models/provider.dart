class Providerr{
  final String display_name;
  final String email;
  final String organisation;
  final String id;

  Providerr(this.display_name, this.email, this.organisation, this.id);
}
class Tax{
  final String title;
  final int rate;
  final String hashed_id;
  final int special;

  Tax(this.title, this.rate, this.hashed_id, this.special);
}
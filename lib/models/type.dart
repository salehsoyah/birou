class Type{
  final String title;
  final int hashed_id;

  Type(this.title, this.hashed_id);
}
class Voucher{
  final String date;
  final String client;
  final int status;
  final String total;
  final String id;

  Voucher(this.date, this.client, this.status, this.total, this.id);
}
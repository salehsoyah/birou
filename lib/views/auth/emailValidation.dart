import 'package:birou/controllers/authController.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/auth/registration.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class EmailValidation extends StatefulWidget {
  @override
  String email;

  _EmailValidationState createState() => _EmailValidationState();

  EmailValidation({Key key, @required this.email}) : super(key: key);
}

class _EmailValidationState extends State<EmailValidation> {
  final _tokenNb1 = TextEditingController();
  final _tokenNb2 = TextEditingController();
  final _tokenNb3 = TextEditingController();
  final _tokenNb4 = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => Registration()));
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 2,
            ),
            Text(
              AppLocalizations.of(context).verify_email,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 4,
            ),
          ],
        ),
      ),
      body: loading == false
          ? SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Container(
                  color: Color.fromRGBO(245, 246, 252, 1),
                  height: MediaQuery.of(context).size.height * 0.9,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: width / 9,
                      ),
                      Center(
                          child: Text(
                        AppLocalizations.of(context)
                            .enter_email_validation_digits,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey[600],
                        ),
                      )),
                      // ignore: deprecated_member_use
                      SizedBox(
                        height: width / 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.width / 10),
                            child: SizedBox(
                              width: width / 6.5,
                              height: width / 6.5,
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFFF6F5FA),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(15),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 5.0,
                                          spreadRadius: 1,
                                          offset: Offset(0.0, 0.75))
                                    ],
                                  ),
                                  child: Center(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 6),
                                      child: TextFormField(
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Required';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        controller: _tokenNb1,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 20),
                                        decoration: new InputDecoration(
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  )),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.width / 10),
                            child: SizedBox(
                              width: width / 6.5,
                              height: width / 6.5,
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFFF6F5FA),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(15),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 5.0,
                                          spreadRadius: 1,
                                          offset: Offset(0.0, 0.75))
                                    ],
                                  ),
                                  child: Center(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 6),
                                      child: TextFormField(
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Required';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        controller: _tokenNb2,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 20),
                                        decoration: new InputDecoration(
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  )),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.width / 10),
                            child: SizedBox(
                              width: width / 6.5,
                              height: width / 6.5,
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFFF6F5FA),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(15),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 5.0,
                                          spreadRadius: 1,
                                          offset: Offset(0.0, 0.75))
                                    ],
                                  ),
                                  child: Center(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 6),
                                      child: TextFormField(
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Required';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        controller: _tokenNb3,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 20),
                                        decoration: new InputDecoration(
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  )),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.width / 10),
                            child: SizedBox(
                              width: width / 6.5,
                              height: width / 6.5,
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFFF6F5FA),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(15),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 5.0,
                                          spreadRadius: 1,
                                          offset: Offset(0.0, 0.75))
                                    ],
                                  ),
                                  child: Center(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 6),
                                      child: TextFormField(
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Required';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        controller: _tokenNb4,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 20),
                                        decoration: new InputDecoration(
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: width * 0.1,
                      ),
                      Center(
                          child: Text(
                        AppLocalizations.of(context).check_spam,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey[600],
                        ),
                      )),
                      SizedBox(
                        height: width / 2.6,
                      ),
                      RaisedButton(
                          color: Color.fromRGBO(55, 86, 223, 1),
                          onPressed: () => {
                                if (_formKey.currentState.validate())
                                  {
                                    setState((){
                                      loading = true;
                                    }),
                                    emailVerify(
                                            int.parse(_tokenNb1.text +
                                                _tokenNb2.text +
                                                _tokenNb3.text +
                                                _tokenNb4.text),
                                            widget.email)
                                        .then((result) {
                                      setState((){
                                        loading = false;
                                      });
                                      if(result['status']['code'] == 200){
                                        Navigator.of(context).pushReplacement(
                                            MaterialPageRoute(
                                                builder: (BuildContext context) =>
                                                    Login()));
                                      }else{
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return DialogScreen(image: 0, text1: 'Error', text2: result['message'], );
                                            });
                                      }
                                    }),
                                  }
                              },
                          // padding: const EdgeInsets.all(0.0),
                          child: Container(
                            color: Color.fromRGBO(55, 86, 223, 1),
                            width: width * 0.78,
                            padding:
                                const EdgeInsets.only(top: 20.0, bottom: 20),
                            child: Row(
                              children: [
                                Spacer(),
                                Container(
                                    margin: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .verify
                                          .toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white),
                                    )),
                                Spacer(),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Image(
                                      image: AssetImage(
                                          "assets/images/arrowGray.png"),
                                    )),
                              ],
                            ),
                          ),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                    ],
                  ),
                ),
              ),
            )
          : Container(
          alignment: Alignment.bottomCenter,
          height: MediaQuery.of(context).size.height / 2, child: spin())
    );
  }

  Widget spin() {
    return Padding(
      padding: EdgeInsets.only(top: 200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Text("Birou", style: TextStyle(
                color: Colors.blueAccent,
                fontSize: 27,
                fontWeight: FontWeight.w500
            ),),
          ),
          SpinKitCubeGrid(
            itemBuilder: (BuildContext context, int index){
              return DecoratedBox(decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.1, 0.4, 0.7, 0.9],
                    colors: [
                      Colors.blueAccent,
                      Colors.redAccent,
                      Colors.greenAccent,
                      Colors.grey,
                    ],
                  )
              ));
            },
            size: 70.0,
          ),
        ],
      ),
    );
  }

}

import 'dart:convert';
import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/device.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:device_info/device_info.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  final _emailController = TextEditingController();
  final _nameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool obscureText = true;
  String mobile_token;
  final _firebaseMessaging = FirebaseMessaging.instance;
  final _formKey = GlobalKey<FormState>();
  bool loading = false;
  List<Company> companies = [];
  bool _isValid = EmailValidator.validate('fredrik.eilertsen@gail.com');

  @override
  void initState() {
    _firebaseMessaging.getToken().then((String token) async {
      assert(token != null);
      setState(() {
        mobile_token = token;
      });
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Color.fromRGBO(245, 246, 252, 1),
          leading: GestureDetector(
            onTap: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => Login()));
            },
            child: Icon(
              Icons.arrow_back,
              color: Color.fromRGBO(55, 86, 223, 1),
            ),
          ),
          title: Row(
            children: [
              Spacer(),
              Text(
                AppLocalizations.of(context).create_account,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 18,
                ),
              ),
              Spacer(),
              Spacer(),
            ],
          ),
        ),
        body: loading == false
            ? SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Container(
                    color: Color.fromRGBO(245, 246, 252, 1),
                    child: Column(
                      children: [
                        SizedBox(
                          height: width / 11,
                        ),
                        Center(
                            child: Text(
                          AppLocalizations.of(context)
                              .enter_your_info_to_create,
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.grey[600],
                          ),
                        )),
                        SizedBox(
                          height: width / 11,
                        ),
                        Container(
                          width: width * 0.86,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(30),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.blue[100],
                                  spreadRadius: 0.5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 25),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: width / 30,
                                ),
                                TextFormField(
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return AppLocalizations.of(context).enter_your_name;
                                    }
                                    return null;
                                  },
                                  controller: _nameController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .your_name,
                                      hintStyle: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 17,
                                          fontWeight: FontWeight.normal)),
                                ),
                                Divider(
                                  color: Colors.grey[300],
                                  thickness: 1,
                                ),
                                TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      _isValid = EmailValidator.validate(value);
                                    });
                                    print(_isValid);
                                  },
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return AppLocalizations.of(context).enter_email;
                                    } else if (_isValid == false) {
                                      return AppLocalizations.of(context).enter_valid_email + ': example@example.com';
                                    }
                                    return null;
                                  },
                                  controller: _emailController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .your_email,
                                      hintStyle: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 17,
                                          fontWeight: FontWeight.normal)),
                                ),
                                Divider(
                                  color: Colors.grey[300],
                                  thickness: 1.0,
                                ),
                                Row(
                                  children: [
                                    Flexible(
                                      child: TextFormField(
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return AppLocalizations.of(context).enter_password;
                                          }
                                          return null;
                                        },
                                        controller: _passwordController,
                                        obscureText: obscureText,
                                        decoration: InputDecoration(
                                            border: InputBorder.none,
                                            hintText:
                                                AppLocalizations.of(context)
                                                    .your_password,
                                            hintStyle: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 17)),
                                      ),
                                    ),
                                    InkWell(
                                        child: Center(
                                            child: Container(
                                                child: Text(
                                          AppLocalizations.of(context).show,
                                          style: TextStyle(
                                              color: Colors.blueAccent),
                                        ))),
                                        onTap: () {
                                          if (obscureText == true) {
                                            setState(() {
                                              obscureText = false;
                                            });
                                          } else {
                                            setState(() {
                                              obscureText = true;
                                            });
                                          }
                                        }),
                                    SizedBox(
                                      width: width / 22,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: width / 30,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: width / 11,
                        ),
                        // ignore: deprecated_member_use
                        RaisedButton(
                            color: Color.fromRGBO(109, 157, 223, 1),
                            onPressed: () => {
                                  _getDeviceInfo().then((device) {
                                    // authFacebook().then((value) {
                                    //   social_registration_login(
                                    //           "facebook",
                                    //           value['email'],
                                    //           value['name'],
                                    //           device.getUuid(),
                                    //           value['picture']['data']['url'],
                                    //           device.getPlatform(),
                                    //           device.getOs_version(),
                                    //           device.getModel(),
                                    //           mobile_token)
                                    //       .then((result) {});
                                    // });
                                  }),
                                },
                            // padding: const EdgeInsets.all(0.0),
                            child: Container(
                              color: Color.fromRGBO(109, 157, 223, 1),
                              width: width * 0.78,
                              padding:
                                  const EdgeInsets.only(top: 20.0, bottom: 20),
                              child: Row(
                                children: [
                                  Spacer(),
                                  Container(
                                      margin: const EdgeInsets.only(left: 10.0),
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .sign_up_facebook
                                            .toUpperCase(),
                                        style: TextStyle(
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.normal,
                                            color: Colors.white),
                                      )),
                                  Spacer(),
                                  Align(
                                      alignment: Alignment.centerRight,
                                      child: Image(
                                        image: AssetImage(
                                            "assets/images/arrowBlue.png"),
                                      )),
                                ],
                              ),
                            ),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0))),
                        SizedBox(
                          height: width * 0.05,
                        ),
                        RaisedButton(
                            color: Color.fromRGBO(177, 142, 145, 1),
                            onPressed: () {
                              _getDeviceInfo().then((device) {
                                authGoogle().then((value) {
                                  social_registration_login(
                                          "google",
                                          value.email,
                                          value.displayName,
                                          device.getUuid(),
                                          value.photoURL,
                                          device.getPlatform(),
                                          device.getOs_version(),
                                          device.getModel(),
                                          mobile_token)
                                      .then((result) async {
                                    print(result);
                                    SharedPreferences sharedPreferences =
                                        await SharedPreferences.getInstance();
                                    sharedPreferences.setString(
                                        "access_token",
                                        result['data']['device']
                                            ['access_token']);

                                    print(sharedPreferences
                                        .getString("access_token"));
                                    sharedPreferences.setString(
                                        'new_old',
                                        result['data']['user'][
                                            'hashed_settings_preferred_company']);
                                    sharedPreferences.setString('fullName',
                                        result['data']['user']['name']);

                                    for (var u in result["data"]["user"]
                                        ['owned_companies']) {
                                      Company company = Company(
                                          u['title'],
                                          u['hashed_id'],
                                          result['data']['user']['name'], u['logo']);
                                      companies.add(company);
                                    }
                                    for (var u in result["data"]["user"]
                                        ['joined_companies']) {
                                      Company company = Company(
                                          u['title'],
                                          u['hashed_id'],
                                          result['data']['user']['name'], u['logo']);
                                      companies.add(company);
                                    }

                                    String companiesList = jsonEncode(companies
                                        .map((e) => e.toJson())
                                        .toList());
                                    sharedPreferences.setString(
                                        "companiesList", companiesList);

                                    print(companies);

                                    sharedPreferences.setString("lang", "en");

                                    sharedPreferences.setBool(
                                        "isLoggedIn", true);
                                    Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                MainMenu(
                                                  companies: companies,
                                                )));
                                  });
                                });
                              });
                            },
                            // padding: const EdgeInsets.all(0.0),
                            child: Container(
                              color: Color.fromRGBO(177, 142, 145, 1),
                              width: width * 0.78,
                              padding:
                                  const EdgeInsets.only(top: 20.0, bottom: 20),
                              child: Row(
                                children: [
                                  Spacer(),
                                  Container(
                                      margin: const EdgeInsets.only(left: 10.0),
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .sign_up_google
                                            .toUpperCase(),
                                        style: TextStyle(
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.normal,
                                            color: Colors.white),
                                      )),
                                  Spacer(),
                                  Align(
                                      alignment: Alignment.centerRight,
                                      child: Image(
                                        image: AssetImage(
                                            "assets/images/arrowBrown.png"),
                                      )),
                                ],
                              ),
                            ),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0))),
                        SizedBox(
                          height: width * 0.05,
                        ),
                        RaisedButton(
                            color: Color.fromRGBO(55, 86, 223, 1),
                            onPressed: () => {
                                  if (_formKey.currentState.validate())
                                    {
                                      setState(() {
                                        loading = true;
                                      }),
                                      registration(
                                              _emailController.text,
                                              _passwordController.text,
                                              _nameController.text)
                                          .then((result) {
                                        print(result);
                                        setState(() {
                                          loading = false;
                                        });
                                        if (result['status']['code'] == 402) {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogScreen(
                                                  image: 0,
                                                  text1: 'Error',
                                                  text2: result['message'],
                                                );
                                              });
                                        } else if (result['status']['code'] ==
                                            403) {
                                          Navigator.of(context).pushReplacement(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          EmailValidation(
                                                            email:
                                                                _emailController
                                                                    .text,
                                                          )));
                                        }
                                      })
                                    }
                                },
                            // padding: const EdgeInsets.all(0.0),
                            child: Container(
                              color: Color.fromRGBO(55, 86, 223, 1),
                              width: width * 0.78,
                              padding:
                                  const EdgeInsets.only(top: 20.0, bottom: 20),
                              child: Row(
                                children: [
                                  Spacer(),
                                  Container(
                                      margin: const EdgeInsets.only(left: 10.0),
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .create_account
                                            .toUpperCase(),
                                        style: TextStyle(
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.normal,
                                            color: Colors.white),
                                      )),
                                  Spacer(),
                                  Align(
                                      alignment: Alignment.centerRight,
                                      child: Image(
                                        image: AssetImage(
                                            "assets/images/arrowGray.png"),
                                      )),
                                ],
                              ),
                            ),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0))),
                        SizedBox(
                          height: width * 0.05,
                        ),
                        Row(
                          children: [
                            Spacer(),
                            Text(AppLocalizations.of(context)
                                .already_have_an_account),
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            Login()));
                              },
                              child: Text(
                               ' '+ AppLocalizations.of(context).sign_in,
                                style: TextStyle(color: Colors.blue[700]),
                              ),
                            ),
                            Spacer(),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        )
                      ],
                    ),
                  ),
                ),
              )
            : Container(
                alignment: Alignment.bottomCenter,
                height: MediaQuery.of(context).size.height / 2,
                child: spin()));
  }

  Future<Device> _getDeviceInfo() async {
    var deviceInfo = DeviceInfoPlugin();

    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      String uuid = iosDeviceInfo.identifierForVendor;
      String platform = "IOS";
      int os_version = int.parse(iosDeviceInfo.systemVersion);
      String model = iosDeviceInfo.utsname.machine;

      Device device = Device(uuid, platform, os_version, model, mobile_token);
      return device;
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      String uuid = androidDeviceInfo.androidId;
      String platform = "Android";
      int os_version = androidDeviceInfo.version.sdkInt;
      String model = androidDeviceInfo.model;

      Device device = Device(uuid, platform, os_version, model, mobile_token);

      return device;
    }
  }

  Widget spin() {
    return Padding(
      padding: EdgeInsets.only(top: 200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Text(
              "Birou",
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 27,
                  fontWeight: FontWeight.w500),
            ),
          ),
          SpinKitCubeGrid(
            itemBuilder: (BuildContext context, int index) {
              return DecoratedBox(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.1, 0.4, 0.7, 0.9],
                colors: [
                  Colors.blueAccent,
                  Colors.redAccent,
                  Colors.greenAccent,
                  Colors.grey,
                ],
              )));
            },
            size: 70.0,
          ),
        ],
      ),
    );
  }
}

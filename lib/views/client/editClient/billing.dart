import 'dart:convert';
import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/clientController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/device.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/client/addClient/delivery.dart';
import 'package:birou/views/client/editClient/delivery.dart';
import 'package:birou/views/subscriptions/last_step.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BillingClientUpdate extends StatefulWidget {
  @override
  _BillingClientUpdateState createState() => _BillingClientUpdateState();

  int title;
  String firstName;
  String lastName;
  String company;
  int displayName;
  String email;
  String phone;
  String website;
  String tax;
  String activity;
  String currency;
  String deadline;
  String note;
  String id;
  String companyId;
  final List<Company> companies;

  BillingClientUpdate(
      {Key key,
      @required this.title,
      @required this.firstName,
      @required this.lastName,
      @required this.company,
      @required this.displayName,
      @required this.email,
      @required this.phone,
      @required this.website,
      @required this.tax,
      @required this.activity,
      @required this.currency,
      @required this.deadline,
      @required this.note,
      @required this.id,
      @required this.companyId,
      @required this.companies})
      : super(key: key);
}

class _BillingClientUpdateState extends State<BillingClientUpdate> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();

  final _addressController = TextEditingController();
  final _stateController = TextEditingController();
  final _zipCodeController = TextEditingController();

  bool obscureText = true;
  String mobile_token;
  List<Country> countries = [];
  String country;
  String countryText;
  bool btnPressed = false;
  final _firebaseMessaging = FirebaseMessaging.instance;
  Future listCountries;

  void initState() {
    listCountries = companyCountries();

    getClient(widget.companyId, widget.id).then((value) {
      companyCountries().then((countriesResult) {
        setState(() {
          country =
              jsonDecode(value['data']['client']['billing'])['country_id'];
          setState(() {
            countries = countriesResult;
            for (var i in countries) {
              if (country == i.hashed_id) {
                print('yes');
                setState(() {
                  countryText = i.title;
                });
              }
            }
          });
        });
      });
      setState(() {
        _addressController.text =
            jsonDecode(value['data']['client']['billing'])['address'];
        _stateController.text =
            jsonDecode(value['data']['client']['billing'])['bill_state'];
        _zipCodeController.text =
            jsonDecode(value['data']['client']['billing'])['zip_code'];
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).edit_client,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(245, 246, 252, 1),
          child: Column(
            children: [
              SizedBox(
                height: width / 11,
              ),
              Row(
                children: [
                  SizedBox(
                    width: width / 11,
                  ),
                  Text(
                    AppLocalizations.of(context).billing_address,
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey[600],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.83,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Form(
                  key: _form,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        TextFormField(
                          controller: _addressController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).address,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          controller: _stateController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).state,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          controller: _zipCodeController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).zip_code,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).country,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  countryText != null
                                      ? countryText
                                      : AppLocalizations.of(context)
                                          .choose_country,
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && country == null
                                    ? Text(
                                        AppLocalizations.of(context)
                                            .choose_country,
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 12),
                                      )
                                    : Container()
                              ],
                            ),
                            FutureBuilder(
                              future: listCountries,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text(
                                      AppLocalizations.of(context).loading);
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                                value: value.hashed_id,
                                                child: Text(
                                                  value.title,
                                                ),
                                              ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      country = value;
                                    });

                                    for (var i in countries) {
                                      if (value == i.hashed_id) {
                                        setState(() {
                                          countryText = i.title;
                                        });
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        SizedBox(
                          height: width / 22,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 5.6,
              ),
              // ignore: deprecated_member_use
              RaisedButton(
                  color: Color.fromRGBO(55, 86, 223, 1),
                  onPressed: () {
                    setState(() {
                      btnPressed = true;
                    });

                    print(country);

                    if (_form.currentState.validate() && country != null) {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              DeliveryClientUpdate(
                                lastName: widget.lastName,
                                firstName: widget.firstName,
                                company: widget.company,
                                phone: widget.phone,
                                note: widget.note,
                                billState: _stateController.text,
                                title: widget.title,
                                billAddress: _addressController.text,
                                tax: widget.tax,
                                email: widget.email,
                                website: widget.website,
                                billZip: _zipCodeController.text,
                                displayName: widget.displayName,
                                currency: widget.currency,
                                billCountry: country,
                                activity: widget.activity,
                                companyId: widget.companyId,
                                id: widget.id,
                                deadline: widget.deadline,
                                companies: widget.companies,
                              )));
                    }
                  },
                  // padding: const EdgeInsets.all(0.0),
                  child: Container(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    width: width * 0.78,
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        Container(
                            margin: const EdgeInsets.only(left: 10.0),
                            child: Text(
                              AppLocalizations.of(context).next.toUpperCase(),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            )),
                        Spacer(),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: AssetImage("assets/images/arrowGray.png"),
                            )),
                      ],
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0))),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}

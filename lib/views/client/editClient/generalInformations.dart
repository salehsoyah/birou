import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:io' as Io;

import 'package:birou/controllers/clientController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/models/activity.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/currency.dart';
import 'package:birou/models/deadline.dart';
import 'package:birou/models/displayName.dart';
import 'package:birou/models/language.dart';
import 'package:birou/models/title.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/client/addClient/billing.dart';
import 'package:birou/views/client/addClient/delivery.dart';
import 'package:birou/views/client/editClient/billing.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class GeneralInformationClientUpdate extends StatefulWidget {
  @override
  _GeneralInformationClientUpdateState createState() =>
      _GeneralInformationClientUpdateState();

  String id;
  String companyId;
  final List<Company> companies;

  GeneralInformationClientUpdate(
      {Key key,
        @required this.id,
        @required this.companyId, @required this.companies})
      : super(key: key);
}

class _GeneralInformationClientUpdateState extends State<GeneralInformationClientUpdate> {
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _companyController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _websiteController = TextEditingController();
  final _taxController = TextEditingController();
  final _noteController = TextEditingController();

  File imageFile;
  String pdfLanguage;
  String pdfText;
  int titlePicked;
  String titleText;
  String logo;
  String logoText;
  List<Language> languages = [
    Language("Français", "fr"),
    Language("English", "en"),
    Language("Arabic", "ar")
  ];
  List<TitlePicked> titles = [
    TitlePicked("Mr.", 1),
    TitlePicked("Mlle.", 2),
    TitlePicked("M.", 3)
  ];

  String familyName = '';
  String firstName = '';
  String companyName = '';

  int displayName = 1;
  String displayNameText;
  List<DisplayName> displayNames = [
    DisplayName('', '', 1),
    DisplayName('', '', 2),
    DisplayName('', '', 3)
  ];

  bool btnPressed = false;
  String activity;
  String activityTitle;
  List<Activity> activities = [];
  Future listActivities;

  String currency;
  String currencyTitle;
  List<Currency> currencies = [];
  Future listCurrency;
  Future listDeadline;

  List<Deadline> deadlines = [];
  String deadline;
  String deadlineTitle;

  GlobalKey<FormState> _form = GlobalKey<FormState>();

  void _validate() {
    _form.currentState.validate();
  }

  static Future image2Base64(String path) async {
    File file = new File(path);
    List<int> imageBytes = await file.readAsBytes();
    return base64Encode(imageBytes);
  }

  void initState() {
    listActivities = companyActivities(widget.companyId);
    listCurrency = companyCurrencies();
    listDeadline = companyDeadlines(widget.companyId);

    getClient(widget.companyId, widget.id).then((value){
      companyActivities(widget.companyId).then((result) {
       setState(() {
         activity = value['data']['client']['hashed_activity_id'];
         setState(() {
           activities = result;
           for (var i in activities) {
             if (activity == i.hashed_id) {
               setState(() {
                 activityTitle = i.title;
               });
             }
           }
         });
       });
      });
      companyCurrencies().then((result) {
       setState(() {
         currency = value['data']['client']['hashed_currency_id'];
         setState(() {
           currencies = result;
           for (var i in currencies) {
             if (currency == i.hashed_id) {
               print('yes');
               setState(() {
                 currencyTitle = i.title;
               });
             }
           }
         });
       });
      });
      companyDeadlines(widget.companyId).then((result) {
        setState(() {
          deadline = value['data']['client']['hashed_default_invoice_deadline_id'];
          setState(() {
            deadlines = result;
            for (var i in deadlines) {
              if (deadline == i.id) {
                print('yes');
                setState(() {
                  deadlineTitle = i.title;
                });
              }
            }
          });
        });
      });
      print(value);
      setState(() {
        titlePicked = value['data']['client']['title'];
        _firstNameController.text = value['data']['client']['first_name'];
        _lastNameController.text = value['data']['client']['last_name'];
        _companyController.text = value['data']['client']['organisation'];
        displayNameText = value['data']['client']['display_name'];
        _emailController.text = value['data']['client']['email'];
        _phoneController.text = value['data']['client']['phone'];
        _websiteController.text = value['data']['client']['website'];
        _taxController.text = value['data']['client']['fiscal_id'];

        _noteController.text = value['data']['client']['note'];
        displayNames[0].text1 = value['data']['client']['organisation'];
        displayNames[1].text1 = value['data']['client']['first_name'];
        displayNames[1].text2 = value['data']['client']['last_name'];
        displayNames[2].text1 = value['data']['client']['last_name'];
        displayNames[2].text2 = value['data']['client']['first_name'];

      });

      for (var i in titles) {
        if (titlePicked == i.id) {
          setState(() {
            titleText = i.title;
          });
        }
      }







    });



    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);

          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).edit_client,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _form,
          child: Container(
            color: Color.fromRGBO(245, 246, 252, 1),
            child: Column(
              children: [
                SizedBox(
                  height: width / 11,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: width / 11.5,
                    ),
                    Text(
                      AppLocalizations.of(context).general_information,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey[600],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.84,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).title,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  titleText != null
                                      ? titleText
                                      : AppLocalizations.of(context).choose_title,
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && titlePicked == null
                                    ? Text(
                                  AppLocalizations.of(context).enter_title,
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 12),
                                )
                                    : Container()
                              ],
                            ),
                            PopupMenuButton<int>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              onSelected: (int result) {
                                setState(() {
                                  titlePicked = result;
                                });

                                for (var i in titles) {
                                  if (i.id == result) {
                                    print(i.title);
                                    setState(() {
                                      titleText = i.title;
                                    });
                                  }
                                }
                              },
                              itemBuilder: (BuildContext context) =>
                              <PopupMenuEntry<int>>[
                                PopupMenuItem<int>(
                                  value: titles[0].id,
                                  child: Text(titles[0].title),
                                ),
                                PopupMenuItem<int>(
                                  value: titles[1].id,
                                  child: Text(titles[1].title),
                                ),
                                PopupMenuItem<int>(
                                  value: titles[2].id,
                                  child: Text(titles[2].title),
                                ),
                              ],
                            )
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1,
                        ),
                        TextFormField(
                          onChanged: (text) {
                            setState(() {
                              displayNames[1].text1 = text;
                              displayNames[2].text2 = text;
                            });
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return AppLocalizations.of(context).enter_first_name;
                            }
                            return null;
                          },
                          controller: _firstNameController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).first_name,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          onChanged: (text) {
                            displayNames[1].text2 = text;
                            displayNames[2].text1 = text;
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return AppLocalizations.of(context).enter_last_name;
                            }
                            return null;
                          },
                          controller: _lastNameController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).last_name,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          onChanged: (text) {
                            displayNames[0].text1 = text;
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return AppLocalizations.of(context).company_name;
                            }
                            return null;
                          },
                          controller: _companyController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).company_name,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).display_name,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  displayNameText != null
                                      ? displayNameText
                                      : AppLocalizations.of(context).choose_title,
                                  style: TextStyle(fontSize: 13),
                                ),
                                // btnPressed && titlePicked == null ? Text("Please choose a display name", style: TextStyle(
                                //     color: Colors.red,
                                //     fontSize: 12
                                // ),
                                // ) : Container()
                              ],
                            ),
                            PopupMenuButton<int>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              onSelected: (int result) {
                                setState(() {
                                  displayName = result;
                                });

                                for (var i in displayNames) {
                                  if (i.id == result) {
                                    print(i.text1);
                                    setState(() {
                                      displayNameText = i.text1 + ' ' + i.text2;
                                    });
                                  }
                                }
                              },
                              itemBuilder: (BuildContext context) =>
                              <PopupMenuEntry<int>>[
                                PopupMenuItem<int>(
                                  value: displayNames[0].id,
                                  child: Text(displayNames[0].text1 +
                                      ' ' +
                                      displayNames[0].text2),
                                ),
                                PopupMenuItem<int>(
                                  value: displayNames[1].id,
                                  child: Text(displayNames[1].text1 +
                                      ' ' +
                                      displayNames[1].text2),
                                ),
                                PopupMenuItem<int>(
                                  value: displayNames[2].id,
                                  child: Text(displayNames[2].text1 +
                                      ' ' +
                                      displayNames[2].text2),
                                ),
                              ],
                            )
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1,
                        ),
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return AppLocalizations.of(context).enter_email;
                            }
                            return null;
                          },
                          controller: _emailController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).email,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return AppLocalizations.of(context).enter_phone;
                            }
                            return null;
                          },
                          controller: _phoneController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).phone,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          validator: _websiteController.text != ""
                              ? ValidationBuilder().url().build()
                              : null,
                          controller: _websiteController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).website,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: width / 11.5,
                    ),
                    Text(
                      AppLocalizations.of(context).professional_information,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey[600],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.84,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        TextFormField(
                          controller: _taxController,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return AppLocalizations.of(context).enter_tax_id_number;
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText:
                              AppLocalizations.of(context).tax_id_number,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).activity,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  activityTitle != null
                                      ? activityTitle
                                      : AppLocalizations.of(context)
                                      .choose_an_activity,
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && activity == null
                                    ? Text(
                                  AppLocalizations.of(context).enter_an_activity,
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 12),
                                )
                                    : Container()
                              ],
                            ),
                            FutureBuilder(
                              future: listActivities,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text(AppLocalizations.of(context).loading);
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                        value: value.hashed_id,
                                        child: Text(
                                          value.title,
                                        ),
                                      ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      activity = value;
                                    });
                                    for (var i in activities) {
                                      if (value == i.hashed_id) {
                                        setState(() {
                                          activityTitle = i.title;
                                        });
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).currency,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  currencyTitle != null
                                      ? currencyTitle
                                      : AppLocalizations.of(context)
                                      .main_currency,
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && currency == null
                                    ? Text(
                                  AppLocalizations.of(context).enter_currency,
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 12),
                                )
                                    : Container()
                              ],
                            ),
                            FutureBuilder(
                              future: listCurrency,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text(AppLocalizations.of(context).enter_currency);
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                        value: value.hashed_id,
                                        child: Text(
                                          value.title,
                                        ),
                                      ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      currency = value;
                                    });

                                    for (var i in currencies) {
                                      if (value == i.hashed_id) {
                                        setState(() {
                                          currencyTitle = i.title;
                                        });
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).deadline,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  deadlineTitle != null
                                      ? deadlineTitle
                                      : AppLocalizations.of(context).choose_deadline,
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && deadline == null
                                    ? Text(
                                  AppLocalizations.of(context).enter_deadline,
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 12),
                                )
                                    : Container()
                              ],
                            ),
                            FutureBuilder(
                              future: listDeadline,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text(AppLocalizations.of(context).loading);
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                        value: value.id,
                                        child: Text(
                                          value.title,
                                        ),
                                      ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      deadline = value;
                                    });

                                    for (var i in deadlines) {
                                      if (value == i.id) {
                                        setState(() {
                                          deadlineTitle = i.title;
                                        });
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: width / 11.5,
                    ),
                    Text(
                      AppLocalizations.of(context).notes,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey[600],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.84,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: TextFormField(
                      controller: _noteController,
                      minLines: 6,
                      // any number you need (It works as the rows for the textarea)
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 4,
                ),
                RaisedButton(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    onPressed: () async {
                      setState(() {
                        btnPressed = true;
                      });
                      if (_form.currentState.validate() &&
                          displayName != null &&
                          titlePicked != null &&
                          activity != null &&
                          currency != null &&
                          deadline != null) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => BillingClientUpdate(
                              lastName: _lastNameController.text,
                              activity: activity,
                              displayName: displayName,
                              firstName: _firstNameController.text,
                              website: _websiteController.text,
                              currency: currency,
                              email: _emailController.text,
                              tax: _taxController.text,
                              phone: _phoneController.text,
                              company: _companyController.text,
                              title: titlePicked,
                              note: _noteController.text, companyId: widget.companyId, id: widget.id, deadline: deadline,
                            )));
                      }
                    },
                    child: Container(
                      color: Color.fromRGBO(55, 86, 223, 1),
                      width: width * 0.8,
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                      child: Row(
                        children: [
                          Spacer(),
                          Container(
                              margin: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context).next.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white),
                              )),
                          Spacer(),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Image(
                                image:
                                AssetImage("assets/images/arrowGray.png"),
                              )),
                        ],
                      ),
                    ),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0))),
                SizedBox(
                  height: 10,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

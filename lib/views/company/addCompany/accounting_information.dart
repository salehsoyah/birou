import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/models/activity.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/currency.dart';
import 'package:birou/models/device.dart';
import 'package:birou/models/exercice.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/company/addCompany/financial_information.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AccountingInformation extends StatefulWidget {
  @override
  _AccountingInformationState createState() => _AccountingInformationState();

  String companyName;
  String logo;
  String pdfLanguage;
  String phone;
  String website;
  bool hasCompanies;

  AccountingInformation(
      {Key key,
      this.companyName,
      this.logo,
      this.pdfLanguage,
      this.phone,
      this.website,
      this.hasCompanies})
      : super(key: key);
}

class _AccountingInformationState extends State<AccountingInformation> {
  final _taxIdentificationController = TextEditingController();

  String activity;
  String exercice;
  String currency;

  String activityTitle;

  String exerciceTitle;

  String currencyTitle;

  List<Activity> activities = [];
  List<Exercice> exercices = [];
  List<Currency> currencies = [];

  bool btnPressed = false;

  GlobalKey<FormState> _form = GlobalKey<FormState>();
  final _formKey = GlobalKey<FormState>();

  Future listActivities;
  Future listExercices;
  Future listCurrency;

  @override
  void initState() {
    activitiesList().then((result) {
      print(result);
      setState(() {
        activities = result;
        if(result.length > 0){
          activity = result[0].hashed_id;
          activityTitle = result[0].title;
        }
      });
    });
    companyCurrencies().then((result) {
      setState(() {
        currencies = result;
        if(result.length > 0){
          currency = result[0].hashed_id;
          currencyTitle = result[0].title;
        }
      });
    });
    exercicesList().then((result) {
      setState(() {
        exercices = result;
        if(result.length > 0){
          exercice = result[0].hashed_id;
          exerciceTitle = result[0].title;
        }
      });
    });
    listActivities = activitiesList();
    listExercices = exercicesList();
    listCurrency = companyCurrencies();

    super.initState();
  }

  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).new_company,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
            widget.hasCompanies == true
                ? Icon(
                    Icons.logout,
                    color: Colors.transparent,
                  )
                : Icon(
                    Icons.logout,
                    color: Colors.blueAccent,
                  ),
          ],
        ),
      ),
      body: Container(
        height: height,
        color: Color.fromRGBO(245, 246, 252, 1),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: width / 11,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: width / 11.5,
                    ),
                    Text(
                      AppLocalizations.of(context).accounting_information,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey[600],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.84,
                  height: width * 0.88,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).activity,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  activityTitle != null
                                      ? activityTitle
                                      : AppLocalizations.of(context)
                                          .choose_an_activity,
                                  style: TextStyle(fontSize: 13),
                                ),
                              ],
                            ),
                            FutureBuilder(
                              future: listActivities,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text("loading...");
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                                value: value.hashed_id,
                                                child: Text(
                                                  value.title,
                                                ),
                                              ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      activity = value;
                                    });
                                    print(activity);

                                    for (var i in activities) {
                                      if (value == i.hashed_id) {
                                        setState(() {
                                          activityTitle = i.title;
                                        });
                                        print(activityTitle);
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1,
                        ),
                        TextFormField(
                          // validator: (value) {
                          //   if (value == null || value.isEmpty) {
                          //     return 'Please enter tax identification number';
                          //   }
                          //   return null;
                          // },
                          controller: _taxIdentificationController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText:
                                  AppLocalizations.of(context).tax_id_number,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).exercice,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  exerciceTitle != null
                                      ? exerciceTitle
                                      : AppLocalizations.of(context)
                                          .choose_exercice,
                                  style: TextStyle(fontSize: 13),
                                ),
                              ],
                            ),
                            FutureBuilder(
                              future: listExercices,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text("loading...");
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                                value: value.hashed_id,
                                                child: Text(
                                                  value.title,
                                                ),
                                              ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      exercice = value;
                                    });

                                    for (var i in exercices) {
                                      if (value == i.hashed_id) {
                                        setState(() {
                                          exerciceTitle = i.title;
                                        });
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).currency,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  currencyTitle != null
                                      ? currencyTitle
                                      : AppLocalizations.of(context)
                                          .main_currency,
                                  style: TextStyle(fontSize: 13),
                                ),
                              ],
                            ),
                            FutureBuilder(
                              future: listCurrency,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text("loading...");
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                                value: value.hashed_id,
                                                child: Text(
                                                  value.title,
                                                ),
                                              ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      currency = value;
                                    });

                                    for (var i in currencies) {
                                      if (value == i.hashed_id) {
                                        setState(() {
                                          currencyTitle = i.title;
                                        });
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        SizedBox(
                          height: width / 30,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 3.5,
                ),
                RaisedButton(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    onPressed: () async {
                      setState(() {
                        btnPressed = true;
                      });
                      if (_formKey.currentState.validate()) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                FinancialInformation(
                                  companyName: widget.companyName,
                                  logo: widget.logo,
                                  pdfLanguage: widget.pdfLanguage,
                                  phone: widget.phone,
                                  website: widget.website,
                                  activity: activity,
                                  tax: _taxIdentificationController.text,
                                  exercice: exercice,
                                  currency: currency,
                                  hasCompanies: widget.hasCompanies,
                                )));
                      }
                    },
                    child: Container(
                      color: Color.fromRGBO(55, 86, 223, 1),
                      width: width * 0.8,
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                      child: Row(
                        children: [
                          Spacer(),
                          Container(
                              margin: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)
                                    .financial_information
                                    .toUpperCase(),
                                style: TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white),
                              )),
                          Spacer(),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Image(
                                image:
                                    AssetImage("assets/images/arrowGray.png"),
                              )),
                        ],
                      ),
                    ),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0))),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/device.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/dialog/company.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FinancialInformation extends StatefulWidget {
  @override
  _FinancialInformationState createState() => _FinancialInformationState();

  String companyName;
  String logo;
  String pdfLanguage;
  String phone;
  String website;

  String activity;
  String tax;
  String exercice;
  String currency;

  bool hasCompanies;

  FinancialInformation(
      {Key key,
      this.companyName,
      this.logo,
      this.pdfLanguage,
      this.phone,
      this.website,
      this.activity,
      this.tax,
      this.exercice,
      this.currency,
      this.hasCompanies})
      : super(key: key);
}

class _FinancialInformationState extends State<FinancialInformation> {
  final _bankController = TextEditingController();
  final _bicController = TextEditingController();
  final _ribController = TextEditingController();

  List<Country> countries = [];
  String country;
  String countryText;
  bool btnPressed = false;
  Future listCountries;
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  void initState() {
    countryList().then((result) {
      countries = result;
      setState(() {
        if(result.length > 0){
          country = result[0].hashed_id;
          countryText = result[0].title;
          print(result[0].title);
        }
      });
    });
    listCountries = countryList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).new_company,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
            widget.hasCompanies == true
                ? Icon(
                    Icons.logout,
                    color: Colors.transparent,
                  )
                : Icon(
                    Icons.logout,
                    color: Colors.blueAccent,
                  ),
          ],
        ),
      ),
      body: loading == false
          ? Container(
              height: height,
              color: Color.fromRGBO(245, 246, 252, 1),
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      SizedBox(
                        height: width / 11,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: width / 11.5,
                          ),
                          Text(
                            AppLocalizations.of(context).financial_information,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.grey[600],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: width / 11,
                      ),
                      Container(
                        width: width * 0.84,
                        height: width * 0.88,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.blue[100],
                                spreadRadius: 0.5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 1), // changes position of shadow
                              ),
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 25),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextFormField(
                                controller: _bankController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context)
                                        .bank
                                        .toUpperCase(),
                                    hintStyle: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 17,
                                        fontWeight: FontWeight.normal)),
                              ),
                              Divider(
                                color: Colors.grey[300],
                                thickness: 1,
                              ),
                              TextFormField(
                                controller: _bicController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context)
                                        .bic
                                        .toUpperCase(),
                                    hintStyle: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 17,
                                        fontWeight: FontWeight.normal)),
                              ),
                              Divider(
                                color: Colors.grey[300],
                                thickness: 1.0,
                              ),
                              TextFormField(
                                controller: _ribController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context)
                                        .rib
                                        .toUpperCase(),
                                    hintStyle: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 17,
                                        fontWeight: FontWeight.normal)),
                              ),
                              Divider(
                                color: Colors.grey[300],
                                thickness: 1.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: width / 39,
                                      ),
                                      Text(
                                        AppLocalizations.of(context).country,
                                        style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.grey[500]),
                                      ),
                                      SizedBox(
                                        height: width / 70,
                                      ),
                                      Text(
                                        countryText != null
                                            ? countryText
                                            : AppLocalizations.of(context)
                                                .choose_country,
                                        style: TextStyle(fontSize: 13),
                                      ),
                                    ],
                                  ),
                                  FutureBuilder(
                                    future: listCountries,
                                    builder: (BuildContext context,
                                        AsyncSnapshot snapshot) {
                                      if (!snapshot.hasData) {
                                        return Text("loading...");
                                      }
                                      return PopupMenuButton<String>(
                                        icon: Icon(
                                          Icons.arrow_drop_down,
                                          color: Colors.blueAccent,
                                          size: 35,
                                        ),
                                        itemBuilder: (context) => snapshot.data
                                            .map<PopupMenuItem<String>>(
                                                (value) =>
                                                    PopupMenuItem<String>(
                                                      value: value.hashed_id,
                                                      child: Text(
                                                        value.title,
                                                      ),
                                                    ))
                                            .toList(),
                                        onSelected: (value) {
                                          setState(() {
                                            country = value;
                                          });

                                          for (var i in countries) {
                                            if (value == i.hashed_id) {
                                              setState(() {
                                                countryText = i.title;
                                              });
                                            }
                                          }
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: width / 30,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: width / 3.5,
                      ),
                      RaisedButton(
                          color: Color.fromRGBO(55, 86, 223, 1),
                          onPressed: () async {
                            setState(() {
                              btnPressed = true;
                            });

                            if (_formKey.currentState.validate()) {
                              setState(() {
                                loading = true;
                              });
                              print(widget.companyName);
                              print(widget.logo);
                              print(widget.pdfLanguage);
                              print(widget.phone);
                              print(widget.website);
                              print(widget.activity);
                              print(widget.tax);
                              print(widget.exercice);
                              print(widget.currency);
                              print(_bankController.text);
                              print(_bicController.text);
                              print(_ribController.text);
                              print(country);
                              SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();
                              String userId =
                                  sharedPreferences.getString('userId');
                              newCompany(
                                      widget.companyName,
                                      widget.website != null
                                          ? Uri.parse(widget.website)
                                          : null,
                                      country,
                                      "",
                                      "",
                                      "",
                                      widget.pdfLanguage,
                                      widget.logo,
                                      widget.activity,
                                      widget.tax,
                                      widget.phone != null
                                          ? int.parse(widget.phone)
                                          : null,
                                      widget.currency,
                                      widget.exercice,
                                      _bankController.text,
                                      _bicController.text,
                                      _ribController.text,
                                      _ribController.text)
                                  .then((result) async {
                                setState(() {
                                  loading = false;
                                });
                                print(result);
                                // List<Company> companies = [];
                                // SharedPreferences sharedPreferences =
                                //     await SharedPreferences.getInstance();
                                // if (sharedPreferences.getString("companiesList") !=
                                //     null) {
                                //   for (var u in jsonDecode(
                                //       sharedPreferences.getString("companiesList"))) {
                                //     Company company = Company(
                                //         u['title'], u['hashed_id'], u['user_name']);
                                //     companies.add(company);
                                //   }
                                // }
                                userCompanies().then((result) {
                                  if (result['status']['code'] == 200) {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                MainMenu()));
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return CompanyDialog(
                                            image: 1,
                                            text1: 'Success',
                                            text2: result['message'],
                                          );
                                        });
                                  } else {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return CompanyDialog(
                                            image: 0,
                                            text1: 'Error',
                                            text2: result['message'],
                                          );
                                        });
                                  }
                                });
                              });
                            }
                          },
                          child: Container(
                            color: Color.fromRGBO(55, 86, 223, 1),
                            width: width * 0.8,
                            padding:
                                const EdgeInsets.only(top: 20.0, bottom: 20),
                            child: Row(
                              children: [
                                Spacer(),
                                Container(
                                    margin: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .finish
                                          .toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white),
                                    )),
                                Spacer(),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Image(
                                      image: AssetImage(
                                          "assets/images/arrowGray.png"),
                                    )),
                              ],
                            ),
                          ),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                    ],
                  ),
                ),
              ),
            )
          : Container(
              height: height,
              child: SpinKitFadingCircle(
                itemBuilder: (BuildContext context, int index) {
                  return DecoratedBox(
                    decoration: BoxDecoration(
                      color: index.isEven ? Colors.yellow : Colors.blue,
                    ),
                  );
                },
              ),
            ),
    );
  }
}

import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:io' as Io;

import 'package:birou/controllers/companyController.dart';
import 'package:birou/generated/l10n.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/currency.dart';
import 'package:birou/models/language.dart';
import 'package:birou/models/type.dart';
import 'package:birou/models/use.dart';
import 'package:birou/views/company/additional/inputs.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:form_validator/form_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class UpdateInput extends StatefulWidget {
  @override
  _UpdateInputState createState() => _UpdateInputState();
  String id;
  String title;
  String value;
  int type;
  int use;
  int condition;
  String country;
  String currency;
  String companyId;

  UpdateInput({
    Key key,
    @required this.title,
    @required this.value,
    @required this.type,
    @required this.use,
    @required this.condition,
    @required this.country,
    @required this.currency,
    @required this.companyId,
    @required this.id,
  }) : super(key: key);
}

class _UpdateInputState extends State<UpdateInput> {
  final _titleController = TextEditingController();
  final _rateController = TextEditingController();

  GlobalKey<FormState> _form = GlobalKey<FormState>();
  bool loading = false;
  int type;
  String typeText;
  List<Type> types = [
    Type("%", 1),
    Type("0.00", 0),
  ];

  int use;
  String useText;
  List<Use> uses = [
    Use("Manual", 0),
    Use("Everywhere", 1),
    Use("Conditional", 2),
  ];

  int condition;
  String conditionText;
  List<Condition> conditions = [
    Condition("Par Pays", 1),
    Condition("Par Devise", 2),
  ];

  int open = 0;

  List<Country> countries = [];
  String country;

  String countryText = '';
  bool btnPressed = false;
  Future listCountries;

  List<Currency> currencies = [];
  String currencyTitle;
  String currency;
  Future listCurrency;

  void initState() {
    listCountries = companyCountries();
    listCurrency = companyCurrencies();
    print(widget.type);
    setState(() {
      _titleController.text = widget.title;
      _rateController.text = widget.value;
      type = widget.type;
      use = widget.use;
      condition = widget.condition;
      currency = 'Dl';
      country = 'Dl';

      for (var i in types) {
        if (widget.type == i.hashed_id) {
          setState(() {
            typeText = i.title;
          });
        }
      }

      for (var i in uses) {
        if (widget.type == i.hashed_id) {
          setState(() {
            useText = i.title;
          });
        }
      }

      for (var i in conditions) {
        if (widget.type == i.condition) {
          setState(() {
            conditionText = i.title;
          });
        }
      }
    });

    companyCountries().then((result) {
      setState(() {
        countries = result;
        if (countries.length > 0) {
          country = countries[0].hashed_id;
          countryText = countries[0].title;
        }
      });

      print(countries[0].hashed_id);
    });

    companyCurrencies().then((value) {
      setState(() {
        currencies = value;
        if (currencies.length > 0) {
          currency = currencies[0].hashed_id;
          currencyTitle = currencies[0].title;
        }
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    var width = MediaQuery.of(context).size.width;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.blueAccent,
            )),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).update_input,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),

          ],
        ),
      ),
      body: loading == false
          ? Container(
              height: height,
              color: Color.fromRGBO(245, 246, 252, 1),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: width / 11,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: width / 11.5,
                        ),
                        Text(
                          AppLocalizations.of(context).information,
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.grey[600],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: width / 11,
                    ),
                    Container(
                      width: width * 0.84,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.blue[100],
                              spreadRadius: 0.5,
                              blurRadius: 7,
                              offset:
                                  Offset(0, 1), // changes position of shadow
                            ),
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 25),
                        child: Form(
                          key: _form,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return AppLocalizations.of(context).enter_title;
                                  }
                                  return null;
                                },
                                controller: _titleController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context).title,
                                    hintStyle: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 17,
                                        fontWeight: FontWeight.normal)),
                              ),
                              Divider(
                                color: Colors.grey[300],
                                thickness: 1.0,
                              ),
                              TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return AppLocalizations.of(context).enter_rate;
                                  }
                                  return null;
                                },
                                controller: _rateController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context).rate,
                                    hintStyle: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 17,
                                        fontWeight: FontWeight.normal)),
                              ),
                              Divider(
                                color: Colors.grey[300],
                                thickness: 1.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: width / 39,
                                      ),
                                      Text(
                                          AppLocalizations.of(context).type,
                                        style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.grey[500]),
                                      ),
                                      SizedBox(
                                        height: width / 70,
                                      ),
                                      Text(
                                        typeText != null
                                            ? typeText
                                            : AppLocalizations.of(context).choose_type,
                                        style: TextStyle(fontSize: 13),
                                      ),
                                      btnPressed && typeText == null
                                          ? Text(
                                          AppLocalizations.of(context).enter_type,
                                              style: TextStyle(
                                                  color: Colors.red,
                                                  fontSize: 12),
                                            )
                                          : Container()
                                    ],
                                  ),
                                  PopupMenuButton<int>(
                                    icon: Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.blueAccent,
                                      size: 35,
                                    ),
                                    onSelected: (int result) {
                                      setState(() {
                                        type = result;
                                      });

                                      for (var i in types) {
                                        if (i.hashed_id == result) {
                                          print(i.title);
                                          setState(() {
                                            typeText = i.title;
                                          });
                                        }
                                      }
                                    },
                                    itemBuilder: (BuildContext context) =>
                                        <PopupMenuEntry<int>>[
                                      PopupMenuItem<int>(
                                        value: types[0].hashed_id,
                                        child: Text(types[0].title),
                                      ),
                                      PopupMenuItem<int>(
                                        value: types[1].hashed_id,
                                        child: Text(types[1].title),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Divider(
                                color: Colors.grey[300],
                                thickness: 1.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: width / 39,
                                      ),
                                      Text(
                                        AppLocalizations.of(context).use,
                                        style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.grey[500]),
                                      ),
                                      SizedBox(
                                        height: width / 70,
                                      ),
                                      Text(
                                        useText != null
                                            ? useText
                                            : AppLocalizations.of(context).choose_use,
                                        style: TextStyle(fontSize: 13),
                                      ),
                                      // btnPressed && pdfLanguage == null ? Text("Please choose a type", style: TextStyle(
                                      //     color: Colors.red,
                                      //     fontSize: 12
                                      // ),
                                      // ) : Container()
                                    ],
                                  ),
                                  PopupMenuButton<int>(
                                    icon: Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.blueAccent,
                                      size: 35,
                                    ),
                                    onSelected: (int result) {
                                      setState(() {
                                        use = result;
                                        if (result == 2) {
                                          open = 1;
                                        } else {
                                          setState(() {
                                            open = 0;
                                          });
                                        }
                                      });

                                      for (var i in uses) {
                                        if (i.hashed_id == result) {
                                          print(i.title);
                                          setState(() {
                                            useText = i.title;
                                          });
                                        }
                                      }
                                    },
                                    itemBuilder: (BuildContext context) =>
                                        <PopupMenuEntry<int>>[
                                      PopupMenuItem<int>(
                                        value: uses[0].hashed_id,
                                        child: Text(AppLocalizations.of(context).manual),
                                      ),
                                      PopupMenuItem<int>(
                                        value: uses[1].hashed_id,
                                        child: Text(AppLocalizations.of(context).everywhere),
                                      ),
                                      PopupMenuItem<int>(
                                        value: uses[2].hashed_id,
                                        child: Text(AppLocalizations.of(context).conditional),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              open == 1
                                  ? Container(
                                      child: Column(
                                        children: [
                                          Divider(
                                            color: Colors.grey[300],
                                            thickness: 1.0,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  SizedBox(
                                                    height: width / 39,
                                                  ),
                                                  Text(
                                                    AppLocalizations.of(context).condition,
                                                    style: TextStyle(
                                                        fontSize: 17,
                                                        color:
                                                            Colors.grey[500]),
                                                  ),
                                                  SizedBox(
                                                    height: width / 70,
                                                  ),
                                                  Text(
                                                    conditionText != null
                                                        ? conditionText
                                                        : AppLocalizations.of(context).choose_condition,
                                                    style:
                                                        TextStyle(fontSize: 13),
                                                  ),
                                                  // btnPressed && pdfLanguage == null ? Text("Please choose a type", style: TextStyle(
                                                  //     color: Colors.red,
                                                  //     fontSize: 12
                                                  // ),
                                                  // ) : Container()
                                                ],
                                              ),
                                              PopupMenuButton<int>(
                                                icon: Icon(
                                                  Icons.arrow_drop_down,
                                                  color: Colors.blueAccent,
                                                  size: 35,
                                                ),
                                                onSelected: (int result) {
                                                  setState(() {
                                                    condition = result;
                                                  });

                                                  for (var i in conditions) {
                                                    if (i.condition == result) {
                                                      print(i.title);
                                                      setState(() {
                                                        conditionText = i.title;
                                                      });
                                                    }
                                                  }
                                                },
                                                itemBuilder:
                                                    (BuildContext context) =>
                                                        <PopupMenuEntry<int>>[
                                                  PopupMenuItem<int>(
                                                    value:
                                                        conditions[0].condition,
                                                    child: Text(
                                                        AppLocalizations.of(context).by_country),
                                                  ),
                                                  PopupMenuItem<int>(
                                                    value:
                                                        conditions[1].condition,
                                                    child: Text(
                                                        AppLocalizations.of(context).by_currency),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey[300],
                                            thickness: 1.0,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  SizedBox(
                                                    height: width / 39,
                                                  ),
                                                  Text(
                                                    AppLocalizations.of(context)
                                                        .country,
                                                    style: TextStyle(
                                                        fontSize: 17,
                                                        color:
                                                            Colors.grey[500]),
                                                  ),
                                                  SizedBox(
                                                    height: width / 70,
                                                  ),
                                                  Text(
                                                    countryText != ''
                                                        ? countryText
                                                        : AppLocalizations.of(
                                                                context)
                                                            .choose_country,
                                                    style:
                                                        TextStyle(fontSize: 13),
                                                  ),
                                                  // btnPressed && country == null
                                                  //     ? Text(
                                                  //   "Please choose a country",
                                                  //   style: TextStyle(
                                                  //       color: Colors.red, fontSize: 12),
                                                  // )
                                                  //     : Container()
                                                ],
                                              ),
                                              FutureBuilder(
                                                future: listCountries,
                                                builder: (BuildContext context,
                                                    AsyncSnapshot snapshot) {
                                                  if (!snapshot.hasData) {
                                                    return Text(AppLocalizations.of(context).loading);
                                                  }
                                                  return PopupMenuButton<
                                                      String>(
                                                    icon: Icon(
                                                      Icons.arrow_drop_down,
                                                      color: Colors.blueAccent,
                                                      size: 35,
                                                    ),
                                                    itemBuilder: (context) =>
                                                        snapshot.data
                                                            .map<
                                                                    PopupMenuItem<
                                                                        String>>(
                                                                (value) =>
                                                                    PopupMenuItem<
                                                                        String>(
                                                                      value: value
                                                                          .hashed_id,
                                                                      child:
                                                                          Text(
                                                                        value
                                                                            .title,
                                                                      ),
                                                                    ))
                                                            .toList(),
                                                    onSelected: (value) {
                                                      print(value);
                                                      setState(() {
                                                        country = value;
                                                      });

                                                      for (var i in countries) {
                                                        if (value ==
                                                            i.hashed_id) {
                                                          setState(() {
                                                            countryText =
                                                                i.title;
                                                          });
                                                        }
                                                      }
                                                    },
                                                  );
                                                },
                                              ),
                                            ],
                                          ),
                                          Divider(
                                            color: Colors.grey[300],
                                            thickness: 1.0,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  SizedBox(
                                                    height: width / 39,
                                                  ),
                                                  Text(
                                                    AppLocalizations.of(context)
                                                        .currency,
                                                    style: TextStyle(
                                                        fontSize: 17,
                                                        color:
                                                            Colors.grey[500]),
                                                  ),
                                                  SizedBox(
                                                    height: width / 70,
                                                  ),
                                                  Text(
                                                    currencyTitle != null
                                                        ? currencyTitle
                                                        : AppLocalizations.of(
                                                                context)
                                                            .main_currency,
                                                    style:
                                                        TextStyle(fontSize: 13),
                                                  ),
                                                  // btnPressed && currency == null ? Text("Please choose a currency", style: TextStyle(
                                                  //     color: Colors.red,
                                                  //     fontSize: 12
                                                  // ),
                                                  // ) : Container()
                                                ],
                                              ),
                                              FutureBuilder(
                                                future: listCurrency,
                                                builder: (BuildContext context,
                                                    AsyncSnapshot snapshot) {
                                                  if (!snapshot.hasData) {
                                                    return Text(AppLocalizations.of(context).loading);
                                                  }
                                                  return PopupMenuButton<
                                                      String>(
                                                    icon: Icon(
                                                      Icons.arrow_drop_down,
                                                      color: Colors.blueAccent,
                                                      size: 35,
                                                    ),
                                                    itemBuilder: (context) =>
                                                        snapshot.data
                                                            .map<
                                                                    PopupMenuItem<
                                                                        String>>(
                                                                (value) =>
                                                                    PopupMenuItem<
                                                                        String>(
                                                                      value: value
                                                                          .hashed_id,
                                                                      child:
                                                                          Text(
                                                                        value
                                                                            .title,
                                                                      ),
                                                                    ))
                                                            .toList(),
                                                    onSelected: (value) {
                                                      setState(() {
                                                        currency = value;
                                                      });

                                                      for (var i
                                                          in currencies) {
                                                        if (value ==
                                                            i.hashed_id) {
                                                          setState(() {
                                                            currencyTitle =
                                                                i.title;
                                                          });
                                                        }
                                                      }
                                                    },
                                                  );
                                                },
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    )
                                  : Container(),
                              SizedBox(
                                height: 20,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height / 4,
                    ),
                    RaisedButton(
                        color: Color.fromRGBO(55, 86, 223, 1),
                        onPressed: () async {
                          setState(() {
                            btnPressed = true;
                          });
                          if (_form.currentState.validate()) {
                            setState(() {
                              loading = true;
                            });
                            editInput(
                                    _titleController.text,
                                    use,
                                    type,
                                    double.parse(_rateController.text),
                                    condition == null ? 0 : condition,
                                    country,
                                    currency,
                                    widget.id)
                                .then((result) {
                              setState(() {
                                loading = false;
                              });
                              if (result['status']['code'] == 200) {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            Inputs()));
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return DialogScreen(
                                        image: 1,
                                        text1: 'Success',
                                        text2: result['status']['message'],
                                      );
                                    });
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return DialogScreen(
                                        image: 0,
                                        text1: 'Error',
                                        text2: result['status']['message'],
                                      );
                                    });
                              }
                            });
                          }
                        },
                        child: Container(
                          color: Color.fromRGBO(55, 86, 223, 1),
                          width: width * 0.8,
                          padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                          child: Row(
                            children: [
                              Spacer(),
                              Container(
                                  margin: const EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    AppLocalizations.of(context).update.toUpperCase(),
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.normal,
                                        color: Colors.white),
                                  )),
                              Spacer(),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: Image(
                                    image: AssetImage(
                                        "assets/images/arrowGray.png"),
                                  )),
                            ],
                          ),
                        ),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0))),
                    SizedBox(
                      height: 10,
                    )
                  ],
                ),
              ),
            )
          : Container(
              alignment: Alignment.bottomCenter,
              height: MediaQuery.of(context).size.height / 2,
              child: spin()),
    );
  }

  Widget spin() {
    return Padding(
      padding: EdgeInsets.only(top: 200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Text(
              "Birou",
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 27,
                  fontWeight: FontWeight.w500),
            ),
          ),
          SpinKitCubeGrid(
            itemBuilder: (BuildContext context, int index) {
              return DecoratedBox(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.1, 0.4, 0.7, 0.9],
                colors: [
                  Colors.blueAccent,
                  Colors.redAccent,
                  Colors.greenAccent,
                  Colors.grey,
                ],
              )));
            },
            size: 70.0,
          ),
        ],
      ),
    );
  }
}

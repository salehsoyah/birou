import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:io' as Io;

import 'package:birou/controllers/companyController.dart';
import 'package:birou/generated/l10n.dart';
import 'package:birou/models/language.dart';
import 'package:birou/views/company/collaborators/collaborators.dart';
import 'package:birou/views/company/tax/taxs.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:form_validator/form_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddCollaborator extends StatefulWidget {
  @override
  _AddCollaboratorState createState() => _AddCollaboratorState();

  String companyId;

  AddCollaborator({Key key, this.companyId});
}

class _AddCollaboratorState extends State<AddCollaborator> {
  final _titleController = TextEditingController();
  final _rateController = TextEditingController();

  GlobalKey<FormState> _form = GlobalKey<FormState>();
  bool loading = false;


  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.blueAccent,
            )),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).new_collaborator,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),

          ],
        ),
      ),
      body: loading == false ? SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(245, 246, 252, 1),
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              SizedBox(
                height: width / 11,
              ),
              Row(
                children: [
                  SizedBox(
                    width: width / 11.5,
                  ),
                  Text(
                    AppLocalizations.of(context).information,
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey[600],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.84,
                height: width * 0.42,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Form(
                    key: _form,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return AppLocalizations.of(context).enter_name;
                            }
                            return null;
                          },
                          controller: _titleController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).name,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return AppLocalizations.of(context).enter_email_other;
                            }
                            return null;
                          },
                          controller: _rateController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).email,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: width / 1.5,
              ),
              RaisedButton(
                  color: Color.fromRGBO(55, 86, 223, 1),
                  onPressed: () async {
                    if (_form.currentState.validate()) {
                      setState(() {
                        loading = true;
                      });
                      newCollaborator(_titleController.text, _rateController.text).then((result){
                        print(result);
                        setState(() {
                          loading = false;
                        });
                        if(result['status']['code'] == 200)
                        {
                          print(result);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) => Collaborators(

                              )));
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return DialogScreen(image: 1, text1: 'Success', text2: result['status']['message'], );
                              });
                        }else{
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return DialogScreen(image: 0, text1: 'Error', text2: result['status']['message'], );
                              });
                        }
                      });
                    }
                  },
                  child: Container(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    width: width * 0.8,
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        Container(
                            margin: const EdgeInsets.only(left: 10.0),
                            child: Text(
                              AppLocalizations.of(context).add.toUpperCase(),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            )),
                        Spacer(),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: AssetImage("assets/images/arrowGray.png"),
                            )),
                      ],
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0))),
            ],
          ),
        ),
      ) : Container(
          alignment: Alignment.bottomCenter,
          height: MediaQuery.of(context).size.height / 2, child: spin()),
    );
  }

  Widget spin() {
    return Padding(
      padding: EdgeInsets.only(top: 200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Text("Birou", style: TextStyle(
                color: Colors.blueAccent,
                fontSize: 27,
                fontWeight: FontWeight.w500
            ),),
          ),
          SpinKitCubeGrid(
            itemBuilder: (BuildContext context, int index){
              return DecoratedBox(decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.1, 0.4, 0.7, 0.9],
                    colors: [
                      Colors.blueAccent,
                      Colors.redAccent,
                      Colors.greenAccent,
                      Colors.grey,
                    ],
                  )
              ));
            },
            size: 70.0,
          ),
        ],
      ),
    );
  }

}

import 'dart:math';

import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/contactsController.dart';
import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/models/deadline.dart';
import 'package:birou/views/company/additional/addInput.dart';
import 'package:birou/views/company/additional/updateInput.dart';
import 'package:birou/views/company/collaborators/addCollaborator.dart';
import 'package:birou/views/dialog/deleteInput.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:bubbled_navigation_bar/bubbled_navigation_bar.dart';
import 'package:clay_containers/clay_containers.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ionicons/ionicons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Collaborators extends StatefulWidget {
  @override
  String companyId;
  final List<Company> companies;

  Collaborators({Key key, this.companies, this.companyId});

  _CollaboratorsState createState() => _CollaboratorsState();
}

class _CollaboratorsState extends State<Collaborators> {
  @override
  String full_name;

  bool clientList = true;
  bool providerList = false;
  int itemCount = 4;
  double listBuilderHeightClient = 350.0;
  double listBuilderHeightProvider = 350.0;

  final spinkit = SpinKitFadingCircle(
    color: Colors.blueAccent,
    size: 50.0,
  );

  getUserName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("full_name");
  }

  Future collaborators;
  int condition;
  String conditionText;
  List<Condition> conditions = [
    Condition("Update", 1),
    Condition("Delete", 2),
  ];

  @override
  void initState() {
    // getUserName().then((result) {
    //   setState(() {
    //     full_name = result;
    //   });
    // });
    collaborators = companyCollaborators();

    companyCollaborators().then((result) {
      print(result);
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Color.fromRGBO(245, 246, 252, 1),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 18.0, top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => MainMenu(
                                  companies: widget.companies,
                                )));
                      },
                      child: Container(
                        width: 18,
                        height: 18,
                        child: Image.asset(
                          "assets/images/menu.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 30,
                        child: ClipOval(
                          child: Image.asset(
                            'assets/images/person.png',
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: Text(
                  AppLocalizations.of(context).collaborators,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: width / 15,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 30.0, left: 18.0, right: 18.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      AppLocalizations.of(context).synthesis_collaborators,
                      style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AddCollaborator()));
                      },
                      child: Icon(
                        Icons.add,
                        size: width / 14,
                        color: Colors.grey[700],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Card(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Column(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              FutureBuilder(
                                future: collaborators,
                                builder: (BuildContext context,
                                    AsyncSnapshot<dynamic> snapshot) {
                                  if (snapshot.data == null) {
                                    return Container(
                                      height: width * 0.5,
                                      alignment: Alignment.center,
                                      child: Center(
                                        child: spinkit,
                                      ),
                                    );
                                  } else if (snapshot.data.length < 1) {
                                    return Container(
                                      height: width * 0.25,
                                      alignment: Alignment.center,
                                      child: Center(
                                        child: Text(AppLocalizations.of(context)
                                            .no_data_found),
                                      ),
                                    );
                                  } else {
                                    return Column(
                                      children: [
                                        ListView.builder(
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount: snapshot.data.length > 3
                                              ? itemCount
                                              : snapshot.data.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Deadline(
                                                snapshot.data[index].name,
                                                (index + 1).toString(),
                                                snapshot.data[index].email,
                                                snapshot.data[index].role,
                                                snapshot.data[index].id);
                                          },
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        snapshot.data.length - itemCount > 0 && snapshot.data.length > 3
                                            ? Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: GestureDetector(
                                                  onTap: () {
                                                    setState(() {
                                                      snapshot.data.length -
                                                          itemCount >=
                                                          2
                                                          ? itemCount =
                                                          itemCount + 2 : itemCount =
                                                          itemCount +
                                                              snapshot.data.length -
                                                              itemCount;
                                                    });
                                                  },
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                        .fromLTRB(8, 8, 20, 8),
                                                    child: Text(
                                                      AppLocalizations.of(
                                                              context)
                                                          .load_more,
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.normal,
                                                          color: Colors
                                                              .blueAccent),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            : Container()
                                      ],
                                    );
                                  }
                                },
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Deadline(String name, index, String email, int role, String id) {
    return Container(
        child: ListTile(
      leading: Text(
        "#" + index,
        style: TextStyle(fontSize: 14),
      ),
      title: Text(
        name,
        style: TextStyle(
            color: Colors.blueAccent,
            fontWeight: FontWeight.normal,
            fontSize: 16),
      ),
      subtitle: Text(
        email,
        style: TextStyle(
            color: Colors.grey[500],
            fontWeight: FontWeight.normal,
            fontSize: 11),
      ),
      trailing: Container(
        width: 135,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              role == 1 ? 'Owner' : 'Collaborator',
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 15),
            ),
            PopupMenuButton<int>(
              icon: Icon(
                Icons.arrow_drop_down,
                color: Colors.blueAccent,
                size: 35,
              ),
              onSelected: (int result) {
                // if (result == 1) {
                //   Navigator.of(context).push(MaterialPageRoute(
                //       builder: (BuildContext context) => UpdateInput(
                //             title: title,
                //             use: use,
                //             country: country,
                //             value: value,
                //             id: id,
                //             currency: currency,
                //             condition: condition,
                //             type: type,
                //           )));
                // } else if (result == 2) {
                //   showDialog(
                //       context: context,
                //       builder: (BuildContext context) {
                //         return DeleteInput(id: id);
                //       });
                // }
              },
              itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
                PopupMenuItem<int>(
                  value: conditions[0].condition,
                  child: Text(AppLocalizations.of(context).update),
                ),
                PopupMenuItem<int>(
                  value: conditions[1].condition,
                  child: Text(AppLocalizations.of(context).delete),
                ),
              ],
            )
          ],
        ),
      ),
    ));
  }
}

import 'dart:math';

import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/contactsController.dart';
import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/deadline.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:bubbled_navigation_bar/bubbled_navigation_bar.dart';
import 'package:clay_containers/clay_containers.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ionicons/ionicons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Logs extends StatefulWidget {
  @override
  String companyId;
  final List<Company> companies;

  Logs({Key key, this.companies, this.companyId});

  _LogsState createState() => _LogsState();
}

class _LogsState extends State<Logs> {
  @override
  String full_name;

  bool clientList = true;
  bool providerList = false;
  int itemCount = 4;
  double listBuilderHeightClient = 350.0;
  double listBuilderHeightProvider = 350.0;

  final spinkit = SpinKitFadingCircle(
    color: Colors.blueAccent,
    size: 50.0,
  );

  getUserName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("full_name");
  }

  Future logs;

  @override
  void initState() {
    logs = companyLogs();

    companyLogs().then((dd) {
      print(dd);
    });

    super.initState();
  }

  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Color.fromRGBO(245, 246, 252, 1),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 18.0, top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => MainMenu(
                                  companies: widget.companies,
                                )));
                      },
                      child: Container(
                        width: 18,
                        height: 18,
                        child: Image.asset(
                          "assets/images/menu.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 30,
                        child: ClipOval(
                          child: Image.asset(
                            'assets/images/person.png',
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: Text(
                  AppLocalizations.of(context).logs,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: width / 15,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Card(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Column(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              FutureBuilder(
                                future: logs,
                                builder: (BuildContext context,
                                    AsyncSnapshot<dynamic> snapshot) {
                                  if (snapshot.data == null) {
                                    return Container(
                                      height: width * 0.5,
                                      alignment: Alignment.center,
                                      child: Center(
                                        child: spinkit,
                                      ),
                                    );
                                  } else if (snapshot.data.length < 1) {
                                    return Container(
                                      height: width * 0.25,
                                      alignment: Alignment.center,
                                      child: Center(
                                        child: Text(AppLocalizations.of(context)
                                            .no_data_found),
                                      ),
                                    );
                                  } else {
                                    return Column(
                                      children: [
                                        ListView.builder(
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount: snapshot.data.length > 3
                                              ? itemCount
                                              : snapshot.data.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Log(
                                                snapshot.data[index].element,
                                                snapshot.data[index].action
                                                    .toString(),
                                                (index + 1).toString(),
                                                snapshot.data[index].module);
                                          },
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        snapshot.data.length - itemCount > 0 &&
                                                snapshot.data.length > 3
                                            ? Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      snapshot.data.length -
                                                                  itemCount >=
                                                              2
                                                          ? itemCount =
                                                              itemCount + 2
                                                          : itemCount =
                                                              itemCount +
                                                                  snapshot.data
                                                                      .length -
                                                                  itemCount;
                                                    });
                                                  },
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                        .fromLTRB(8, 8, 20, 8),
                                                    child: Text(
                                                      AppLocalizations.of(
                                                              context)
                                                          .load_more,
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.normal,
                                                          color: Colors
                                                              .blueAccent),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            : Container()
                                      ],
                                    );
                                  }
                                },
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Log(String element, action, index, module) {
    return Container(
        child: ListTile(
      leading: Text(
        "#" + index,
        style: TextStyle(fontSize: 14),
      ),
      title: Text(
        element,
        style: TextStyle(
            color: Colors.blueAccent,
            fontSize: 16,
            fontWeight: FontWeight.normal),
      ),
      subtitle: Text(
        module,
        style: TextStyle(
            color: Colors.grey[500],
            fontSize: 11,
            fontWeight: FontWeight.normal),
      ),
      trailing: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.green,
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 3, 8, 3),
          child: Text(
            action.toString(),
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.normal,
                color: Colors.white),
          ),
        ),
      ),
    ));
  }
}

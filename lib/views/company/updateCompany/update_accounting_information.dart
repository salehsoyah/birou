import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/models/activity.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/currency.dart';
import 'package:birou/models/device.dart';
import 'package:birou/models/exercice.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/company/updateCompany/update_financial_information.dart';
import 'package:birou/views/company/updateCompany/update_general_information.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class UpdateAccountingInformation extends StatefulWidget {
  @override
  _UpdateAccountingInformationState createState() => _UpdateAccountingInformationState();

  String companyName;
  String logo;
  String pdfLanguage;
  String phone;
  String website;
  String companyId;
  final List<Company> companies;



  UpdateAccountingInformation(
      {Key key,
      @required this.companyName,
      @required this.logo,
      @required this.pdfLanguage,
      @required this.phone,
      @required this.website,
        @required this.companyId,
        this.companies,
      })
      : super(key: key);
}

class _UpdateAccountingInformationState extends State<UpdateAccountingInformation> {
  final _taxIdentificationController = TextEditingController();

  String activity;
  String exercice;
  String currency;

  String activityTitle ;
  String exerciceTitle ;
  String currencyTitle ;

  List<Activity> activities = [];
  List<Exercice> exercices = [];
  List<Currency> currencies = [];

  Future listActivities;
  Future listExercices;
  Future listCurrency;
  @override
  void initState() {
    listActivities = companyActivities(widget.companyId);
    listExercices = exercicesList();
    listCurrency = currencyList();

    getCompany(widget.companyId).then((result) {
      setState(() {
        activity = result['data']['company']['hashed_activity_id'];
        companyActivities(widget.companyId).then((result){
          setState(() {
            activities = result;
            for (var i in activities) {
              if (activity == i.hashed_id) {
                print(activity);
                setState(() {
                  activityTitle = i.title;
                });
              }
            }
          });
        });
        _taxIdentificationController.text = result['data']['company']['fiscal_id'];
        exercice = result['data']['company']['hashed_accounting_period_id'];
        exercicesList().then((result){
          setState(() {
            exercices = result;
            for (var i in exercices) {
              if (exercice == i.hashed_id) {
                print(exercice);
                setState(() {
                  exerciceTitle = i.title;
                });
              }
            }
          });
        });
        currency = result['data']['company']['hashed_default_currency_id'];
        currencyList().then((result){
          setState(() {
            currencies = result;
            for (var i in currencies) {
              if (currency == i.hashed_id) {
                print(currency);
                setState(() {
                  currencyTitle = i.title;
                });
              }
            }
          });
        });
      });
    });









    super.initState();
  }

  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).update_company,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),

          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(245, 246, 252, 1),
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              SizedBox(
                height: width / 11,
              ),
              Row(
                children: [
                  SizedBox(
                    width: width / 11.5,
                  ),
                  Text(
                    AppLocalizations.of(context).accounting_information,
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey[600],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.84,
                height: width * 0.88,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                AppLocalizations.of(context).activity,
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                activityTitle != null ? activityTitle : AppLocalizations.of(context).choose_an_activity,
                                style: TextStyle(fontSize: 13),
                              )
                            ],
                          ),
                          FutureBuilder(
                            future: listActivities,
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (!snapshot.hasData) {
                                return Text("loading...");
                              }
                              return PopupMenuButton<String>(
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.blueAccent,
                                  size: 35,
                                ),
                                itemBuilder: (context) => snapshot.data
                                    .map<PopupMenuItem<String>>(
                                        (value) => PopupMenuItem<String>(
                                              value: value.hashed_id,
                                              child: Text(
                                                value.title,
                                              ),
                                            ))
                                    .toList(),
                                onSelected: (value) {
                                  setState(() {
                                    activity = value;
                                  });

                                  for (var i in activities) {
                                    if (value == i.hashed_id) {
                                      setState(() {
                                        activityTitle = i.title;
                                      });
                                    }
                                  }
                                },
                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1,
                      ),
                      TextFormField(
                        controller: _taxIdentificationController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).tax_id_number,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                AppLocalizations.of(context).exercice,
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                exerciceTitle != null ? exerciceTitle : AppLocalizations.of(context).choose_exercice,
                                style: TextStyle(fontSize: 13),
                              )
                            ],
                          ),
                          FutureBuilder(
                            future: listExercices,
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (!snapshot.hasData) {
                                return Text("loading...");
                              }
                              return PopupMenuButton<String>(
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.blueAccent,
                                  size: 35,
                                ),
                                itemBuilder: (context) => snapshot.data
                                    .map<PopupMenuItem<String>>(
                                        (value) => PopupMenuItem<String>(
                                              value: value.hashed_id,
                                              child: Text(
                                                value.title,
                                              ),
                                            ))
                                    .toList(),
                                onSelected: (value) {
                                  setState(() {
                                    exercice = value;
                                  });

                                  for (var i in exercices) {
                                    if (value == i.hashed_id) {
                                      setState(() {
                                        exerciceTitle = i.title;
                                      });
                                    }
                                  }
                                },
                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                AppLocalizations.of(context).currency,
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                currencyTitle != null ? currencyTitle : AppLocalizations.of(context).main_currency,
                                style: TextStyle(fontSize: 13),
                              )
                            ],
                          ),
                          FutureBuilder(
                            future: listCurrency,
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (!snapshot.hasData) {
                                return Text("loading...");
                              }
                              return PopupMenuButton<String>(
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.blueAccent,
                                  size: 35,
                                ),
                                itemBuilder: (context) => snapshot.data
                                    .map<PopupMenuItem<String>>(
                                        (value) => PopupMenuItem<String>(
                                              value: value.hashed_id,
                                              child: Text(
                                                value.title,
                                              ),
                                            ))
                                    .toList(),
                                onSelected: (value) {
                                  setState(() {
                                    currency = value;
                                  });

                                  for (var i in currencies) {
                                    if (value == i.hashed_id) {
                                      setState(() {
                                        currencyTitle = i.title;
                                      });
                                    }
                                  }
                                },
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: width / 30,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: width / 3.5,
              ),
              RaisedButton(
                  color: Color.fromRGBO(55, 86, 223, 1),
                  onPressed: () async {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) =>
                            UpdateFinancialInformation(
                              companyName: widget.companyName,
                              logo: widget.logo,
                              pdfLanguage: widget.pdfLanguage,
                              phone: widget.phone,
                              website: widget.website,
                              activity: activity,
                              tax: _taxIdentificationController.text,
                              exercice: exercice,
                              currency: currency, companyId: widget.companyId,
                            )));
                  },
                  child: Container(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    width: width * 0.8,
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        Container(
                            margin: const EdgeInsets.only(left: 10.0),
                            child: Text(
                              AppLocalizations.of(context).financial_information.toUpperCase(),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            )),
                        Spacer(),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: AssetImage("assets/images/arrowGray.png"),
                            )),
                      ],
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0))),
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/device.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/company/updateCompany/update_accounting_information.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:birou/views/dialog/company.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class UpdateFinancialInformation extends StatefulWidget {
  @override
  _UpdateFinancialInformationState createState() =>
      _UpdateFinancialInformationState();

  String companyName;
  String logo;
  String pdfLanguage;
  String phone;
  String website;
  String activity;
  String tax;
  String exercice;
  String currency;
  String companyId;

  UpdateFinancialInformation(
      {Key key,
      @required this.companyName,
      @required this.logo,
      @required this.pdfLanguage,
      @required this.phone,
      @required this.website,
      @required this.activity,
      @required this.tax,
      @required this.exercice,
      @required this.currency,
        @required this.companyId
      })
      : super(key: key);
}

class _UpdateFinancialInformationState
    extends State<UpdateFinancialInformation> {
  final _bankController = TextEditingController();
  final _bicController = TextEditingController();
  final _ribController = TextEditingController();

  List<Country> countries = [];
  String country;
  String countryText;
  Future listCountries;
  bool loading = false;
  void initState() {
    listCountries = countryList();
    getCompany(widget.companyId).then((result) {
      setState(() {
        country = result['data']['company']['hashed_country_id'];
        companyCountries().then((result){
          setState(() {
            countries = result;
            for (var i in countries) {
              if (country == i.hashed_id) {
                setState(() {
                  countryText = i.title;
                });
              }
            }
          });
        });
        if(jsonDecode(result['data']['company']['bank_details'])['bank'] != " "){
          _bankController.text = jsonDecode(result['data']['company']['bank_details'])['bank'];
        }
        if(jsonDecode(result['data']['company']['bank_details'])['bs'] != " "){
          _bicController.text = jsonDecode(result['data']['company']['bank_details'])['bs'];
        }
        if(jsonDecode(result['data']['company']['bank_details'])['rib'] != " "){
          _ribController.text = jsonDecode(result['data']['company']['bank_details'])['rib'];
        }

      });
    });



    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);

          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).update_company,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),

          ],
        ),
      ),
      body: loading == false ? SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(245, 246, 252, 1),
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              SizedBox(
                height: width / 11,
              ),
              Row(
                children: [
                  SizedBox(
                    width: width / 11.5,
                  ),
                  Text(
                    AppLocalizations.of(context).financial_information,
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey[600],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.84,
                height: width * 0.88,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TextFormField(
                        controller: _bankController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).bank,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1,
                      ),
                      TextFormField(
                        controller: _bicController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).bic,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _ribController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).rib,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                  AppLocalizations.of(context).country,
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                countryText != null ? countryText : AppLocalizations.of(context).choose_country,
                                style: TextStyle(fontSize: 13),
                              )
                            ],
                          ),
                          FutureBuilder(
                            future: listCountries,
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (!snapshot.hasData) {
                                return Text("loading...");
                              }
                              return PopupMenuButton<String>(
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.blueAccent,
                                  size: 35,
                                ),
                                itemBuilder: (context) => snapshot.data
                                    .map<PopupMenuItem<String>>(
                                        (value) => PopupMenuItem<String>(
                                              value: value.hashed_id,
                                              child: Text(
                                                value.title,
                                              ),
                                            ))
                                    .toList(),
                                onSelected: (value) {
                                  setState(() {
                                    country = value;
                                  });

                                  for (var i in countries) {
                                    if (value == i.hashed_id) {
                                      setState(() {
                                        countryText = i.title;
                                      });
                                    }
                                  }
                                },
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: width / 30,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: width / 3.5,
              ),
              RaisedButton(
                  color: Color.fromRGBO(55, 86, 223, 1),
                  onPressed: () async {
                    setState(() {
                      loading = true;
                    });
                    print(widget.logo);
                    updateCompany(
                            widget.companyId,
                            widget.companyName,
                        widget.website != null
                            ? Uri.parse(widget.website)
                            : null,
                            country,
                            "",
                            "",
                            "",
                            widget.pdfLanguage,
                            widget.logo,
                            widget.activity,
                            widget.tax,
                            int.parse(widget.phone),
                            widget.currency,
                            widget.exercice,
                            _bankController.text,
                            _bicController.text,
                            _ribController.text,
                            _ribController.text)
                        .then((result) {
                      setState(() {
                        loading = false;
                        if(result['status']['code'] == 200){
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return CompanyDialog(image: 1, text1: 'Success', text2: result['message'], );
                              });
                        }else{
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return CompanyDialog(image: 0, text1: 'Error', text2: result['status']['message'], );
                              });
                        }
                      });
                      print(result);
                    });
                  },
                  child: Container(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    width: width * 0.8,
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        Container(
                            margin: const EdgeInsets.only(left: 10.0),
                            child: Text(
                              AppLocalizations.of(context).finish.toUpperCase(),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            )),
                        Spacer(),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: AssetImage("assets/images/arrowGray.png"),
                            )),
                      ],
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0))),
            ],
          ),
        ),
      ) : Container(
        height: height,
        child: SpinKitFadingCircle(
          itemBuilder: (BuildContext context, int index) {
            return DecoratedBox(
              decoration: BoxDecoration(
                color: index.isEven ? Colors.yellow : Colors.blue,
              ),
            );
          },
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:io' as Io;

import 'package:birou/controllers/companyController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/language.dart';
import 'package:birou/views/company/updateCompany/update_accounting_information.dart';
import 'package:birou/views/dashboard/dashboard.dart';
import 'package:birou/views/dashboard/home.dart';
import 'package:birou/views/dialog/logo.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class UpdateGeneralInformation extends StatefulWidget {
  @override
  _UpdateGeneralInformationState createState() =>
      _UpdateGeneralInformationState();
  String companyId;
  final List<Company> companies;

  UpdateGeneralInformation(
      {Key key, @required this.companyId, @required this.companies});
}

class _UpdateGeneralInformationState extends State<UpdateGeneralInformation> {
  final _companyNameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _websiteController = TextEditingController();
  File imageFile;
  String pdfLanguage;
  String pdfText;
  String logo = "";
  List<Language> languages = [
    Language("Français", "fr"),
    Language("English", "en"),
    Language("Arabic", "ar")
  ];

  static Future image2Base64(String path) async {
    File file = new File(path);
    List<int> imageBytes = await file.readAsBytes();
    return base64Encode(imageBytes);
  }

  void initState() {
    getCompany(widget.companyId).then((result) {
      setState(() {
        _companyNameController.text = result['data']['company']['title'];
        if (result['data']['company']['logo'] != null) {
          setState(() {
            logo = result['data']['company']['logo'];
          });
        }
        pdfLanguage = result['data']['company']['language'];
        _phoneController.text = result['data']['company']['phone'];
        _websiteController.text = result['data']['company']['website'];
        for (var i in languages) {
          if (pdfLanguage == i.hashed_id) {
            setState(() {
              pdfText = i.title;
            });
          }
        }
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.blueAccent),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => Home(
                      companyId: widget.companyId,
                      companies: widget.companies,
                      currentScreen: Dashboard(
                        companyId: widget.companyId,
                        companies: widget.companies,
                      ),
                    )));
          },
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).update_company,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),

          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(245, 246, 252, 1),
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              SizedBox(
                height: width / 11,
              ),
              Row(
                children: [
                  SizedBox(
                    width: width / 11.5,
                  ),
                  Text(
                    AppLocalizations.of(context).general_information,
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey[600],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.84,
                height: width * 0.88,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TextFormField(
                        controller: _companyNameController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).company_name,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                AppLocalizations.of(context).logo,
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              GestureDetector(
                                onTap: () async {
                                  PickedFile pickedFile =
                                      await ImagePicker().getImage(
                                    source: ImageSource.gallery,
                                    maxWidth: 1800,
                                    maxHeight: 1800,
                                  );
                                  if (pickedFile != null) {
                                    image2Base64(pickedFile.path).then((value) {
                                      log("data:image/png;base64,$value");
                                      uploadLogo("data:image/png;base64,$value")
                                          .then((result) {
                                        print(result);
                                        setState(() {
                                          logo = result["data"]['photoPath'];
                                        });
                                      });
                                    });
                                    // uploadLogo(img64).then((result){
                                    //   print(result);
                                    // });
                                  }
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    logo != null && logo != "" ? Text(
                                      AppLocalizations.of(context).change_logo,
                                      style: TextStyle(fontSize: 13, color: Colors.red),
                                    ) : Text(
                                      AppLocalizations.of(context).choose_logo,
                                      style: TextStyle(fontSize: 13),
                                    ),
                                  ],
                                )
                              ),
                              logo != null && logo != "" ? SizedBox(height: 5,) : Container(),
                              logo != null && logo != "" ? InkWell(
                                onTap: (){
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return LogoDialog(logo: logo,
                                        );
                                      });
                                },
                                child: Text(
                                  AppLocalizations.of(context).display_current_logo,
                                  style: TextStyle(fontSize: 13, color: Colors.green),
                                ),
                              ) : Container(),
                            ],
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                AppLocalizations.of(context).pdf_language,
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                pdfText != null
                                    ? pdfText
                                    : AppLocalizations.of(context).choose_lang,
                                style: TextStyle(fontSize: 13),
                              )
                            ],
                          ),
                          PopupMenuButton<String>(
                            icon: Icon(
                              Icons.arrow_drop_down,
                              color: Colors.blueAccent,
                              size: 35,
                            ),
                            onSelected: (String result) {
                              setState(() {
                                pdfLanguage = result;
                              });

                              for (var i in languages) {
                                if (i.hashed_id == result) {
                                  print(i.title);
                                  setState(() {
                                    pdfText = i.title;
                                  });
                                }
                              }
                            },
                            itemBuilder: (BuildContext context) =>
                                <PopupMenuEntry<String>>[
                              PopupMenuItem<String>(
                                value: languages[0].hashed_id,
                                child: Text(languages[0].title),
                              ),
                              PopupMenuItem<String>(
                                value: languages[1].hashed_id,
                                child: Text(languages[1].title),
                              ),
                              PopupMenuItem<String>(
                                value: languages[2].hashed_id,
                                child: Text(languages[2].title),
                              ),
                            ],
                          )
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _phoneController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).phone,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _websiteController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).website,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      SizedBox(
                        height: width / 40,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: width / 3.5,
              ),
              RaisedButton(
                  color: Color.fromRGBO(55, 86, 223, 1),
                  onPressed: () async {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) =>
                            UpdateAccountingInformation(
                              companyName: _companyNameController.text,
                              logo: logo,
                              pdfLanguage: pdfLanguage,
                              phone: _phoneController.text,
                              website: _websiteController.text,
                              companyId: widget.companyId,
                            )));
                  },
                  child: Container(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    width: width * 0.8,
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        Container(
                            margin: const EdgeInsets.only(left: 10.0),
                            child: Text(
                              AppLocalizations.of(context)
                                  .accounting_information
                                  .toUpperCase(),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            )),
                        Spacer(),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: AssetImage("assets/images/arrowGray.png"),
                            )),
                      ],
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0))),
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:math';

import 'package:birou/controllers/clientController.dart';
import 'package:birou/controllers/contactsController.dart';
import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/views/client/addClient/generalInformations.dart';
import 'package:birou/views/client/editClient/generalInformations.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:birou/views/provider/addProvider/generalInformations.dart';
import 'package:birou/views/provider/editProvider/generalInformations.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:bubbled_navigation_bar/bubbled_navigation_bar.dart';
import 'package:clay_containers/clay_containers.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ionicons/ionicons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Contact extends StatefulWidget {
  @override
  String companyId;
  final List<Company> companies;

  Contact({Key key, this.companies, this.companyId});

  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contact> {
  @override
  String full_name;
  String userImage = '';

  bool clientList = true;
  bool providerList = false;
  Future providers;
  Future clients;
  int itemCount = 4;
  double listBuilderHeightClient = 350.0;
  double listBuilderHeightProvider = 350.0;

  final spinkit = SpinKitFadingCircle(
    color: Colors.blueAccent,
    size: 50.0,
  );

  List<Condition> conditions = [
    Condition("Update", 1),
    Condition("Delete", 2),
  ];

  getUserName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("full_name");
  }

  getUserImage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("userImage");
  }

  @override
  void initState() {
    clients = clientsList(widget.companyId);
    providers = providersList(widget.companyId);

    getUserName().then((result) {
      setState(() {
        full_name = result;
      });
    });

    getUserImage().then((result) {
      setState(() {
        userImage = result;
      });
    });

    super.initState();
  }

  Widget build(BuildContext context) {
    var width = MediaQuery
        .of(context)
        .size
        .width;
    var height = MediaQuery
        .of(context)
        .size
        .height;

    return Scaffold(
      backgroundColor: Color.fromRGBO(245, 246, 252, 1),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(18.0, 15, 18, 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                MainMenu(
                                  companies: widget.companies,
                                )));
                      },
                      child: Container(
                        width: 18,
                        height: 18,
                        child: Image.asset(
                          "assets/images/menu.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 30,
                        child: ClipOval(
                          child: userImage == ''
                              ? Image.asset(
                            'assets/images/person.png',
                          )
                              : new Image.network(
                              'https://www.birou.tn/' + userImage),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: Text(
                  AppLocalizations
                      .of(context)
                      .clients,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: width / 15,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              // Padding(
              //   padding: const EdgeInsets.all(18.0),
              //   child: Center(
              //     child: Container(
              //       height: height / 12.5,
              //       decoration: BoxDecoration(
              //         borderRadius: BorderRadius.circular(20),
              //         boxShadow: [
              //           BoxShadow(
              //             color: Colors.blue.withOpacity(0.1),
              //             spreadRadius: 5,
              //             blurRadius: 7,
              //             offset: Offset(0, 3), // changes position of shadow
              //           ),
              //         ],
              //       ),
              //       child: Card(
              //         elevation: 0,
              //         shape: RoundedRectangleBorder(
              //             borderRadius: BorderRadius.circular(20)),
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //           children: [
              //             FlatButton(
              //                 onPressed: () {
              //                   setState(() {
              //                     clientList = true;
              //                     providerList = false;
              //                   });
              //                 },
              //                 child: Container(
              //                     width: width / 3,
              //                     child: Center(
              //                       child: Text(
              //                         AppLocalizations.of(context).clients,
              //                         style: TextStyle(
              //                             fontSize: 16,
              //                             fontWeight: FontWeight.normal,
              //                             color: clientList == true
              //                                 ? Colors.grey
              //                                 : Colors.black),
              //                       ),
              //                     ))),
              //             // FlatButton(
              //             //     onPressed: () {
              //             //       setState(() {
              //             //         clientList = false;
              //             //         providerList = true;
              //             //       });
              //             //     },
              //             //     child: Container(
              //             //         width: width / 3,
              //             //         child: Center(
              //             //           child: Text(
              //             //               AppLocalizations.of(context).providers,
              //             //               style: TextStyle(
              //             //                   fontSize: 16,
              //             //                   fontWeight: FontWeight.normal,
              //             //                   color: providerList == true
              //             //                       ? Colors.grey
              //             //                       : Colors.black)),
              //             //         ))),
              //           ],
              //         ),
              //       ),
              //     ),
              //   ),
              // ),

              Padding(
                padding:
                const EdgeInsets.only(top: 30.0, left: 18.0, right: 18.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      clientList == true
                          ? AppLocalizations
                          .of(context)
                          .synthesis_clients
                          : AppLocalizations
                          .of(context)
                          .synthesis_providers,
                      style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                    ),
                    InkWell(
                      onTap: () {
                        if (clientList == true) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  GeneralInformationClient(
                                    companyId: widget.companyId,
                                    companies: widget.companies,
                                  )));
                        } else {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  GeneralInformationProvider(
                                    companyId: widget.companyId,
                                  )));
                        }
                      },
                      child: Icon(
                        Icons.add,
                        size: width / 14,
                        color: Colors.grey[700],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Card(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          FutureBuilder(
                            future: clientList == true ? clients : providers,
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.waiting) {
                                return Container(
                                  height: width * 0.5,
                                  alignment: Alignment.center,
                                  child: Center(
                                    child: spinkit,
                                  ),
                                );
                              } else if (snapshot.data.length < 1) {
                                return Container(
                                  height: width * 0.25,
                                  alignment: Alignment.center,
                                  child: Center(
                                    child: Text(AppLocalizations
                                        .of(context)
                                        .no_data_found),
                                  ),
                                );
                              } else {
                                return Column(
                                  children: [
                                    ListView.builder(
                                      shrinkWrap: true,
                                      physics:
                                      const NeverScrollableScrollPhysics(),
                                      itemCount: snapshot.data.length > 3
                                          ? itemCount
                                          : snapshot.data.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Client(
                                            snapshot.data[index].display_name,
                                            snapshot.data[index].organisation,
                                            (index + 1).toString(),
                                            snapshot.data[index].id);
                                      },
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    snapshot.data.length - itemCount > 0 &&
                                        snapshot.data.length > 3
                                        ? Align(
                                      alignment: Alignment.bottomRight,
                                      child: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            snapshot.data.length -
                                                itemCount >=
                                                2
                                                ? itemCount =
                                                itemCount + 2 : itemCount =
                                                itemCount +
                                                    snapshot.data.length -
                                                    itemCount;
                                          });
                                        },
                                        child: Padding(
                                          padding:
                                          const EdgeInsets.fromLTRB(
                                              8, 8, 20, 8),
                                          child: Text(
                                            AppLocalizations
                                                .of(context)
                                                .load_more,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight:
                                                FontWeight.normal,
                                                color: Colors.blueAccent),
                                          ),
                                        ),
                                      ),
                                    )
                                        : Container()
                                  ],
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Client(String name, company, index, id) {
    return Container(
        child: ListTile(
          leading: Text(
            "#" + index,
            style: TextStyle(fontSize: 15),
          ),
          title: Text(
            name,
            style: TextStyle(
                color: Colors.blueAccent,
                fontSize: 16,
                fontWeight: FontWeight.normal),
          ),
          subtitle: Text(
            company != null ? company : '',
            style: TextStyle(
              color: Colors.grey[700],
              fontSize: 12,
            ),
          ),
          trailing: Container(
            width: 120,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                PopupMenuButton<int>(
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: Colors.blueAccent,
                    size: 35,
                  ),
                  onSelected: (int result) {
                    if (result == 1) {
                      if (providerList == true) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                GeneralInformationProviderUpdate(
                                  companyId: widget.companyId,
                                  id: id,
                                )));
                      } else {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                GeneralInformationClientUpdate(
                                  companyId: widget.companyId,
                                  id: id,
                                  companies: widget.companies,
                                )));
                      }
                    } else if (result == 2) {
                      // showDialog(
                      //     context: context,
                      //     builder: (BuildContext context) {
                      //       return DeleteInput(id: id);
                      //     });
                    }
                  },
                  itemBuilder: (BuildContext context) =>
                  <PopupMenuEntry<int>>[
                    PopupMenuItem<int>(
                      value: conditions[0].condition,
                      child: Text(AppLocalizations
                          .of(context)
                          .update),
                    ),
                    PopupMenuItem<int>(
                      value: conditions[1].condition,
                      child: Text(AppLocalizations
                          .of(context)
                          .delete),
                    )
                  ],
                )
              ],
            ),
          ),
        ));
  }
}

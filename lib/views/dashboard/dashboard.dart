import 'dart:math';

import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/controllers/invoiceController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/views/company/updateCompany/update_general_information.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ionicons/ionicons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Dashboard extends StatefulWidget {
  String companyId;
  final List<Company> companies;

  Dashboard({Key key, this.companies, this.companyId});

  final List<Color> availableColors = [
    Colors.purpleAccent,
    Colors.yellow,
    Colors.lightBlue,
    Colors.orange,
    Colors.pink,
    Colors.redAccent,
  ];
  final titles = ['Dashboard', 'Phone', 'Location', 'Info', 'Profile'];
  final colors = [
    Color.fromRGBO(55, 86, 223, 1),
    Color.fromRGBO(55, 86, 223, 1),
    Color.fromRGBO(55, 86, 223, 1),
    Color.fromRGBO(55, 86, 223, 1),
    Color.fromRGBO(55, 86, 223, 1),
  ];
  final icons = [
    AssetImage("assets/images/icon5.png"),
    AssetImage("assets/images/icon1.png"),
    AssetImage("assets/images/icon2.png"),
    AssetImage("assets/images/icon3.png"),
    AssetImage("assets/images/icon4.png"),
  ];

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  final Color leftBarColor = const Color(0xff53fdd7);
  final Color rightBarColor = const Color(0xffff5182);
  final Color thirdBarColor = Colors.blueAccent;

  final double width = 4;

  List<BarChartGroupData> rawBarGroups;
  List<BarChartGroupData> showingBarGroups;

  int touchedGroupIndex;

  int touchedIndex;
  final Color barBackgroundColor = Colors.transparent;
  final Duration animDuration = const Duration(milliseconds: 250);
  bool isPlaying = false;
  int currentIndex = 0;
  String companyName;
  String companyId;
  String userImage = '';

  BarChartGroupData barGroup1;
  BarChartGroupData barGroup2;
  BarChartGroupData barGroup3;
  BarChartGroupData barGroup4;
  BarChartGroupData barGroup5;
  BarChartGroupData barGroup6;
  BarChartGroupData barGroup7;
  BarChartGroupData barGroup8;
  BarChartGroupData barGroup9;
  BarChartGroupData barGroup10;
  BarChartGroupData barGroup11;
  BarChartGroupData barGroup12;

  String estimates;
  String invoices;
  String payments;
  double maxY = 5;
  List<int> estimatesList = [];
  List<int> invoicesList = [];
  List<int> paymentsList = [];

  List<int> expenseList = [];
  String fullName;
  Future dashboard;
  String since = ' ';
  getName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("fullName");
  }

  getUserImage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("userImage");
  }

  @override
  void initState() {
    getInvoices(widget.companyId).then((value){
        if(value.length > 0){
          setState(() {
            since = value[0].date.toString().substring(0, 4);
          });
        }
    });
    dashboard = companyDashboard(widget.companyId);
    getName().then((name) {
      setState(() {
        fullName = name;
      });
    });
    getUserImage().then((result) {
      setState(() {
        userImage = result;
      });
    });
    companyDashboard(widget.companyId).then((result) {
      setState(() {
        companyName = result['data']['company']['title'];
      });

      var estimatesConverted = result['data']['estimatesConverted'];
      var invoicesConverted = result['data']['invoicesConverted'];
      var paymentsConverted = result['data']['incomesConverted'];
      var totalEstimates = result['data']['estimatesConvertedSum'];
      var totalInvoices = result['data']['invoicesConvertedSum'];
      var totalPayments = result['data']['incomesConvertedSum'];
      // print(result['data']['sortedExpenseCategories']);
      result['data']['sortedExpenseCategories'].forEach((key, value) {
        // print(key);
        expenseList.add(value.floor());
      });
      print(expenseList);

      //estimates total
      if (totalEstimates == 0) {
        setState(() {
          estimates = "0";
        });
      } else {
        setState(() {
          estimates = totalEstimates;
        });
      }

      //invoices total
      if (totalInvoices == 0) {
        setState(() {
          invoices = "0";
        });
      } else {
        setState(() {
          invoices = totalInvoices;
        });
      }

      //payments total
      if (totalPayments == 0) {
        setState(() {
          payments = "0";
        });
      } else {
        setState(() {
          payments = totalPayments;
        });
      }

      setState(() {
        //estimates graph values
        if (estimatesConverted.length == 0) {
          for (var i = 1; i <= 12; i++) {
            estimatesList.add(0);
          }
        } else {
          for (var i = 1; i <= 12; i++) {
            if (estimatesConverted[i.toString()] == null) {
              estimatesList.add(0);
            } else {
              estimatesList.add((estimatesConverted[i.toString()]) ~/ 1000);
              if (maxY < (estimatesConverted[i.toString()]) ~/ 1000) {
                setState(() {
                  maxY = ((estimatesConverted[i.toString()]) ~/ 1000)
                          .floorToDouble() +
                      10;
                });
              }
            }
          }
        }

        //invoices graph values
        if (invoicesConverted.length == 0) {
          for (var i = 1; i <= 12; i++) {
            invoicesList.add(0);
          }
        } else {
          for (var i = 1; i <= 12; i++) {
            if (invoicesConverted[i.toString()] == null) {
              invoicesList.add(0);
            } else {
              invoicesList.add(invoicesConverted[i.toString()] ~/ 1000);
              if (maxY < (invoicesConverted[i.toString()]) ~/ 1000) {
                setState(() {
                  maxY = ((invoicesConverted[i.toString()]) ~/ 1000)
                          .floorToDouble() +
                      10;
                });
              }
            }
          }
        }

        //payments graph values
        if (paymentsConverted.length == 0) {
          for (var i = 1; i <= 12; i++) {
            paymentsList.add(0);
          }
        } else {
          for (var i = 1; i <= 12; i++) {
            if (paymentsConverted[i.toString()] == null) {
              paymentsList.add(0);
            } else {
              paymentsList.add(paymentsConverted[i.toString()] ~/ 1000);
              if (maxY < (paymentsConverted[i.toString()]) ~/ 1000) {
                setState(() {
                  maxY = ((paymentsConverted[i.toString()]) ~/ 1000)
                          .floorToDouble() +
                      10;
                });
              }
            }
          }
        }

        // print(estimatesList);
        // print(invoicesList);
        // print(paymentsList);

        barGroup1 = makeGroupData(0, estimatesList[0].floorToDouble(),
            invoicesList[0].floorToDouble(), paymentsList[0].floorToDouble());
        barGroup2 = makeGroupData(1, estimatesList[1].floorToDouble(),
            invoicesList[1].floorToDouble(), paymentsList[1].floorToDouble());
        barGroup3 = makeGroupData(2, (estimatesList[2].floorToDouble()),
            invoicesList[2].floorToDouble(), paymentsList[2].floorToDouble());
        barGroup4 = makeGroupData(3, estimatesList[3].floorToDouble(),
            invoicesList[3].floorToDouble(), paymentsList[3].floorToDouble());
        barGroup5 = makeGroupData(4, estimatesList[4].floorToDouble(),
            invoicesList[4].floorToDouble(), paymentsList[4].floorToDouble());
        barGroup6 = makeGroupData(5, estimatesList[5].floorToDouble(),
            invoicesList[5].floorToDouble(), paymentsList[5].floorToDouble());
        barGroup7 = makeGroupData(6, estimatesList[6].floorToDouble(),
            invoicesList[6].floorToDouble(), paymentsList[6].floorToDouble());
        barGroup8 = makeGroupData(7, estimatesList[7].floorToDouble(),
            invoicesList[7].floorToDouble(), paymentsList[7].floorToDouble());
        barGroup9 = makeGroupData(8, estimatesList[8].floorToDouble(),
            invoicesList[8].floorToDouble(), paymentsList[8].floorToDouble());
        barGroup10 = makeGroupData(9, estimatesList[9].floorToDouble(),
            invoicesList[9].floorToDouble(), paymentsList[9].floorToDouble());
        barGroup11 = makeGroupData(10, estimatesList[10].floorToDouble(),
            invoicesList[10].floorToDouble(), paymentsList[10].floorToDouble());
        barGroup12 = makeGroupData(11, estimatesList[11].floorToDouble(),
            invoicesList[11].floorToDouble(), paymentsList[11].floorToDouble());

        final items = [
          barGroup1,
          barGroup2,
          barGroup3,
          barGroup4,
          barGroup5,
          barGroup6,
          barGroup7,
          barGroup8,
          barGroup9,
          barGroup10,
          barGroup11,
          barGroup12,
        ];

        rawBarGroups = items;

        showingBarGroups = rawBarGroups;
      });
    });

    super.initState();
  }

  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var heigth = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Color.fromRGBO(245, 246, 252, 1),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: SafeArea(
            child: FutureBuilder(
              future: dashboard,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState != ConnectionState.done) {
                  return Container(
                      alignment: Alignment.bottomCenter,
                      height: MediaQuery.of(context).size.height / 2,
                      child: spin());
                } else {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18.0, 15, 18, 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MainMenu(
                                              companies: widget.companies,
                                            )));
                              },
                              child: Container(
                                width: 18,
                                height: 18,
                                child: Image.asset(
                                  "assets/images/menu.png",
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {},
                              child: CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius: 30,
                                child: ClipOval(
                                  child: userImage == ''
                                      ? Image.asset(
                                    'assets/images/person.png',
                                  )
                                      : new Image.network(
                                      'https://www.birou.tn/' + userImage),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 18.0, right: 5),
                            child: Text(
                              companyName != null ? companyName : "",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: width / 12.5,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start,
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.edit,
                              color: Colors.blueAccent,
                            ),
                            onPressed: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          UpdateGeneralInformation(companyId: widget.companyId, companies: widget.companies,)));
                            },
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 18.0, top: 5),
                        child: Text(
                          AppLocalizations.of(context).hello +
                              ", " +
                              (fullName != null ? fullName : ""),
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: width / 19,
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: Container(
                            height: heigth / 2.3,
                            width: width * 0.91,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.blue.withOpacity(0.1),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              color: Color.fromRGBO(245, 246, 252, 1),
                              child: Padding(
                                padding: const EdgeInsets.all(16),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Text(
                                      AppLocalizations.of(context)
                                          .current_exercise_sales,
                                      style: TextStyle(
                                          fontSize: width / 19,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                estimates == null
                                                    ? "0"
                                                    : estimates,
                                                style: TextStyle(
                                                    fontSize: width / 14,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              Text(
                                                AppLocalizations.of(context)
                                                    .estimates,
                                                style: TextStyle(
                                                    color: Colors.grey),
                                              ),
                                              Text(
                                                "TND",
                                                style: TextStyle(
                                                    fontSize: width / 27,
                                                    color: Colors.green[400]),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                invoices == null
                                                    ? "0"
                                                    : invoices,
                                                style: TextStyle(
                                                    fontSize: width / 14,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              Text(
                                                AppLocalizations.of(context)
                                                    .invoices,
                                                style: TextStyle(
                                                    color: Colors.grey),
                                              ),
                                              Text(
                                                "TND",
                                                style: TextStyle(
                                                    fontSize: width / 27,
                                                    color: Colors.green[400]),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                payments == null
                                                    ? "0"
                                                    : payments,
                                                style: TextStyle(
                                                    fontSize: width / 14,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              Text(
                                                AppLocalizations.of(context)
                                                    .payments,
                                                style: TextStyle(
                                                    color: Colors.grey),
                                              ),
                                              Text(
                                                "TND",
                                                style: TextStyle(
                                                    fontSize: width / 27,
                                                    color: Colors.green[400]),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: width / 10,
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0),
                                        child: BarChart(
                                          BarChartData(
                                            maxY: maxY,
                                            barTouchData: BarTouchData(
                                                touchTooltipData:
                                                    BarTouchTooltipData(
                                                  tooltipBgColor: Colors.grey,
                                                  getTooltipItem:
                                                      (_a, _b, _c, _d) => null,
                                                ),
                                                touchCallback: (response) {
                                                  if (response.spot == null) {
                                                    setState(() {
                                                      touchedGroupIndex = -1;
                                                      showingBarGroups =
                                                          List.of(rawBarGroups);
                                                    });
                                                    return;
                                                  }

                                                  touchedGroupIndex = response
                                                      .spot
                                                      .touchedBarGroupIndex;

                                                  setState(() {
                                                    if (response.touchInput
                                                            is PointerExitEvent ||
                                                        response.touchInput
                                                            is PointerUpEvent) {
                                                      touchedGroupIndex = -1;
                                                      showingBarGroups =
                                                          List.of(rawBarGroups);
                                                    } else {
                                                      showingBarGroups =
                                                          List.of(rawBarGroups);
                                                      if (touchedGroupIndex !=
                                                          -1) {
                                                        double sum = 0;
                                                        for (BarChartRodData rod
                                                            in showingBarGroups[
                                                                    touchedGroupIndex]
                                                                .barRods) {
                                                          sum += rod.y;
                                                        }
                                                        final avg = sum /
                                                            showingBarGroups[
                                                                    touchedGroupIndex]
                                                                .barRods
                                                                .length;

                                                        showingBarGroups[
                                                                touchedGroupIndex] =
                                                            showingBarGroups[
                                                                    touchedGroupIndex]
                                                                .copyWith(
                                                          barRods: showingBarGroups[
                                                                  touchedGroupIndex]
                                                              .barRods
                                                              .map((rod) {
                                                            return rod.copyWith(
                                                                y: avg);
                                                          }).toList(),
                                                        );
                                                      }
                                                    }
                                                  });
                                                }),
                                            titlesData: FlTitlesData(
                                              show: true,
                                              bottomTitles: SideTitles(
                                                showTitles: true,
                                                getTextStyles: (value) =>
                                                    TextStyle(
                                                        color:
                                                            Color(0xff7589a2),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: width / 33),
                                                margin: 15,
                                                getTitles: (double value) {
                                                  switch (value.toInt()) {
                                                    case 0:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['1']);
                                                    case 1:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['2']);
                                                    case 2:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['3']);
                                                    case 3:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['4']);
                                                    case 4:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['5']);
                                                    case 5:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['6']);
                                                    case 6:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['7']);
                                                    case 7:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['8']);
                                                    case 8:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['9']);
                                                    case 9:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['10']);
                                                    case 10:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['11']);
                                                    case 11:
                                                      return (snapshot
                                                                  .data['data'][
                                                              'organisedMonths']
                                                          ['12']);
                                                    default:
                                                      return '';
                                                  }
                                                },
                                              ),
                                              leftTitles: SideTitles(
                                                showTitles: true,
                                                interval: (maxY / 5).floorToDouble(),
                                                getTextStyles: (value) =>
                                                    TextStyle(
                                                        color:
                                                            Color(0xff7589a2),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: width / 27.8),
                                                margin: 20,
                                                getTitles: (value) {
                                                    return value.floor().toString();

                                                },
                                              ),
                                            ),
                                            borderData: FlBorderData(
                                              show: false,
                                            ),
                                            barGroups: showingBarGroups,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: heigth / 500,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: heigth / 50,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 18.0),
                        child: Text(
                          AppLocalizations.of(context).overall_statistics,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: width / 19,
                              fontWeight: FontWeight.w500),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      SizedBox(
                        height: heigth / 50,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Container(
                              height: heigth / 6.5,
                              width: width / 3.5,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.blue.withOpacity(0.1),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Icon(
                                            Ionicons.cash,
                                            color: Colors.blueAccent,
                                            size: width / 12,
                                          ),
                                          Text(
                                            since != ' ' ? "Since " + since : '',
                                            style: TextStyle(
                                                fontSize: 9,
                                                color: Colors.grey),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: heigth / 70,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)
                                                .total_balance,
                                            style: TextStyle(),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Text(
                                                (snapshot.data['data']
                                                            ['totalBalance']
                                                        .floor())
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: width / 23,
                                                    color: Colors.grey),
                                              ),
                                              SizedBox(width: 2,),
                                              Text(
                                                "TND",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.green),
                                              )
                                            ],
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              height: heigth / 6.5,
                              width: width / 3.5,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.blue.withOpacity(0.1),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Icon(
                                            Ionicons.card,
                                            color: Colors.blueAccent,
                                            size: width / 12,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: heigth / 70,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(AppLocalizations.of(context)
                                              .incomes),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Text(
                                                (snapshot.data['data']
                                                            ['totalIncomes']
                                                        .floor())
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: width / 23,
                                                    color: Colors.grey),
                                              ),
                                              SizedBox(width: 2,),
                                              Text(
                                                "TND",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.green),
                                              )
                                            ],
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              height: heigth / 6.5,
                              width: width / 3.5,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.blue.withOpacity(0.1),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Icon(
                                            Ionicons.wallet_outline,
                                            color: Colors.blueAccent,
                                            size: width / 12,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: heigth / 70,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(AppLocalizations.of(context)
                                              .outcomes),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Text(
                                                (snapshot.data['data']
                                                            ['totalOutcomes']
                                                        .floor())
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: width / 23,
                                                    color: Colors.grey),
                                              ),
                                              SizedBox(width: 2,),
                                              Text(
                                                "TND",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.green),
                                              )
                                            ],
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: heigth / 70,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: Container(
                            height: heigth / 2.1,
                            width: width * 0.92,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.blue.withOpacity(0.1),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              color: Color.fromRGBO(245, 246, 252, 1),
                              child: Padding(
                                padding: const EdgeInsets.all(16),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Text(
                                      AppLocalizations.of(context)
                                          .main_expenses,
                                      style: TextStyle(
                                          fontSize: width / 19,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(height: 20,),
                                    Expanded(
                                      child: expenseList.length < 6
                                          ? Container()
                                          : BarChart(
                                              isPlaying
                                                  ? randomData()
                                                  : mainBarData(),
                                              swapAnimationDuration:
                                                  animDuration,
                                            ),
                                    ),
                                    SizedBox(
                                      height: 12,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.circle,
                                                  size: 11,
                                                  color: Color.fromRGBO(
                                                      163, 209, 255, 1),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .accounting_fees,
                                                  style: TextStyle(
                                                      fontSize: width / 29),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.circle,
                                                  size: 11,
                                                  color: Color.fromRGBO(
                                                      175, 220, 231, 1),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .automobile_expense,
                                                  style: TextStyle(
                                                      fontSize: width / 29),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.circle,
                                                  size: 11,
                                                  color: Color.fromRGBO(
                                                      157, 235, 254, 1),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .telecommunication,
                                                  style: TextStyle(
                                                      fontSize: width / 29),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.circle,
                                                  size: 11,
                                                  color: Color.fromRGBO(
                                                      153, 245, 231, 1),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .advertising_and_marketing,
                                                  style: TextStyle(
                                                      fontSize: width / 29),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.circle,
                                                  color: Color.fromRGBO(
                                                      212, 252, 178, 1),
                                                  size: 11,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .consultant_fees,
                                                  style: TextStyle(
                                                      fontSize: width / 29),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.circle,
                                                  size: 11,
                                                  color: Color.fromRGBO(
                                                      223, 225, 155, 1),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .credit_card_fees,
                                                  style: TextStyle(
                                                      fontSize: width / 29),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Padding getIcon(int index) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 3),
      child: Image(
        image: widget.icons[index],
      ),
    );
  }

  BarChartGroupData makeGroupData(int x, double y1, double y2, double y3) {
    return BarChartGroupData(barsSpace: 4, x: x, barRods: [
      BarChartRodData(
        y: y1,
        colors: [leftBarColor],
        width: width,
      ),
      BarChartRodData(
        y: y2,
        colors: [rightBarColor],
        width: width,
      ),
      BarChartRodData(
        y: y3,
        colors: [thirdBarColor],
        width: width,
      ),
    ]);
  }

  Widget makeTransactionsIcon() {
    const double width = 4.5;
    const double space = 3.5;
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 42,
          color: Colors.white.withOpacity(1),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
      ],
    );
  }

  BarChartGroupData makeGroupDataBar(
    int x,
    double y, {
    bool isTouched = false,
    Color barColor,
    double width = 22,
    List<int> showTooltips = const [],
  }) {
    return BarChartGroupData(
      x: x,
      barRods: [
        BarChartRodData(
          y: isTouched ? y + 1 : y,
          colors: isTouched ? [Colors.yellow] : [barColor],
          width: width,
          backDrawRodData: BackgroundBarChartRodData(
            show: true,
            y: 20,
            colors: [barBackgroundColor],
          ),
        ),
      ],
      showingTooltipIndicators: showTooltips,
    );
  }

  List<BarChartGroupData> showingGroups() => List.generate(6, (i) {
        if (expenseList.length < 6) {
          return null;
        } else {
          switch (i) {
            case 0:
              return makeGroupDataBar(0, expenseList[0].floorToDouble(),
                  isTouched: i == touchedIndex,
                  barColor: Color.fromRGBO(163, 209, 255, 1));
            case 1:
              return makeGroupDataBar(1, expenseList[1].floorToDouble(),
                  isTouched: i == touchedIndex,
                  barColor: Color.fromRGBO(157, 235, 254, 1));
            case 2:
              return makeGroupDataBar(2, expenseList[2].floorToDouble(),
                  isTouched: i == touchedIndex,
                  barColor: Color.fromRGBO(153, 245, 231, 1));
            case 3:
              return makeGroupDataBar(3, expenseList[3].floorToDouble(),
                  isTouched: i == touchedIndex,
                  barColor: Color.fromRGBO(175, 220, 231, 1));
            case 4:
              return makeGroupDataBar(4, expenseList[4].floorToDouble(),
                  isTouched: i == touchedIndex,
                  barColor: Color.fromRGBO(212, 252, 178, 1));
            case 5:
              return makeGroupDataBar(5, expenseList[5].floorToDouble(),
                  isTouched: i == touchedIndex,
                  barColor: Color.fromRGBO(223, 225, 155, 1));
            default:
              return null;
          }
        }
      });

  BarChartData mainBarData() {
    return BarChartData(
      barTouchData: BarTouchData(
        touchTooltipData: BarTouchTooltipData(
            tooltipBgColor: Colors.blueGrey,
            getTooltipItem: (group, groupIndex, rod, rodIndex) {
              String weekDay;
              switch (group.x.toInt()) {
                case 0:
                  weekDay = 'Frais Comptables';
                  break;
                case 1:
                  weekDay = 'Télécommunication';
                  break;
                case 2:
                  weekDay = 'Publicité et Marketing';
                  break;
                case 3:
                  weekDay = 'Automobile Expense';
                  break;
                case 4:
                  weekDay = 'Frais de Consultant';
                  break;
                case 5:
                  weekDay = 'Frais de Carte de Crédit';
                  break;
              }
              return BarTooltipItem(
                weekDay + '\n',
                TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: (rod.y - 1).toString(),
                    style: TextStyle(
                      color: Colors.yellow,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              );
            }),
        touchCallback: (barTouchResponse) {
          setState(() {
            if (barTouchResponse.spot != null &&
                barTouchResponse.touchInput is! PointerUpEvent &&
                barTouchResponse.touchInput is! PointerExitEvent) {
              touchedIndex = barTouchResponse.spot.touchedBarGroupIndex;
            } else {
              touchedIndex = -1;
            }
          });
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: false,
          getTextStyles: (value) => const TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14),
          margin: 16,
          getTitles: (double value) {
            switch (value.toInt()) {
              case 0:
                return '';
              case 1:
                return '';
              case 2:
                return '';
              case 3:
                return '';
              case 4:
                return '';
              case 5:
                return '';
              case 6:
                return '';
              default:
                return '';
            }
          },
        ),
        leftTitles: SideTitles(
          showTitles: false,
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      barGroups: showingGroups(),
    );
  }

  BarChartData randomData() {
    return BarChartData(
      barTouchData: BarTouchData(
        enabled: false,
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14),
          margin: 16,
          getTitles: (double value) {
            switch (value.toInt()) {
              case 0:
                return '';
              case 1:
                return '';
              case 2:
                return '';
              case 3:
                return '';
              case 4:
                return '';
              case 5:
                return '';
              case 6:
                return '';
              default:
                return '';
            }
          },
        ),
        leftTitles: SideTitles(
          showTitles: false,
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      barGroups: List.generate(7, (i) {
        switch (i) {
          case 0:
            return makeGroupDataBar(0, Random().nextInt(15).toDouble() + 6,
                barColor: widget.availableColors[
                    Random().nextInt(widget.availableColors.length)]);
          case 1:
            return makeGroupDataBar(1, Random().nextInt(15).toDouble() + 6,
                barColor: widget.availableColors[
                    Random().nextInt(widget.availableColors.length)]);
          case 2:
            return makeGroupDataBar(2, Random().nextInt(15).toDouble() + 6,
                barColor: widget.availableColors[
                    Random().nextInt(widget.availableColors.length)]);
          case 3:
            return makeGroupDataBar(3, Random().nextInt(15).toDouble() + 6,
                barColor: widget.availableColors[
                    Random().nextInt(widget.availableColors.length)]);
          case 4:
            return makeGroupDataBar(4, Random().nextInt(15).toDouble() + 6,
                barColor: widget.availableColors[
                    Random().nextInt(widget.availableColors.length)]);
          case 5:
            return makeGroupDataBar(5, Random().nextInt(15).toDouble() + 6,
                barColor: widget.availableColors[
                    Random().nextInt(widget.availableColors.length)]);
          case 6:
            return makeGroupDataBar(6, Random().nextInt(15).toDouble() + 6,
                barColor: widget.availableColors[
                    Random().nextInt(widget.availableColors.length)]);
          default:
            return null;
        }
      }),
    );
  }

  Future<dynamic> refreshState() async {
    setState(() {});
    await Future<dynamic>.delayed(
        animDuration + const Duration(milliseconds: 50));
    if (isPlaying) {
      refreshState();
    }
  }

  Widget spin() {
    return Padding(
      padding: EdgeInsets.only(top: 200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Text(
              "Birou",
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 27,
                  fontWeight: FontWeight.w500),
            ),
          ),
          SpinKitCubeGrid(
            itemBuilder: (BuildContext context, int index) {
              return DecoratedBox(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.1, 0.4, 0.7, 0.9],
                colors: [
                  Colors.blueAccent,
                  Colors.redAccent,
                  Colors.greenAccent,
                  Colors.grey,
                ],
              )));
            },
            size: 70.0,
          ),
        ],
      ),
    );
  }
}

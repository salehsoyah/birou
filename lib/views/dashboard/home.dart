import 'dart:async';
import 'dart:math';

import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/contacts/contactsList.dart';
import 'package:birou/views/dashboard/dashboard.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:birou/views/sales/estimate/estimates.dart';
import 'package:birou/views/sales/invoice/invoices.dart';
import 'package:birou/views/sales/payment/payments.dart';
import 'package:birou/views/sales/sales.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:bubbled_navigation_bar/bubbled_navigation_bar.dart';
import 'package:clay_containers/clay_containers.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {

  final List<Company> companies;
  String companyId;
  Widget currentScreen;

  Home({Key key, @required this.companies, @required this.companyId, @required this.currentScreen});

  final titles = ['Dashboard', 'Clients', 'Invoices', 'Estimates', 'Payments'];

  final colors = [
    Color.fromRGBO(55, 86, 223, 1),
    Color.fromRGBO(55, 86, 223, 1),
    Color.fromRGBO(55, 86, 223, 1),
    Color.fromRGBO(55, 86, 223, 1),
    Color.fromRGBO(55, 86, 223, 1),
  ];
  final icons = [
    AssetImage("assets/images/icon5.png"),
    AssetImage("assets/images/icon1.png"),
    AssetImage("assets/images/icon2.png"),
    AssetImage("assets/images/icon3.png"),
    AssetImage("assets/images/icon4.png"),
  ];

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int currentTab = 0;
  int currentIndex = 0;
  // final List<Widget> screens = [
  //   Dashboard(),
  //   Contact(),
  //   Sales(),
  // ];

  final PageStorageBucket bucket = PageStorageBucket();

  Widget currentScreen = Dashboard();
  MenuPositionController _menuPositionController;

  @override
  void initState() {
    setState(() {
      currentScreen = widget.currentScreen;
      _menuPositionController = MenuPositionController(initPosition: 0);
    });

    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      backgroundColor: Color.fromRGBO(245, 246, 252, 1),
      bottomNavigationBar: BubbledNavigationBar(
        controller: _menuPositionController,
        itemMargin: EdgeInsets.only(left: 30, right: 30),
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        defaultBubbleColor: Colors.blue,
        initialIndex: 2,
        onTap: (index) {
          switch (index) {
            case 0:
              setState(() {
                currentScreen = Dashboard(companies: widget.companies,companyId: widget.companyId,);
              });
              break;
            case 1:
              setState(() {
                currentScreen = Contact(companyId: widget.companyId,companies: widget.companies,);
              });
              break;
            case 2:
              setState(() {
                currentScreen = Invoices(companyId: widget.companyId,companies: widget.companies);
              });
              break;
            case 3:
              setState(() {
                currentScreen = Estimates(companyId: widget.companyId,companies: widget.companies);
              });
              break;
            case 4:
              setState(() {
                currentScreen = Payments(companyId: widget.companyId,companies: widget.companies);
              });
              break;
          }
        },
        items: widget.titles.map(
              (title) {
            var index = widget.titles.indexOf(title);
            var color = widget.colors[index];
            return BubbledNavigationBarItem(
              icon: getIcon(index),
              bubbleColor: color,
              title: Row(
                children: [
                  Text(
                    title,
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  SizedBox(width: 18)
                ],
              ),
            );
          },
        ).toList(),
      ),
    );
  }

  Padding getIcon(int index) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 3),
      child: Image(
        image: widget.icons[index],
      ),
    );
  }

}

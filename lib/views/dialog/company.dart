import 'package:birou/generated/l10n.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CompanyDialog extends StatefulWidget {
  @override
  _CompanyDialogState createState() => _CompanyDialogState();
  String text1;
  String text2;
  int image;

  CompanyDialog({
    Key key,
    @required this.text1,
    @required this.text2,
    @required this.image,
  }) : super(key: key);
}

class _CompanyDialogState extends State<CompanyDialog> {
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
      elevation: 3,
      backgroundColor: Colors.transparent,
      child: SafeArea(
          child: Container(
              height: MediaQuery.of(context).size.height / 1.75,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(18),
                boxShadow: [
                  BoxShadow(
                    color: Colors.blueAccent.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Image(
                    image: AssetImage(widget.image == 1
                        ? 'assets/images/popupimage.png'
                        : 'assets/images/browser.png'),
                    width: width / 2.5,
                    height: height / 5,
                  ),
                  SizedBox(
                    height: height / 15,
                  ),
                  Text(
                    widget.text1,
                    style: TextStyle(fontSize: height / 30),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: height / 60),
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0, left: 10.0),
                    child: Text(
                      widget.text2,
                      style: TextStyle(
                          fontSize: height / 45, color: Colors.grey[600]),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 17,
                  ),
                  RaisedButton(
                      color: Color.fromRGBO(55, 86, 223, 1),
                      onPressed: () => {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) => MainMenu()))
                          },
                      // padding: const EdgeInsets.all(0.0),
                      child: Container(
                        color: Color.fromRGBO(55, 86, 223, 1),
                        width: width / 1.7,
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(left: 10.0),
                                child: Text(
                                  AppLocalizations.of(context).go_to_menu,
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                )),
                          ],
                        ),
                      ),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0))),
                ],
              ))),
    );
  }
}

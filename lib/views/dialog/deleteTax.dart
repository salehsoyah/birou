import 'package:birou/controllers/companyController.dart';
import 'package:birou/views/company/additional/inputs.dart';
import 'package:birou/views/company/method/methods.dart';
import 'package:birou/views/company/tax/taxs.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:flutter/material.dart';

class DeleteTax extends StatefulWidget {
  @override
  _DeleteTaxState createState() => _DeleteTaxState();
  String id;
  String companyId;

  DeleteTax({
    Key key,
    @required this.id,
    @required this.companyId

  }) : super(key: key);
}

class _DeleteTaxState extends State<DeleteTax> {
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
      elevation: 3,
      backgroundColor: Colors.transparent,
      child: SafeArea(
          child: Container(
              height: MediaQuery.of(context).size.height / 1.5,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(18),
                boxShadow: [
                  BoxShadow(
                    color: Colors.blueAccent.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Image(
                    image: AssetImage('assets/images/popupimage.png'),
                    width: width / 2.5,
                    height: height / 5,
                  ),
                  SizedBox(
                    height: height / 15,
                  ),
                  Text(
                    'Delete',
                    style: TextStyle(fontSize: height / 30),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: height / 60),
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0, left: 10.0),
                    child: Text(
                      'Are you sure you want to delete this tax ?',
                      style: TextStyle(fontSize:  height / 45, color: Colors.grey[600]),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 13,
                  ),
                  RaisedButton(
                      color: Color.fromRGBO(55, 86, 223, 1),
                      onPressed: () => {

                        deleteTax(widget.id).then((result){
                          if(result['status']['code'] == 200)
                          {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) => Taxs()));
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(image: 1, text1: 'Success', text2: result['status']['message'], );
                                });
                          }else{
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(image: 0, text1: 'Error', text2: result['status']['message'], );
                                });
                          }
                        }),
                      },
                      // padding: const EdgeInsets.all(0.0),
                      child: Container(
                        color: Color.fromRGBO(55, 86, 223, 1),
                        width: width / 1.7,
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(left: 10.0),
                                child: Text(
                                  "Confirm",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                )),
                          ],
                        ),
                      ),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0))),
                  SizedBox(height: height / 45,),
                  RaisedButton(
                      color: Colors.red,
                      onPressed: () => {
                        Navigator.pop(context),
                      },
                      // padding: const EdgeInsets.all(0.0),
                      child: Container(
                        color: Colors.red,
                        width: width / 1.7,
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(left: 10.0),
                                child: Text(
                                  "Exit",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                )),
                          ],
                        ),
                      ),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0))),
                ],
              ))),
    );
  }
}

import 'package:flutter/material.dart';

class LogoDialog extends StatefulWidget {
  @override
  _LogoDialogState createState() => _LogoDialogState();
  String logo;
  LogoDialog({
    @required this.logo,
    Key key,

  }) : super(key: key);
}

class _LogoDialogState extends State<LogoDialog> {
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
      elevation: 3,
      backgroundColor: Colors.transparent,
      child: SafeArea(
          child: Container(
              height: MediaQuery.of(context).size.height / 2,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(18),
                boxShadow: [
                  BoxShadow(
                    color: Colors.blueAccent.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  Image.network("https://www.birou.tn/" + widget.logo),
                  SizedBox(
                    height: 10,
                  ),
                  RaisedButton(
                      color: Color.fromRGBO(55, 86, 223, 1),
                      onPressed: () => {
                        Navigator.pop(context),
                      },
                      // padding: const EdgeInsets.all(0.0),
                      child: Container(
                        color: Color.fromRGBO(55, 86, 223, 1),
                        width: width / 1.7,
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(left: 10.0),
                                child: Text(
                                  "OK",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                )),
                          ],
                        ),
                      ),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0))),
                ],
              ))),
    );
  }
}

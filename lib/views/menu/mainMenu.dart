import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/controllers/estimateController.dart';
import 'package:birou/controllers/invoiceController.dart';
import 'package:birou/controllers/paymentController.dart';
import 'package:birou/models/company.dart';

import 'package:birou/providers/local_provider.dart';
import 'package:birou/views/company/addCompany/general_information.dart';
import 'package:birou/views/company/additional/inputs.dart';
import 'package:birou/views/company/collaborators/collaborators.dart';
import 'package:birou/views/company/logs/logs.dart';
import 'package:birou/views/company/tax/taxs.dart';
import 'package:birou/views/company/updateCompany/update_general_information.dart';
import 'package:birou/views/dashboard/dashboard.dart';
import 'package:birou/views/dashboard/home.dart';
import 'package:birou/views/sales/sales.dart';
import 'package:flutter/material.dart';
import 'package:gson/gson.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MainMenu extends StatefulWidget {
  @override
  final List<Company> companies;

  MainMenu({Key key, this.companies});

  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  String companyId;
  String companyName;
  String lang = 'en';
  List<Company> companies = [];

  getCompanyId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("companyId");
  }

  getLang() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("lang");
  }

  bool hasCompanies = false;

  void initState() {
    userCompanies().then((result) {
      setState(() {
        companies = result;
        print(companies[2].logo);
      });
      if (result.length > 0) {
        setState(() {
          hasCompanies = true;
        });
      }
    });

    getLang().then((lang) {
      setState(() {
        lang = lang;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height = MediaQuery.of(context).size.height -
        padding.top -
        padding.bottom -
        AppBar().preferredSize.height;
    double width = MediaQuery.of(context).size.width;
    final provider = Provider.of<LocaleProvider>(context);
    final locale = provider.locale ?? Locale('en');

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(width / 30),
        child: AppBar(
          title: Container(),
          elevation: 0,
          backgroundColor: Colors.blue[700],
        ),
      ),
      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    color: Colors.white,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          SizedBox(
                            width: width / 16.5,
                          ),
                          if (widget.companies != null) ...[
                            for (int i = 0;
                                i < widget.companies.length;
                                i++) ...[
                              GestureDetector(
                                  onTap: () async {
                                    SharedPreferences sharedPreferences =
                                        await SharedPreferences.getInstance();
                                    sharedPreferences.setString(
                                        "lastVisitedCompanyId",
                                        widget.companies[i].hashed_id);

                                    Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                Home(
                                                  companies: widget.companies,
                                                  companyId: widget
                                                      .companies[i].hashed_id,
                                                  currentScreen: Dashboard(
                                                    companies: widget.companies,
                                                    companyId: widget
                                                        .companies[i].hashed_id,
                                                  ),
                                                )));
                                  },
                                  child: company(
                                      width,
                                      widget.companies[i].title,
                                      widget.companies[i].logo)),
                            ],
                          ] else ...[
                            for (int i = 0; i < companies.length; i++) ...[
                              GestureDetector(
                                  onTap: () async {
                                    SharedPreferences sharedPreferences =
                                        await SharedPreferences.getInstance();
                                    sharedPreferences.setString(
                                        "lastVisitedCompanyId",
                                        companies[i].hashed_id);

                                    Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                Home(
                                                  companies: companies,
                                                  companyId:
                                                      companies[i].hashed_id,
                                                  currentScreen: Dashboard(
                                                      companies: companies,
                                                      companyId: companies[i]
                                                          .hashed_id),
                                                )));
                                  },
                                  child: company(width, companies[i].title,
                                      companies[i].logo)),
                            ],
                          ],
                          companies.length > 3
                              ? Container()
                              : GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                GeneralInformation(
                                                  hasCompanies: hasCompanies,
                                                  companies: companies,
                                                )));
                                  },
                                  child: Column(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                              top: width * 1.39,
                                              left: width / 40),
                                          width: width / 5.33,
                                          height: width / 5.33,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey,
                                                  spreadRadius: 0.03,
                                                  blurRadius: 7,
                                                  offset: Offset(0,
                                                      3), // changes position of shadow
                                                ),
                                              ]),
                                          child: Icon(Icons.add)),
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: width / 19, top: width / 25),
                                        child: Text(
                                          AppLocalizations.of(context)
                                              .new_company,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  height: height / 2,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50),
                        bottomRight: Radius.circular(50)),
                    color: Colors.blue[700],
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            right: width / 13.3,
                            left: width / 13.3,
                            bottom: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: width / 4,
                            ),
                            Text(
                              AppLocalizations.of(context).main_menu,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                            SizedBox(
                              width: width / 4,
                            ),
                            Icon(
                              Icons.close_sharp,
                              color: Colors.white,
                              size: 20,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: width / 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            GestureDetector(
                              onTap: () async {
                                provider.setLocale(Locale('ar'));
                                SharedPreferences sharedPreferences =
                                    await SharedPreferences.getInstance();
                                sharedPreferences.setString("lang", 'ar');
                              },
                              child: Image(
                                  image: AssetImage(
                                      "assets/images/tunisia_flag.png")),
                            ),
                            SizedBox(
                              width: width / 70,
                            ),
                            GestureDetector(
                                onTap: () async {
                                  provider.setLocale(Locale('en'));
                                  SharedPreferences sharedPreferences =
                                      await SharedPreferences.getInstance();
                                  sharedPreferences.setString("lang", 'en');
                                },
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/usa_flag.png"))),
                            SizedBox(
                              width: width / 70,
                            ),
                            GestureDetector(
                              onTap: () async {
                                provider.setLocale(Locale('fr'));
                                SharedPreferences sharedPreferences =
                                    await SharedPreferences.getInstance();
                                sharedPreferences.setString("lang", 'fr');
                              },
                              child: Image(
                                  image: AssetImage(
                                      "assets/images/france_flag.png")),
                            ),
                          ],
                        ),
                      ),
                      Center(
                        child: Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: width / 15, vertical: width / 15.5),
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(10, 84, 157, 1),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0))),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              // Expanded(
                              //   flex: 1,
                              //   child: TextField(
                              //     decoration: InputDecoration(
                              //       contentPadding: const EdgeInsets.symmetric(
                              //           vertical: 25.0),
                              //       fillColor: Color.fromRGBO(10, 84, 157, 1),
                              //       filled: true,
                              //       border: OutlineInputBorder(
                              //           borderRadius: BorderRadius.all(
                              //             Radius.circular(25),
                              //           ),
                              //           borderSide: BorderSide.none),
                              //       hintText: "      " +
                              //           AppLocalizations.of(context).explore,
                              //       hintStyle: TextStyle(
                              //         color: Colors.white,
                              //       ),
                              //       suffixIcon: Padding(
                              //         padding: const EdgeInsets.only(right: 10),
                              //         child: Container(
                              //             width: width / 7.3,
                              //             height: width / 7.3,
                              //             decoration: BoxDecoration(
                              //               color:
                              //                   Color.fromRGBO(55, 86, 233, 1),
                              //               borderRadius:
                              //                   BorderRadius.circular(10),
                              //             ),
                              //             child: Icon(
                              //               Icons.search,
                              //               color: Colors.white,
                              //               size: 25,
                              //             )),
                              //       ),
                              //     ),
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: width / 3,
                  left: width / 15,
                  right: width / 15,
                  child: Container(
                    width: width * 0.88,
                    height: width * 0.96,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blueAccent,
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          GestureDetector(
                            onTap: () async {
                              SharedPreferences sharedPreferences =
                                  await SharedPreferences.getInstance();
                              String companyId = sharedPreferences
                                  .getString("lastVisitedCompanyId");

                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          UpdateGeneralInformation(
                                            companyId: companyId,
                                            companies: companies,
                                          )));
                            },
                            child: Row(
                              children: [
                                Image(
                                  image: AssetImage("assets/images/circle.png"),
                                ),
                                SizedBox(
                                  width: width / 20,
                                ),
                                Text(
                                  AppLocalizations.of(context).settings,
                                  style: TextStyle(fontSize: 17),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          Collaborators()));
                            },
                            child: Row(
                              children: [
                                Image(
                                  image: AssetImage("assets/images/circle.png"),
                                ),
                                SizedBox(
                                  width: width / 20,
                                ),
                                Text(
                                  AppLocalizations.of(context).collaborators,
                                  style: TextStyle(fontSize: 17),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          Inputs()));
                            },
                            child: Row(
                              children: [
                                Image(
                                  image: AssetImage("assets/images/circle.png"),
                                ),
                                SizedBox(
                                  width: width / 20,
                                ),
                                Text(
                                  AppLocalizations.of(context)
                                      .additional_inputs,
                                  style: TextStyle(fontSize: 17),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          Taxs()));
                            },
                            child: Row(
                              children: [
                                Image(
                                  image: AssetImage("assets/images/circle.png"),
                                ),
                                SizedBox(
                                  width: width / 20,
                                ),
                                Text(
                                  AppLocalizations.of(context).taxes,
                                  style: TextStyle(fontSize: 17),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Row(
                              children: [
                                Image(
                                  image: AssetImage("assets/images/circle.png"),
                                ),
                                SizedBox(
                                  width: width / 20,
                                ),
                                Text(
                                  AppLocalizations.of(context).customization,
                                  style: TextStyle(fontSize: 17),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          Logs()));
                            },
                            child: Row(
                              children: [
                                Image(
                                  image: AssetImage("assets/images/circle.png"),
                                ),
                                SizedBox(
                                  width: width / 20,
                                ),
                                Text(
                                  AppLocalizations.of(context).logs,
                                  style: TextStyle(fontSize: 17),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget company(double width, String company, String logo) {
    return Padding(
      padding: EdgeInsets.only(right: width / 15),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: width * 1.39),
            width: width / 5.33,
            height: width / 5.33,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    spreadRadius: 0.03,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ]),
            child: logo != null && logo.isNotEmpty
                ? ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: Image.network('https://www.birou.tn/' + logo))
                : Icon(
                    Icons.info_outline,
                    size: 25,
                    color: Colors.grey,
                  ),
          ),
          Container(
            padding: EdgeInsets.only(top: width / 25),
            child: Text(company != null ? company : ""),
          )
        ],
      ),
    );
  }
}

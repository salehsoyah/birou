import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/contactsController.dart';
import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/controllers/deliveryNotesController.dart';
import 'package:birou/controllers/stockController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/generated/l10n.dart';
import 'package:birou/models/bank.dart';
import 'package:birou/models/client.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/device.dart';
import 'package:birou/models/input.dart';
import 'package:birou/models/item.dart';
import 'package:birou/models/language.dart';
import 'package:birou/models/tax.dart';
import 'package:birou/models/inputAdd.dart';

import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/dashboard/home.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:birou/views/sales/delivery/addDelivery/deliverySetting.dart';
import 'package:birou/views/sales/delivery/addDelivery/form.dart';
import 'package:birou/views/sales/sales.dart';
import 'package:birou/views/subscriptions/last_step.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:date_field/date_field.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_validator/form_validator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddDelivery extends StatefulWidget {
  @override
  String companyId;
  final List<Company> companies;

  AddDelivery({Key key, @required this.companyId, @required this.companies});

  _AddDeliveryState createState() => _AddDeliveryState();
}

class _AddDeliveryState extends State<AddDelivery> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();

  bool obscureText = true;
  String mobile_token;
  List<Country> countries = [];
  String country;
  String countryText;
  bool btnPressed = false;
  Future listCountries;
  Future companyTaxes;
  Future banksList;
  Future clientList;

  List<Tax> taxes = [
    Tax("Hors Taxes", 1, "", 0),
    Tax("Taxes incluses", 2, "", 0),
  ];

  String discount = '0';

  String pdfText;
  List<Language> languages = [
    Language("Français", "fr"),
    Language("English", "en"),
    Language("Arabic", "ar")
  ];

  int taxPicked = 1;
  String taxText;

  List<Clientt> clients = [];
  String clientText;
  String client;

  String conditionText;
  List<Condition> conditions = [
    Condition("Oui", 1),
    Condition("Non", 0),
  ];

  DateTime date = DateTime.now();

  bool condition = true;
  bool stamp = false;
  bool billing = true;
  bool delivery = true;
  bool bank = true;
  int currency = 1;
  List<String> inputs = [];
  String pdfLanguage;
  String bankId;

  final _deliveryNumberController = TextEditingController();
  final _referenceController = TextEditingController();
  final _generalConditionsController = TextEditingController();
  final _notesController = TextEditingController();
  final _discountController = TextEditingController();
  final _totalController = TextEditingController();
  final _subtotalController = TextEditingController();
  final _taxesController = TextEditingController();

  List<ItemForm> items = [];
  List<Bank> banks = [];

  var articles;

  String bankText;

  void initState() {

    setState(() {
      var item = Item();
      items.add(ItemForm(
        item: item,
        onDelete: () => onDelete(item),
        companyId: widget.companyId,
        notifyParent: refresh,
        printTax: printTax,
      ));
    });

    taxText = taxes[0].title;
    companyTaxes = companyTax();
    banksList = bankList(widget.companyId);
    clientList = clientsList(widget.companyId);

    clientsList(widget.companyId).then((value) {
      setState(() {
        clients = value;
      });
    });

    bankList(widget.companyId).then((value) {
      setState(() {
        banks = value;
        bankId = value[0].id;
        bankText = value[0].title;
      });
    });

    nextDelivery(widget.companyId).then((value) {
      setState(() {
        _deliveryNumberController.text = value['data']['nextDeliveryNumber'];
      });
    });

    companyDashboard(widget.companyId).then((result) {
      setState(() {
        pdfLanguage = result['data']['company']['language'];
      });
    });

    bankList(widget.companyId).then((value) {
      setState(() {
        bankId = value[0].id;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).new_delivery,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return DeliverySettings(
                        companyId: widget.companyId,
                        companies: widget.companies,
                        language: pdfLanguage,
                        conditions: condition,
                        billing: billing,
                        currency: currency,
                        bank: bank,
                        bankId: bankId,
                        stamp: stamp,
                        delivery: delivery,
                        inputs: inputs,
                      );
                    }).then((returnValue) {
                  print(returnValue);
                  if(returnValue != false){
                    setState(() {
                      pdfLanguage = returnValue['pdfLanguage'];
                      condition = returnValue['condition'];
                      billing = returnValue['billing'];
                      currency = returnValue['currency'];
                      bank = returnValue['bank'];
                      bankId = returnValue['bankId'];
                      stamp = returnValue['stamp'];
                      delivery = returnValue['delivery'];
                      inputs = returnValue['inputs'];

                    });
                  }
                });
              },
              child: Icon(
                Icons.format_list_bulleted_rounded,
                color: Colors.blue,
              ),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _form,
          child: Container(
            color: Color.fromRGBO(245, 246, 252, 1),
            child: Column(
              children: [
                SizedBox(
                  height: width / 11,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: width / 11,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          btnPressed = true;
                        });
                        onSave();
                        // print(articles);
                        if (_form.currentState.validate() &&
                            taxPicked != null &&
                            client != null &&
                            articles != null) {
                          newDelivery(
                                  date.toString().substring(0, 10),
                                  _deliveryNumberController.text,
                                  _generalConditionsController.text,
                                  _referenceController.text,
                                  _notesController.text,
                                  _discountController.text == ''
                                      ? null
                                      : double.parse(_discountController.text),
                                  taxPicked,
                                  client,
                                  condition == true ? 1 : 0,
                                  stamp == true ? 1 : 0,
                                  billing == true ? 1 : 0,
                                  delivery == true ? 1 : 0,
                                  bank == true ? 1 : 0,
                                  bankId,
                                  1,
                                  0,
                                  currency,
                                  pdfLanguage,
                                  articles,
                                  inputs,
                                  widget.companyId)
                              .then((value) {
                            if (value['status']['code'] != 200) {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return DialogScreen(
                                      image: 0,
                                      text1: 'Error',
                                      text2: value['status']['message'],
                                    );
                                  });
                            } else {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return DialogScreen(
                                      image: 1,
                                      text1: 'Success',
                                      text2: value['status']['message'],
                                    );
                                  });
                            }
                            print(value);
                          });
                        }
                      },
                      child: Container(
                        width: width / 6,
                        height: width / 6,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.blue[100],
                                spreadRadius: 0.5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 1), // changes position of shadow
                              ),
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 9.0, bottom: 2.0, top: 2.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Icon(
                                    Icons.circle,
                                    color: Colors.grey[300],
                                    size: 7,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Image.asset(
                                "assets/images/icon10.png",
                                fit: BoxFit.contain,
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Text('Draft')
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: width / 15,
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        width: width / 6,
                        height: width / 6,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.blue[100],
                                spreadRadius: 0.5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 1), // changes position of shadow
                              ),
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 9.0, bottom: 2.0, top: 2.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Icon(
                                    Icons.circle,
                                    color: Colors.grey[300],
                                    size: 7,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Image.asset(
                                "assets/images/icon11.png",
                                fit: BoxFit.contain,
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Text('Send')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        DateTimeFormField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            hintStyle: TextStyle(color: Colors.black45),
                            errorStyle: TextStyle(color: Colors.redAccent),
                            suffixIcon: Icon(Icons.event_note),
                            labelText: date != null
                                ? date.toString().substring(0, 10)
                                : AppLocalizations.of(context).date,
                          ),
                          firstDate:
                              DateTime.now().add(const Duration(days: 10)),
                          initialDate:
                              DateTime.now().add(const Duration(days: 10)),
                          autovalidateMode: AutovalidateMode.always,
                          validator: (DateTime e) =>
                              (date == null) ? 'Please enter date' : null,
                          onDateSelected: (DateTime value) {
                            print(value);
                            setState(() {
                              date = value;
                            });
                          },
                        ),
                        SizedBox(
                          height: width / 50,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      AppLocalizations.of(context).delivery_nb,
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                    Container(
                      width: width * 0.38,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.blue[100],
                              spreadRadius: 0.5,
                              blurRadius: 7,
                              offset:
                                  Offset(0, 1), // changes position of shadow
                            ),
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          validator: (val) => val.length < 1
                              ? 'Delivery number is required'
                              : null,
                          controller: _deliveryNumberController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'DN-2021-',
                              hintStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 27,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).client,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  clientText != null
                                      ? clientText
                                      : "Foulen ben foulen",
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && client == null
                                    ? Text(
                                        AppLocalizations.of(context)
                                            .enter_client,
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 12),
                                      )
                                    : Container()
                              ],
                            ),
                            FutureBuilder(
                              future: clientList,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text(
                                      AppLocalizations.of(context).loading);
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                                value: value.id,
                                                child: Text(
                                                  value.display_name,
                                                ),
                                              ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      client = value;
                                    });

                                    for (var i in clients) {
                                      if (value == i.id) {
                                        setState(() {
                                          clientText = i.display_name;
                                          print(i.display_name);
                                        });
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          controller: _referenceController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText:
                                  AppLocalizations.of(context).reference_nb,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).item_prices_are,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  taxText != null ? taxText : "Choose tax",
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && taxPicked == null
                                    ? Text(
                                        "Choose tax",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 12),
                                      )
                                    : Container()
                              ],
                            ),
                            PopupMenuButton<int>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              onSelected: (int result) {
                                setState(() {
                                  taxPicked = result;
                                });

                                for (var i in taxes) {
                                  if (i.rate == taxPicked) {
                                    print(i.title);
                                    setState(() {
                                      taxText = i.title;
                                    });
                                  }
                                }
                                printTax();
                              },
                              itemBuilder: (BuildContext context) =>
                                  <PopupMenuEntry<int>>[
                                PopupMenuItem<int>(
                                  value: taxes[0].rate,
                                  child: Text(taxes[0].title),
                                ),
                                PopupMenuItem<int>(
                                  value: taxes[1].rate,
                                  child: Text(taxes[1].title),
                                ),
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: 0.0, left: width / 11, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        AppLocalizations.of(context).items,
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                      GestureDetector(
                        onTap: () {
                          onAddForm();
                        },
                        child: Icon(
                          Icons.add,
                          size: width / 14,
                          color: Colors.grey[700],
                        ),
                      ),
                    ],
                  ),
                ),
                btnPressed == true && items.length < 1
                    ? Padding(
                        padding: EdgeInsets.only(
                            top: 0.0, left: width / 11, right: 30.0),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              AppLocalizations.of(context).add_items,
                              style: TextStyle(color: Colors.red, fontSize: 12),
                            )),
                      )
                    : Container(),
                SizedBox(
                  height: width / 27,
                ),
                items.length > 0
                    ? ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        addAutomaticKeepAlives: true,
                        itemCount: items.length,
                        itemBuilder: (_, i) => Padding(
                          padding: const EdgeInsets.only(left: 15.0, right: 15),
                          child: items[i],
                        ),
                      )
                    : Container(),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        Column(
                          children: [
                            _subtotalController.text.isNotEmpty
                                ? Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      AppLocalizations.of(context).subtotal,
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  )
                                : Container(),
                            TextFormField(
                              controller: _subtotalController,
                              enabled: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText:
                                      AppLocalizations.of(context).subtotal,
                                  hintStyle: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 17,
                                      fontWeight: FontWeight.normal)),
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                          controller: _discountController,
                          onChanged: (dscnt) {
                            setState(() {
                              discount = dscnt;
                            });
                            printTax();
                          },
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).discount,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        Column(
                          children: [
                            _taxesController.text.isNotEmpty
                                ? Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      AppLocalizations.of(context).taxes,
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  )
                                : Container(),
                            TextFormField(
                              controller: _taxesController,
                              enabled: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: AppLocalizations.of(context).taxes,
                                  hintStyle: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 17,
                                      fontWeight: FontWeight.normal)),
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        Column(
                          children: [
                            _totalController.text.isNotEmpty
                                ? Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      AppLocalizations.of(context).total_ttc,
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  )
                                : Container(),
                            TextFormField(
                              controller: _totalController,
                              enabled: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText:
                                      AppLocalizations.of(context).total_ttc,
                                  hintStyle: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 17,
                                      fontWeight: FontWeight.normal)),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        TextFormField(
                          controller: _notesController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).notes,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        TextFormField(
                          maxLines: 6,
                          controller: _generalConditionsController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context)
                                  .general_conditions,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onDelete(Item item) {
    setState(() {
      var find = items.firstWhere(
        (it) => it.item == item,
        orElse: () => null,
      );
      if (find != null) items.removeAt(items.indexOf(find));
    });
    printTax();
  }

  void onAddForm() {
    setState(() {
      var item = Item();
      items.add(ItemForm(
        item: item,
        onDelete: () => onDelete(item),
        companyId: widget.companyId,
        notifyParent: refresh,
        printTax: printTax,
      ));
    });
  }

  void refresh() {
    setState(() {});
  }

  void onSave() {
    if (items.length > 0) {
      var allValid = true;
      items.forEach((form) => {allValid = allValid && form.isValid()});
      if (allValid) {
        var data = items.map((it) => jsonEncode(it.item)).toList();
        setState(() {
          articles = data;
        });
      }
    }
  }

  double sumTotal(List<String> prices, String discount) {
    double sum = 0;
    double sumWD = -1;
    prices.forEach(
        (price) => {price != null ? sum = sum + double.parse(price) : sum});

    if (discount != '0' && discount != '') {
      sumWD = sum - double.parse(discount);
    }

    if (sumWD > 0) {
      return sumWD;
    } else {
      return sum;
    }
  }

  double sumSubTotal(List<String> prices) {
    double sum = 0;
    prices.forEach(
        (price) => {price != null ? sum = sum + double.parse(price) : sum});

    return sum;
  }

  double sumTaxesTotal(List<String> prices) {
    double sum = 0;
    setState(() {
      prices.forEach((price) => {
            price != null ? sum = sum + double.parse(price) : sum,
          });
    });

    return sum;
  }

  void printTax() {
    double taxes = 0;
    double subtotal = 0;
    double total = 0;

    items.forEach((item) {
      var qty = item.qty != null ? item.qty : 0;
      var pu = item.up != null ? item.up : 0;

      var newPu = double.parse(pu.toString()) * qty;

      if (taxPicked == 1) {
        item.state.taxForms.forEach((taxForm) {
          if (taxForm.special == 1) {
            var tax = taxForm.rate;
            newPu = newPu + double.parse((newPu * (tax / 100)).toString());
          }
        });
        item.state.taxForms.forEach((taxForm) {
          var tax = taxForm.rate;
          if (taxForm.special == 1) {
            taxes = taxes + ((pu * (tax / 100)) * (qty));
          } else {
            taxes = taxes + (newPu * (tax / 100));
          }
        });
      } else {
        item.state.taxForms.forEach((taxForm) {
          if (taxForm.special == 0) {
            var tax = taxForm.rate;

            taxes = taxes + (((pu * (tax / 100)) * qty) / ((tax / 100) + 1));

            newPu = newPu - taxes;
          }
        });
        item.state.taxForms.forEach((taxForm) {
          var tax = taxForm.rate;
          if (taxForm.special == 1) {
            taxes = taxes + ((newPu * (tax / 100)) / ((tax / 100) + 1));
          }
        });
      }

      setState(() {
        total = total + newPu;
      });
    });

    double discount = _discountController.text != ''
        ? double.parse(_discountController.text)
        : 0;

    if (discount < 0) {
      setState(() {
        discount = 0;
      });
    }
    setState(() {
      _subtotalController.text = (total).toStringAsFixed(2).toString();
      _taxesController.text = taxes.toStringAsFixed(2).toString();
    });
    if (taxPicked == 1) {
      if (((total + taxes) - discount) > 0) {
        setState(() {
          _totalController.text =
              ((total + taxes) - discount).toStringAsFixed(2).toString();
        });
      } else {
        setState(() {
          _totalController.text =
              ((total + taxes)).toStringAsFixed(2).toString();
        });
      }
    } else {
      if (total - discount > 0) {
        setState(() {
          _totalController.text =
              (total - discount).toStringAsFixed(2).toString();
        });
      } else {
        setState(() {
          _totalController.text = total.toStringAsFixed(2).toString();
        });
      }
    }
    print('total  = ' + total.toString());
  }
}

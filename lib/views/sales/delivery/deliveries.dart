// import 'dart:math';
//
// import 'package:birou/controllers/companyController.dart';
// import 'package:birou/controllers/contactsController.dart';
// import 'package:birou/controllers/dashboardController.dart';
// import 'package:birou/controllers/deliveryNotesController.dart';
// import 'package:birou/controllers/estimateController.dart';
// import 'package:birou/models/company.dart';
// import 'package:birou/models/condition.dart';
// import 'package:birou/models/deadline.dart';
// import 'package:birou/views/auth/login.dart';
// import 'package:birou/views/company/additional/addInput.dart';
// import 'package:birou/views/company/additional/updateInput.dart';
// import 'package:birou/views/dialog/deleteInput.dart';
// import 'package:birou/views/menu/mainMenu.dart';
// import 'package:bottom_navy_bar/bottom_navy_bar.dart';
// import 'package:bubbled_navigation_bar/bubbled_navigation_bar.dart';
// import 'package:clay_containers/clay_containers.dart';
// import 'package:fl_chart/fl_chart.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:ionicons/ionicons.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//
// class Deliveries extends StatefulWidget {
//   @override
//   String companyId;
//   final List<Company> companies;
//
//   Deliveries({Key key, this.companies, this.companyId});
//
//   _DeliveriesState createState() => _DeliveriesState();
// }
//
// class _DeliveriesState extends State<Deliveries> {
//   @override
//   String full_name;
//
//   bool clientList = true;
//   bool providerList = false;
//   int itemCount = 4;
//   double listBuilderHeightClient = 350.0;
//   double listBuilderHeightProvider = 350.0;
//
//   final spinkit = SpinKitFadingCircle(
//     color: Colors.blueAccent,
//     size: 50.0,
//   );
//
//   getUserName() async {
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     return sharedPreferences.getString("full_name");
//   }
//
//   Future deliveries;
//   int condition;
//   String conditionText;
//   List<Condition> conditions = [
//     Condition("Update", 1),
//     Condition("Delete", 2),
//   ];
//
//   @override
//   void initState() {
//     // getUserName().then((result) {
//     //   setState(() {
//     //     full_name = result;
//     //   });
//     // });
//     deliveries = getDeliveries();
//
//     getEstimates(widget.companyId).then((result) {
//       print(result);
//     });
//     super.initState();
//   }
//
//   Widget build(BuildContext context) {
//     var width = MediaQuery.of(context).size.width;
//     var height = MediaQuery.of(context).size.height;
//
//     return Scaffold(
//       backgroundColor: Color.fromRGBO(245, 246, 252, 1),
//       body: SingleChildScrollView(
//         child: SafeArea(
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               Padding(
//                 padding: const EdgeInsets.all(12.0),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: <Widget>[
//                     GestureDetector(
//                       onTap: () {
//                         Navigator.of(context).push(MaterialPageRoute(
//                             builder: (BuildContext context) => MainMenu(
//                               companies: widget.companies,
//                             )));
//                       },
//                       child: Container(
//                         width: 18,
//                         height: 18,
//                         child: Image.asset(
//                           "assets/images/menu.png",
//                           fit: BoxFit.contain,
//                         ),
//                       ),
//                     ),
//                     GestureDetector(
//                       onTap: () {},
//                       child: CircleAvatar(
//                         backgroundColor: Colors.transparent,
//                         radius: 30,
//                         child: ClipOval(
//                           child: Image.asset(
//                             'assets/images/person.png',
//                           ),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(left: 18.0),
//                 child: Text(
//                   'Deliveries',
//                   style: TextStyle(
//                       color: Colors.black,
//                       fontSize: width / 15,
//                       fontWeight: FontWeight.bold),
//                   textAlign: TextAlign.start,
//                 ),
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               SizedBox(
//                 height: 5,
//               ),
//               Padding(
//                 padding:
//                 const EdgeInsets.only(top: 30.0, left: 30.0, right: 30.0),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Text(
//                       AppLocalizations.of(context).synthesis,
//                       style: TextStyle(fontSize: 15, color: Colors.grey[700]),
//                     ),
//                     InkWell(
//                       onTap: () {
//                         Navigator.of(context).push(MaterialPageRoute(
//                             builder: (BuildContext context) => AddInput()));
//                       },
//                       child: Icon(
//                         Icons.addPayment,
//                         size: width / 14,
//                         color: Colors.grey[700],
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               SizedBox(
//                 height: 5,
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(left: 8.0, right: 8.0),
//                 child: Center(
//                   child: Container(
//                     height: clientList
//                         ? listBuilderHeightClient
//                         : listBuilderHeightProvider,
//                     width: width * 0.9,
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(20),
//                       boxShadow: [
//                         BoxShadow(
//                           color: Colors.blue.withOpacity(0.1),
//                           spreadRadius: 5,
//                           blurRadius: 7,
//                           offset: Offset(0, 3), // changes position of shadow
//                         ),
//                       ],
//                     ),
//                     child: Card(
//                       elevation: 0,
//                       shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(20)),
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.end,
//                         children: [
//                           Expanded(
//                             child: Column(
//                               children: [
//                                 SizedBox(
//                                   height: 20,
//                                 ),
//                                 FutureBuilder(
//                                   future: deliveries,
//                                   builder: (BuildContext context,
//                                       AsyncSnapshot<dynamic> snapshot) {
//                                     if (snapshot.data == null) {
//                                       return Container(
//                                         height: width * 0.5,
//                                         alignment: Alignment.center,
//                                         child: Center(
//                                           child: spinkit,
//                                         ),
//                                       );
//                                     } else {
//                                       return Expanded(
//                                         flex: 1,
//                                         child: ListView.builder(
//                                           physics:
//                                           const NeverScrollableScrollPhysics(),
//                                           itemCount: snapshot.data.length > 3
//                                               ? itemCount
//                                               : snapshot.data.length,
//                                           itemBuilder: (BuildContext context,
//                                               int index) {
//                                             return Deadline(
//                                                 (snapshot.data[index].date).substring(0, 10),
//                                                 snapshot
//                                                     .data[index].client
//                                                     .toString(),
//                                                 (index + 1).toString(),
//                                                 snapshot.data[index].status,
//                                                 snapshot.data[index].total,
//                                                 snapshot.data[index]
//                                                     .id,
//                                             );
//                                           },
//                                         ),
//                                       );
//                                     }
//                                   },
//                                 )
//                               ],
//                             ),
//                           ),
//                           SizedBox(
//                             height: 10,
//                           ),
//                           GestureDetector(
//                             onTap: () {
//                               setState(() {
//                                 itemCount = itemCount + 2;
//                                 if (clientList) {
//                                   listBuilderHeightClient =
//                                       listBuilderHeightClient + 150;
//                                 } else {
//                                   listBuilderHeightProvider =
//                                       listBuilderHeightProvider + 150;
//                                 }
//                               });
//                             },
//                             child: Padding(
//                               padding: const EdgeInsets.fromLTRB(8, 8, 20, 8),
//                               child: Text(
//                                 AppLocalizations.of(context).load_more,
//                                 style: TextStyle(
//                                     fontSize: 14,
//                                     fontWeight: FontWeight.normal,
//                                     color: Colors.blueAccent),
//                               ),
//                             ),
//                           )
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   Widget Deadline(String date, String client, index, status, String total, String id) {
//     return Container(
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             SizedBox(width: 3,),
//             Text(
//               date,
//               style: TextStyle(
//                   color: Colors.blueAccent,
//                   fontSize: 18,
//                   fontWeight: FontWeight.normal),
//             ),
//             Text("#" + index),
//             Container(
//               width: 40,
//               child: Text(
//                 total.toString(),
//                 style: TextStyle(fontSize: 17, fontWeight: FontWeight.normal),
//               ),
//             ),
//             Container(
//               width: 40,
//               child: Text(
//                 client.toString(),
//                 style: TextStyle(fontSize: 17, fontWeight: FontWeight.normal),
//               ),
//             ),
//             Text(
//               'Payed',
//               style: TextStyle(fontSize: 17, fontWeight: FontWeight.normal),
//             ),
//             PopupMenuButton<int>(
//               icon: Icon(
//                 Icons.arrow_drop_down,
//                 color: Colors.blueAccent,
//                 size: 35,
//               ),
//               onSelected: (int result) {
//                 if (result == 1) {
//                   Navigator.of(context).push(MaterialPageRoute(
//                       builder: (BuildContext context) => Login(
//
//                       )));
//                 } else if (result == 2) {
//                   showDialog(
//                       context: context,
//                       builder: (BuildContext context) {
//                         return DeleteInput(id: id);
//                       });
//                 }
//               },
//               itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
//                 PopupMenuItem<int>(
//                   value: conditions[0].condition,
//                   child: Text(conditions[0].title),
//                 ),
//                 PopupMenuItem<int>(
//                   value: conditions[1].condition,
//                   child: Text(conditions[1].title),
//                 ),
//               ],
//             )
//           ],
//         ),
//     );
//   }
// }

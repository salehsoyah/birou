import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/contactsController.dart';
import 'package:birou/controllers/deliveryNotesController.dart';
import 'package:birou/controllers/stockController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/generated/l10n.dart';
import 'package:birou/models/bank.dart';
import 'package:birou/models/client.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/device.dart';
import 'package:birou/models/input.dart';
import 'package:birou/models/item.dart';
import 'package:birou/models/language.dart';
import 'package:birou/models/tax.dart';
import 'package:birou/models/inputAdd.dart';
import 'package:birou/models/taxAdd.dart';

import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/dashboard/home.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:birou/views/sales/delivery/addDelivery/form.dart';
import 'package:birou/views/sales/delivery/editDelivery/deliverySettingUpdate.dart';
import 'package:birou/views/sales/delivery/editDelivery/formEdit.dart';
import 'package:birou/views/sales/invoice/editInvoice/invoiceSettingUpdate.dart';
import 'package:birou/views/sales/sales.dart';
import 'package:birou/views/subscriptions/last_step.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:date_field/date_field.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:form_validator/form_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EditDelivery extends StatefulWidget {
  @override
  String id;
  String companyId;
  final List<Company> companies;

  EditDelivery(
      {Key key,
      @required this.id,
      @required this.companyId,
      @required this.companies});

  _EditDeliveryState createState() => _EditDeliveryState();
}

class _EditDeliveryState extends State<EditDelivery> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();

  bool obscureText = true;
  String mobile_token;
  List<Country> countries = [];
  String country;
  String countryText;
  bool btnPressed = false;
  final _firebaseMessaging = FirebaseMessaging.instance;
  Future listCountries;
  Future companyTaxes;
  Future banksList;
  Future clientList;
  String discount = '0';

  List<Tax> taxes = [
    Tax("Hors Taxes", 1, "", 0),
    Tax("Taxes incluses", 2, "", 0),
  ];

  String pdfLanguage;
  String pdfText;
  List<Language> languages = [
    Language("Français", "fr"),
    Language("English", "en"),
    Language("Arabic", "ar")
  ];

  int taxPicked = 1;
  String taxText;

  List<Clientt> clients = [];
  String clientText;
  String client;

  String conditionText;
  List<Condition> conditions = [
    Condition("Oui", 1),
    Condition("Non", 0),
  ];
  String dateT;
  DateTime date;
  bool condition = false;
  bool stamp = false;
  bool billing = false;
  bool delivery = false;
  bool bank = false;
  int currency = 1;

  final _deliveryNumberController = TextEditingController();
  final _referenceController = TextEditingController();
  final _generalConditionsController = TextEditingController();
  final _notesController = TextEditingController();
  final _discountController = TextEditingController();
  final _totalController = TextEditingController();
  final _subtotalController = TextEditingController();
  final _taxesController = TextEditingController();

  List<ItemFormEdit> items = [];
  List<Bank> banks = [];

  var articles;
  List<String> inputs = [];

  String bankText;
  String bankId;

  bool loading = false;

  void initState() {
    companyTaxes = companyTax();
    banksList = bankList(widget.companyId);
    clientList = clientsList(widget.companyId);

    getDelivery(widget.companyId, widget.id).then((result) {
      setState(() {
        client = result['data']['delivery']['hashed_contact_id'];
        clientsList(widget.companyId).then((value) {
          setState(() {
            clients = value;
            for (var v in clients) {
              if (v.id == client) {
                // print(client);
                setState(() {
                  clientText = v.display_name;
                });
              }
            }
          });
        });
      });

      setState(() {
        for (var v in (result['data']['delivery']['additional_inputs'])) {
          print(v['default_value']);
          inputs.add(jsonEncode(InputAdd(v['hashed_id'], v['default_value'])));
        }
      });

      setState(() {
        condition =
            jsonDecode(result['data']['delivery']['display'])['conditions'] == 1
                ? true
                : false;
        stamp = jsonDecode(result['data']['delivery']['display'])['stamp'] == 1
            ? true
            : false;
        billing =
            jsonDecode(result['data']['delivery']['display'])['billing'] == 1
                ? true
                : false;
        delivery =
            jsonDecode(result['data']['delivery']['display'])['delivery'] == 1
                ? true
                : false;
        bank = jsonDecode(result['data']['delivery']['display'])['bank'] != 0
            ? true
            : false;
        bankId = jsonDecode(result['data']['delivery']['display'])['bank'] != 0
            ? jsonDecode(result['data']['delivery']['display'])['bank']
            : bankId;
      });

      setState(() {
        currency = result['data']['delivery']['currency_rate'];
        pdfLanguage = result['data']['delivery']['language'];

        _discountController.text =
            jsonDecode(result['data']['delivery']['totals'])['discount']
                .toString();
        _subtotalController.text =
            jsonDecode(result['data']['delivery']['totals'])['subtotal']
                .toString();
        _taxesController.text =
            jsonDecode(result['data']['delivery']['totals'])['taxes']
                .toString();
        _totalController.text =
            jsonDecode(result['data']['delivery']['totals'])['total']
                .toString();

        date = DateTime.parse(result['data']['delivery']['date']);
        _deliveryNumberController.text =
            result['data']['delivery']['delivery_number'];
        _referenceController.text =
            result['data']['delivery']['reference_number'];

        // taxPicked = 1;
        for (var v in taxes) {
          if (v.rate == taxPicked) {
            // print(taxPicked);
            setState(() {
              taxText = v.title;
            });
          }
        }

        _generalConditionsController.text =
            result['data']['delivery']['conditions'];
        _notesController.text = result['data']['delivery']['notes'];
      });
      for (var u in result['data']['delivery']['items']) {
        var item = Item();
        item.item_hashed_id =
            u['temporary_item'] != 'null' ? u['hashed_item_id'] : '';
        item.item = u['temporary_item'] != null
            ? u['temporary_item']
            : u['item']['title'];
        item.qte = u['quantity'];
        item.price = double.parse(u['rate']);
        item.taxes = [];
        if (u['taxes'].isNotEmpty) {
          // print(u['taxes']);
          for (var v in u['taxes']) {
            var taxAdd = TaxAdd();
            taxAdd.tax_hashed_id = v['hashed_company_tax_id'].toString();
            item.taxes.add(taxAdd);
          }
        }

        items.add(ItemFormEdit(
          item: item,
          onDelete: () => onDelete(item),
          companyId: widget.companyId,
          printTax: printTax,
          notifyParent: refresh,
        ));
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).edit_delivery,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return DeliverySettingsUpdate(
                        companyId: widget.companyId,
                        companies: widget.companies,
                        language: pdfLanguage,
                        conditions: condition,
                        billing: billing,
                        currency: currency,
                        bank: bank,
                        bankId: bankId,
                        stamp: stamp,
                        delivery: delivery,
                        inputs: inputs,
                        id: widget.id,
                      );
                    }).then((returnValue) {
                  print(returnValue);
                  if (returnValue != false) {
                    setState(() {
                      pdfLanguage = returnValue['pdfLanguage'];
                      condition = returnValue['condition'];
                      billing = returnValue['billing'];
                      currency = returnValue['currency'];
                      bank = returnValue['bank'];
                      bankId = returnValue['bankId'];
                      stamp = returnValue['stamp'];
                      delivery = returnValue['delivery'];
                      inputs = returnValue['inputs'];
                    });
                  }
                });
              },
              child: Icon(
                Icons.format_list_bulleted_rounded,
                color: Colors.blue,
              ),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _form,
          child: Container(
            color: Color.fromRGBO(245, 246, 252, 1),
            child: Column(
              children: [
                SizedBox(
                  height: width / 11,
                ),
                SingleChildScrollView(
                  padding: EdgeInsets.only(top: 5, bottom: 5),
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      SizedBox(
                        width: width / 11,
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            btnPressed = true;
                          });
                          onSave();
                          print(articles);
                          if (_form.currentState.validate() &&
                              taxPicked != null &&
                              client != null &&
                              articles != null) {
                            editDelivery(
                                    date.toString().substring(0, 10),
                                    _deliveryNumberController.text.substring(3),
                                    _generalConditionsController.text,
                                    _referenceController.text,
                                    _notesController.text,
                                    _discountController.text == ''
                                        ? null
                                        : double.parse(
                                            _discountController.text),
                                    taxPicked,
                                    client,
                                    condition == true ? 1 : 0,
                                    stamp == true ? 1 : 0,
                                    billing == true ? 1 : 0,
                                    delivery == true ? 1 : 0,
                                    bank == true ? 1 : 0,
                                    bankId,
                                    1,
                                    0,
                                    currency,
                                    pdfLanguage,
                                    articles,
                                    inputs,
                                    widget.id,
                                    widget.companyId)
                                .then((value) {
                              if (value['status']['code'] != 200) {
                                Navigator.of(context)
                                    .pushReplacement(MaterialPageRoute(
                                        builder: (BuildContext context) => Home(
                                              companies: widget.companies,
                                              companyId: widget.companyId,
                                              currentScreen: Sales(
                                                companies: widget.companies,
                                                companyId: widget.companyId,
                                                invoice: false,
                                                estimate: false,
                                                delivery: true,
                                              ),
                                            )));
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return DialogScreen(
                                        image: 0,
                                        text1: 'Error',
                                        text2: value['status']['message'],
                                      );
                                    });
                              } else {
                                Navigator.of(context)
                                    .pushReplacement(MaterialPageRoute(
                                        builder: (BuildContext context) => Home(
                                              companies: widget.companies,
                                              companyId: widget.companyId,
                                              currentScreen: Sales(
                                                companies: widget.companies,
                                                companyId: widget.companyId,
                                                invoice: false,
                                                estimate: false,
                                                delivery: true,
                                              ),
                                            )));
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return DialogScreen(
                                        image: 1,
                                        text1: 'Success',
                                        text2: value['status']['message'],
                                      );
                                    });
                              }
                              print(value);
                            });
                          }
                        },
                        child: Container(
                          width: width / 6,
                          height: width / 6,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.blue[100],
                                  spreadRadius: 0.5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 9.0, bottom: 2.0, top: 2.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Icon(
                                      Icons.circle,
                                      color: Colors.grey[300],
                                      size: 7,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Image.asset(
                                  "assets/images/icon10.png",
                                  fit: BoxFit.contain,
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(AppLocalizations.of(context).draft)
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: width / 15,
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                          width: width / 6,
                          height: width / 6,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.blue[100],
                                  spreadRadius: 0.5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 9.0, bottom: 2.0, top: 2.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Icon(
                                      Icons.circle,
                                      color: Colors.grey[300],
                                      size: 7,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Image.asset(
                                  "assets/images/send.png",
                                  fit: BoxFit.contain,
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(AppLocalizations.of(context).send)
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: width / 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            btnPressed = true;
                          });
                          deliveryInvoice(widget.companyId, widget.companyId)
                              .then((result) {
                            print(result);
                            if (result['status']['code'] != 200) {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return DialogScreen(
                                      image: 0,
                                      text1: 'Error',
                                      text2: result['status']['message'],
                                    );
                                  });
                            } else {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      InvoiceSettingsUpdate(
                                        id: result['data']['invoiceId'],
                                        companyId: widget.companyId,
                                        companies: widget.companies,
                                      )));
                            }
                          });
                        },
                        child: Container(
                          width: width / 6,
                          height: width / 6,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.blue[100],
                                  spreadRadius: 0.5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 9.0, bottom: 2.0, top: 2.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Icon(
                                      Icons.circle,
                                      color: Colors.green,
                                      size: 7,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Image.asset(
                                  "assets/images/preview.png",
                                  fit: BoxFit.contain,
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(AppLocalizations.of(context).invoice)
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: width / 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            btnPressed = true;
                          });
                          downloadDelivery(widget.companyId, widget.id,
                              _deliveryNumberController.text);
                        },
                        child: Container(
                          width: width / 5.3,
                          height: width / 6,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.blue[100],
                                  spreadRadius: 0.5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 9.0, bottom: 2.0, top: 2.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Icon(
                                      Icons.circle,
                                      color: Colors.green,
                                      size: 7,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Image.asset(
                                  "assets/images/preview.png",
                                  fit: BoxFit.contain,
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(AppLocalizations.of(context).download)
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: width / 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          deleteDelivery(widget.companyId, widget.id)
                              .then((result) {
                            print(result);
                            if (result['status']['code'] != 200) {
                              Navigator.of(context)
                                  .pushReplacement(MaterialPageRoute(
                                      builder: (BuildContext context) => Home(
                                            companies: widget.companies,
                                            companyId: widget.companyId,
                                            currentScreen: Sales(
                                              companies: widget.companies,
                                              companyId: widget.companyId,
                                              invoice: false,
                                              estimate: false,
                                              delivery: true,
                                            ),
                                          )));
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return DialogScreen(
                                      image: 0,
                                      text1: 'Error',
                                      text2: result['message'],
                                    );
                                  });
                            } else {
                              Navigator.of(context)
                                  .pushReplacement(MaterialPageRoute(
                                      builder: (BuildContext context) => Home(
                                            companies: widget.companies,
                                            companyId: widget.companyId,
                                            currentScreen: Sales(
                                              companies: widget.companies,
                                              companyId: widget.companyId,
                                              invoice: false,
                                              estimate: false,
                                              delivery: true,
                                            ),
                                          )));
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return DialogScreen(
                                      image: 1,
                                      text1: 'Success',
                                      text2: result['message'],
                                    );
                                  });
                            }
                          });
                        },
                        child: Container(
                          width: width / 6,
                          height: width / 6,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.blue[100],
                                  spreadRadius: 0.5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 9.0, bottom: 2.0, top: 2.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Icon(
                                      Icons.circle,
                                      color: Colors.grey[300],
                                      size: 7,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 3,
                                ),
                                Image.asset(
                                  "assets/images/delete.png",
                                  fit: BoxFit.contain,
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(AppLocalizations.of(context).delete)
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: width / 11,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        DateTimeFormField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            hintStyle: TextStyle(color: Colors.black45),
                            errorStyle: TextStyle(color: Colors.redAccent),
                            suffixIcon: Icon(Icons.event_note),
                            labelText: date != null
                                ? date.toString().substring(0, 10)
                                : AppLocalizations.of(context).date,
                          ),
                          firstDate:
                              DateTime.now().add(const Duration(days: 10)),
                          initialDate:
                              DateTime.now().add(const Duration(days: 10)),
                          autovalidateMode: AutovalidateMode.always,
                          validator: (DateTime e) =>
                              (date == null) ? 'Please enter date' : null,
                          onDateSelected: (DateTime value) {
                            print(value);
                            setState(() {
                              date = value;
                            });
                          },
                        ),
                        SizedBox(
                          height: width / 50,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      AppLocalizations.of(context).delivery_nb,
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                    Container(
                      width: width * 0.38,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.blue[100],
                              spreadRadius: 0.5,
                              blurRadius: 7,
                              offset:
                                  Offset(0, 1), // changes position of shadow
                            ),
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          validator: (val) => val.length < 1
                              ? 'Delivery number is required'
                              : null,
                          controller: _deliveryNumberController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'DN-2021-',
                              hintStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 27,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).client,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  clientText != null
                                      ? clientText
                                      : "Foulen ben foulen",
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && client == null
                                    ? Text(
                                        AppLocalizations.of(context)
                                            .enter_client,
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 12),
                                      )
                                    : Container()
                              ],
                            ),
                            FutureBuilder(
                              future: clientList,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text(
                                      AppLocalizations.of(context).loading);
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                                value: value.id,
                                                child: Text(
                                                  value.display_name,
                                                ),
                                              ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      client = value;
                                    });

                                    for (var i in clients) {
                                      if (value == i.id) {
                                        setState(() {
                                          clientText = i.display_name;
                                          print(i.display_name);
                                        });
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          controller: _referenceController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText:
                                  AppLocalizations.of(context).reference_nb,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  AppLocalizations.of(context).item_prices_are,
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  taxText != null
                                      ? taxText
                                      : AppLocalizations.of(context).choose_tax,
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && taxPicked == null
                                    ? Text(
                                        AppLocalizations.of(context).choose_tax,
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 12),
                                      )
                                    : Container()
                              ],
                            ),
                            PopupMenuButton<int>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              onSelected: (int result) {
                                setState(() {
                                  print(result);
                                  taxPicked = result;
                                });

                                for (var i in taxes) {
                                  if (i.rate == taxPicked) {
                                    setState(() {
                                      taxText = i.title;
                                    });
                                  }
                                }
                                printTax();
                              },
                              itemBuilder: (BuildContext context) =>
                                  <PopupMenuEntry<int>>[
                                PopupMenuItem<int>(
                                  value: taxes[0].rate,
                                  child: Text(taxes[0].title),
                                ),
                                PopupMenuItem<int>(
                                  value: taxes[1].rate,
                                  child: Text(taxes[1].title),
                                ),
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: 0.0, left: width / 11, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        AppLocalizations.of(context).items,
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                      GestureDetector(
                        onTap: () {
                          onAddForm();
                        },
                        child: Icon(
                          Icons.add,
                          size: width / 14,
                          color: Colors.grey[700],
                        ),
                      ),
                    ],
                  ),
                ),
                btnPressed == true && items.length < 1
                    ? Padding(
                        padding: EdgeInsets.only(
                            top: 0.0, left: width / 11, right: 30.0),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              AppLocalizations.of(context).add_items,
                              style: TextStyle(color: Colors.red, fontSize: 12),
                            )),
                      )
                    : Container(),
                SizedBox(
                  height: width / 27,
                ),
                items.length > 0
                    ? ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        addAutomaticKeepAlives: true,
                        itemCount: items.length,
                        itemBuilder: (_, i) => Padding(
                          padding: const EdgeInsets.only(left: 15.0, right: 15),
                          child: items[i],
                        ),
                      )
                    : Container(),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        Column(
                          children: [
                            _subtotalController.text.isNotEmpty
                                ? Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      AppLocalizations.of(context).subtotal,
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  )
                                : Container(),
                            TextFormField(
                              controller: _subtotalController,
                              enabled: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText:
                                      AppLocalizations.of(context).subtotal,
                                  hintStyle: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 17,
                                      fontWeight: FontWeight.normal)),
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                          controller: _discountController,
                          onChanged: (dscnt) {
                            setState(() {
                              discount = dscnt;
                            });
                            printTax();
                          },
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).discount,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        Column(
                          children: [
                            _taxesController.text.isNotEmpty
                                ? Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      AppLocalizations.of(context).taxes,
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  )
                                : Container(),
                            TextFormField(
                              controller: _taxesController,
                              enabled: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: AppLocalizations.of(context).taxes,
                                  hintStyle: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 17,
                                      fontWeight: FontWeight.normal)),
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        Column(
                          children: [
                            _totalController.text.isNotEmpty
                                ? Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      AppLocalizations.of(context).total_ttc,
                                      style: TextStyle(fontSize: 10),
                                    ),
                                  )
                                : Container(),
                            TextFormField(
                              controller: _totalController,
                              enabled: false,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText:
                                      AppLocalizations.of(context).total_ttc,
                                  hintStyle: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 17,
                                      fontWeight: FontWeight.normal)),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        TextFormField(
                          controller: _notesController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context).notes,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        TextFormField(
                          maxLines: 6,
                          controller: _generalConditionsController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppLocalizations.of(context)
                                  .general_conditions,
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onDelete(Item item) {
    setState(() {
      var find = items.firstWhere(
        (it) => it.item == item,
        orElse: () => null,
      );
      if (find != null) items.removeAt(items.indexOf(find));
    });
    printTax();
  }

  void onAddForm() {
    setState(() {
      var item = Item();
      items.add(ItemFormEdit(
        item: item,
        onDelete: () => onDelete(item),
        companyId: widget.companyId,
        notifyParent: refresh,
        printTax: printTax,
      ));
    });
  }

  void refresh() {
    setState(() {});
  }

  void onSave() {
    if (items.length > 0) {
      var allValid = true;
      items.forEach((form) => {allValid = allValid && form.isValid()});
      if (allValid) {
        var data = items.map((it) => jsonEncode(it.item)).toList();
        setState(() {
          articles = data;
        });
      }
    }
  }

  double sumTotal(List<String> prices, String discount) {
    double sum = 0;
    double sumWD = -1;
    prices.forEach(
        (price) => {price != null ? sum = sum + double.parse(price) : sum});

    if (discount != '0' && discount != '') {
      sumWD = sum - double.parse(discount);
    }

    if (sumWD > 0) {
      return sumWD;
    } else {
      return sum;
    }
  }

  double sumSubTotal(List<String> prices) {
    double sum = 0;
    prices.forEach(
        (price) => {price != null ? sum = sum + double.parse(price) : sum});

    return sum;
  }

  double sumTaxesTotal(List<String> prices) {
    double sum = 0;
    setState(() {
      prices.forEach((price) => {
            price != null ? sum = sum + double.parse(price) : sum,
          });
    });

    return sum;
  }

  void printTax() {
    double taxes = 0;
    double subtotal = 0;
    double total = 0;

    items.forEach((item) {
      var qty = item.qty != null ? item.qty : 0;
      var pu = item.up != null ? item.up : 0;

      var newPu = double.parse(pu.toString()) * qty;

      if (taxPicked == 1) {
        item.state.taxForms.forEach((taxForm) {
          if (taxForm.special == 1) {
            var tax = taxForm.rate;
            newPu = newPu + double.parse((newPu * (tax / 100)).toString());
          }
        });
        item.state.taxForms.forEach((taxForm) {
          var tax = taxForm.rate;
          if (taxForm.special == 1) {
            taxes = taxes + ((pu * (tax / 100)) * (qty));
          } else {
            taxes = taxes + (newPu * (tax / 100));
          }
        });
      } else {
        item.state.taxForms.forEach((taxForm) {
          print("lena ddddddd " + taxForm.rate.toString());

          if (taxForm.special == 0) {
            var tax = taxForm.rate;

            taxes = taxes + (((pu * (tax / 100)) * qty) / ((tax / 100) + 1));

            newPu = newPu - taxes;
          }
        });
        item.state.taxForms.forEach((taxForm) {
          var tax = taxForm.rate;
          if (taxForm.special == 1) {
            taxes = taxes + ((newPu * (tax / 100)) / ((tax / 100) + 1));
          }
        });
      }

      setState(() {
        total = total + newPu;
      });
    });

    double discount = _discountController.text != ''
        ? double.parse(_discountController.text)
        : 0;

    if (discount < 0) {
      setState(() {
        discount = 0;
      });
    }
    setState(() {
      _subtotalController.text = (total.toStringAsFixed(2)).toString();
      _taxesController.text = taxes.toStringAsFixed(2).toString();
    });

    if (taxPicked == 1) {
      if (((total + taxes) - discount) > 0) {
        setState(() {
          _totalController.text =
              ((total + taxes) - discount).toStringAsFixed(2).toString();
        });
      } else {
        setState(() {
          _totalController.text =
              ((total + taxes)).toStringAsFixed(2).toString();
        });
      }
    } else {
      if (total - discount > 0) {
        setState(() {
          _totalController.text =
              (total - discount).toStringAsFixed(2).toString();
        });
      } else {
        setState(() {
          _totalController.text = total.toStringAsFixed(2).toString();
        });
      }
    }
    print('total  = ' + taxes.toString());
  }
}

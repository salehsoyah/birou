import 'dart:convert';

import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/stockController.dart';
import 'package:birou/generated/l10n.dart';
import 'package:birou/models/article.dart';
import 'package:birou/models/client.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/item.dart';
import 'package:birou/models/tax.dart';
import 'package:birou/models/taxAdd.dart';
import 'package:birou/views/sales/delivery/addDelivery/taxForm.dart';
import 'package:birou/views/sales/delivery/editDelivery/taxFormEdit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

typedef OnDelete();
typedef PrintTax();

class ItemFormEdit extends StatefulWidget {
  final Item item;
  final state = _ItemFormEditState();
  final OnDelete onDelete;
  String companyId;
  String total;
  double taxes;
  String taxPrice;
  final Function() notifyParent;
  final PrintTax printTax;
  int qty;
  double up;

  ItemFormEdit(
      {Key key,
      this.item,
      this.onDelete,
      @required this.companyId,
      this.total,
      this.taxes,
      this.taxPrice,
      this.notifyParent,
      this.printTax,
      this.qty,
      this.up})
      : super(key: key);

  @override
  _ItemFormEditState createState() => state;

  bool isValid() => state.validate();
}

class _ItemFormEditState extends State<ItemFormEdit> {
  final form = GlobalKey<FormState>();
  final _companyController = TextEditingController();
  final _addressController = TextEditingController();
  final _stateController = TextEditingController();
  final _zipCodeController = TextEditingController();
  final titleController = TextEditingController();

  bool obscureText = true;
  String mobile_token;
  String country;
  String countryText;
  bool btnPressed = false;
  Future listCountries;
  Future companyTaxes;

  String taxPicked;
  String taxText;
  List<Tax> taxes = [];

  final _priceController = TextEditingController();
  final _upController = TextEditingController();
  final _quantityController = TextEditingController();
  List<Article> foodList = [];
  List<Article> foodListSearch;
  TextEditingController _textController = TextEditingController();
  final FocusNode _textFocusNode = FocusNode();
  List<TaxFormEdit> taxForms = [];

  void _onFocusChange() {
    debugPrint("Focus: ${_textFocusNode.hasFocus.toString()}");
  }

  void initState() {
    if (widget.item.taxes != null && widget.item.taxes.length > 0) {
      for (var u in widget.item.taxes) {
        var taxAdd = TaxAdd();
        taxAdd.tax_hashed_id = u.tax_hashed_id;
        taxForms.add(TaxFormEdit(
          taxAdd: taxAdd,
          onDelete: () => onDeleteTaxForm(taxAdd),
          companyId: widget.companyId,
          notifyParent: widget.notifyParent,
          printTax: widget.printTax,
          rate: 0,
          special: 0,
        ));
      }
    }
    setState(() {
      titleController.text = widget.item.item;
      _quantityController.text = widget.item.qte.toString();
      widget.qty = int.parse(widget.item.qte.toString());
      _upController.text = widget.item.price.toString();
      widget.up = double.parse(widget.item.price.toString());
      _priceController.text = (widget.item.qte * widget.item.price).toString();
    });
    widget.item.taxes = [];

    getStock(widget.companyId).then((value) {
      setState(() {
        foodList = value;
      });
    });
    _textFocusNode.addListener(_onFocusChange);
    companyTaxes = companyTax();
    companyTaxes.then((value) {

      setState(() {
        taxes = value;
        TaxAdd taxAdd = TaxAdd(tax_hashed_id: taxes[0].hashed_id);
        widget.item.taxes.add(taxAdd);
        print(widget.item.taxes);
        taxPicked = value[0].hashed_id;
        taxText = value[0].rate.toString() + '%' + ' ' + value[0].title;
      });
    });
    super.initState();
  }

  void dispose() {
    _textFocusNode.removeListener(_onFocusChange);
    _textFocusNode.dispose();
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.blue[100],
                spreadRadius: 0.5,
                blurRadius: 7,
                offset: Offset(0, 1), // changes position of shadow
              ),
            ]),
        child: Form(
          key: form,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.width / 39,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: TextFormField(
                        onChanged: (value) {
                          setState(() {
                            foodListSearch = foodList
                                .where((element) => element.title
                                .toLowerCase()
                                .contains(value.toLowerCase()))
                                .toList();
                          });
                        },
                        focusNode: _textFocusNode,
                        controller: titleController,
                        onSaved: (val) => widget.item.item = val,
                        validator: (val) =>
                        val.length < 1 ? AppLocalizations.of(context).enter_item : null,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).item,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                    ),
                    titleController.text.isNotEmpty &&
                        _textFocusNode.hasFocus == true &&
                        foodListSearch.isNotEmpty
                        ? Container(
                      height: 200,
                      child: ListView.builder(
                          itemCount: foodListSearch.length,
                          itemBuilder: (ctx, index) {
                            return InkWell(
                              onTap: () {
                                setState(() {
                                  titleController.text =
                                      foodListSearch[index].title;
                                  widget.item.item_hashed_id =
                                      foodListSearch[index].id;
                                });
                              },
                              child: Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: CircleAvatar(
                                      child: Icon(Icons.food_bank),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    foodListSearch[index].title,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  )
                                ],
                              ),
                            );
                          }),
                    )
                        : Container(),
                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: TextFormField(
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                        keyboardType: TextInputType.number,
                        // initialValue: widget.item.quantity.toString(),
                        onSaved: (val) => widget.item.qte = int.parse(val),
                        onChanged: (qte) {
                          if (qte.isNotEmpty && !widget.up.isNaN) {
                            setState(() {
                              widget.qty = int.parse(qte);
                              _priceController.text =
                                  (double.parse(widget.up.toString()) *
                                      int.parse(qte))
                                      .toString();
                              widget.total = (double.parse(widget.up.toString()) *
                                  int.parse(qte))
                                  .toString();
                              widget.notifyParent();
                              widget.printTax();
                            });
                          } else {
                            setState(() {
                              widget.qty = 0;
                              _priceController.text = '0';
                              widget.total = '0';
                              widget.notifyParent();
                              widget.printTax();
                            });
                          }
                        },
                        validator: (val) =>
                        val.isEmpty ? AppLocalizations.of(context).enter_quantity : null,
                        controller: _quantityController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).quantity,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                    ),
                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly
                        ],

                        validator: (val) =>
                        val.isEmpty ? AppLocalizations.of(context).enter_up : null,
                        controller: _upController,
                        // initialValue: widget.item.up.toString(),
                        onSaved: (val) => widget.item.price = double.parse(val),
                        onChanged: (up) {
                          if (!widget.qty.isNaN  && up.isNotEmpty) {
                            setState(() {
                              widget.up = double.parse(up);

                              _priceController.text = (double.parse(up) *
                                  int.parse(widget.qty.toString()))
                                  .toString();
                              widget.total = (double.parse(up) *
                                  int.parse(widget.qty.toString()))
                                  .toString();
                              widget.notifyParent();
                              widget.printTax();
                            });
                          } else {
                            setState(() {
                              widget.up = 0;
                              _priceController.text = '0';
                              widget.total = '0';
                              widget.notifyParent();
                              widget.printTax();
                            });
                          }
                        },
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).unit_price,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                    ),
                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    taxForms.length > 0
                        ? ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      addAutomaticKeepAlives: true,
                      itemCount: taxForms.length,
                      itemBuilder: (_, i) => taxForms[i],
                    )
                        : Container(),
                    Align(
                      alignment: Alignment.centerRight,
                      child: TextButton.icon(
                        style: TextButton.styleFrom(
                          textStyle: TextStyle(color: Colors.blue),
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                        onPressed: () => {
                          onAddTaxForm(),
                        },
                        icon: Icon(
                          Icons.add,
                        ),
                        label: Text(
                          AppLocalizations.of(context).add_tax,
                        ),
                      ),
                    ),
                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: TextFormField(
                        controller: _priceController,
                        // initialValue: widget.item.price.toString(),
                        enabled: false,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppLocalizations.of(context).total,
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.width / 15,
                    )
                  ],
                ),
              ),
              FlatButton(
                color: Colors.transparent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(AppLocalizations.of(context).delete),
                    Icon(Icons.delete, size: 20,),
                  ],
                ),
                onPressed: (){
                  widget.onDelete();
                  widget.printTax();
                  widget.notifyParent();
                },
              ),

            ],
          ),
        ),
      ),
    );
  }

  ///form validator
  bool validate() {
    var data = taxForms.map((it) => (it.taxAdd)).toList();
    setState(() {
      widget.item.taxes = [];
      data.forEach((dat) => {
            if (dat.tax_hashed_id != null) {widget.item.taxes.add(dat)}
          });
    });
    var valid = form.currentState.validate();
    if (valid) form.currentState.save();
    return valid;
  }

  void onAddTaxForm() {
    setState(() {
      var taxAdd = TaxAdd();
      taxForms.add(TaxFormEdit(
        rate: 0,
        taxAdd: taxAdd,
        onDelete: () => onDeleteTaxForm(taxAdd),
        companyId: widget.companyId,
        printTax: () => widget.printTax(),
        notifyParent: widget.notifyParent,
      ));
    });
  }

  void onDeleteTaxForm(TaxAdd taxAdd) {
    setState(() {
      var find = taxForms.firstWhere(
        (it) => it.taxAdd == taxAdd,
        orElse: () => null,
      );
      if (find != null) taxForms.removeAt(taxForms.indexOf(find));
    });
  }
}

import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/stockController.dart';
import 'package:birou/models/article.dart';
import 'package:birou/models/client.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/item.dart';
import 'package:birou/models/tax.dart';
import 'package:birou/models/taxAdd.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

typedef OnDelete();
typedef PrintTax();

class TaxFormEdit extends StatefulWidget {
  final Function() notifyParent;
  final TaxAdd taxAdd;
  final state = _TaxFormEditState();
  final OnDelete onDelete;
  String companyId;
  int rate;
  int special;
  final PrintTax printTax;

  TaxFormEdit(
      {Key key,
      this.taxAdd,
      this.onDelete,
      @required this.companyId,
      this.rate = 0,
      this.printTax,
      this.notifyParent,
      this.special})
      : super(key: key);

  @override
  _TaxFormEditState createState() => state;

  bool isValid() => state.validate();
}

class _TaxFormEditState extends State<TaxFormEdit> {
  final form = GlobalKey<FormState>();
  final _companyController = TextEditingController();
  final _addressController = TextEditingController();
  final _stateController = TextEditingController();
  final _zipCodeController = TextEditingController();
  final titleController = TextEditingController();

  bool obscureText = true;
  String mobile_token;
  String country;
  String countryText;
  bool btnPressed = false;
  Future listCountries;
  Future companyTaxes;

  String taxPicked;
  String taxText;
  List<Tax> taxes = [];

  TextEditingController _textController = TextEditingController();
  final FocusNode _textFocusNode = FocusNode();

  void _onFocusChange() {
    debugPrint("Focus: ${_textFocusNode.hasFocus.toString()}");
  }

  void initState() {
    _textFocusNode.addListener(_onFocusChange);
    companyTaxes = companyTax();
    companyTaxes.then((value) {
      setState(() {
        taxes = value;
        for (var i in taxes) {
          if (widget.taxAdd.tax_hashed_id == i.hashed_id) {
            setState(() {
              taxText = i.rate.toString() + '%' + ' ' + i.title;
              widget.rate = i.rate;
              widget.special = i.special;
            });
          }
        }
      });
    });
    super.initState();

  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.width / 39,
              ),
              Text(
                AppLocalizations.of(context).tax,
                style: TextStyle(fontSize: 17, color: Colors.grey[500]),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.width / 70,
              ),
              Text(
                taxText != null ? taxText : AppLocalizations.of(context).choose_tax,
                style: TextStyle(fontSize: 13),
              ),
              btnPressed && taxPicked == null
                  ? Text(
                      "Please choose tax",
                      style: TextStyle(color: Colors.red, fontSize: 12),
                    )
                  : Container()
            ],
          ),
          Row(
            children: [
              FutureBuilder(
                future: companyTaxes,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (!snapshot.hasData) {
                    return Text(AppLocalizations.of(context).loading);
                  }
                  return PopupMenuButton<String>(
                    icon: Icon(
                      Icons.arrow_drop_down,
                      color: Colors.blueAccent,
                      size: 35,
                    ),
                    itemBuilder: (context) => snapshot.data
                        .map<PopupMenuItem<String>>(
                            (value) => PopupMenuItem<String>(
                                  value: value.hashed_id,
                                  child: Text(
                                    value.rate.toString() + '%',
                                  ),
                                ))
                        .toList(),
                    onSelected: (value) {
                      setState(() {
                        taxPicked = value;
                        // widget.item.tax_hashed_id = value;
                        widget.taxAdd.tax_hashed_id = value;
                      });

                      for (var i in taxes) {
                        if (value == i.hashed_id) {
                          setState(() {
                            taxText = i.rate.toString() + '%' + ' ' + i.title;
                            widget.rate = i.rate;
                            widget.special = i.special;
                          });
                        }
                      }
                      widget.notifyParent();
                      widget.printTax();
                    },
                  );
                },
              ),
              IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  widget.onDelete();
                  widget.notifyParent();
                  widget.printTax();
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  ///form validator
  bool validate() {
    if (widget.taxAdd.tax_hashed_id != '' &&
        widget.taxAdd.tax_hashed_id != null)
      widget.taxAdd.tax_hashed_id = taxPicked;
    return true;
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:html_editor_enhanced/html_editor.dart';


class SendMail extends StatefulWidget {
  @override

  SendMail(
      {Key key,
       });

  _SendMailState createState() => _SendMailState();
}

class _SendMailState extends State<SendMail> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();

  HtmlEditorController controller = HtmlEditorController();


  void initState() {


    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),

        title: Row(
          children: [
            Spacer(
              flex: 2,
            ),
            Text(
              'Send Delivery by Mail',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
            GestureDetector(
              onTap: (){
                Navigator.pop(context);
              },
              child: Icon(
                Icons.close,
                color: Colors.blue,
              ),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _form,
          child: Container(
            color: Color.fromRGBO(245, 246, 252, 1),
            child: Column(
              children: [
                SizedBox(
                  height: width / 11,
                ),

                Padding(
                  padding:
                  EdgeInsets.only(top: 0.0, left: width / 11, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'From',
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: width / 27,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [

                        TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'From',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),

                      ],
                    ),
                  ),
                ),

                Padding(
                  padding:
                  EdgeInsets.only(top: 20.0, left: width / 11, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'To',
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: width / 27,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [

                        TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'To',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),

                      ],
                    ),
                  ),
                ),

                Padding(
                  padding:
                  EdgeInsets.only(top: 20.0, left: width / 11, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Cc',
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: width / 27,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [

                        TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Cc',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.only(top: 20.0, left: width / 11, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Subject',
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: width / 27,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [

                        TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Subject',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),

                      ],
                    ),
                  ),
                ),

                Padding(
                  padding:
                  EdgeInsets.only(top: 20.0, left: width / 11, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Message',
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: width / 27,
                ),

                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [

                      HtmlEditor(
                        controller: controller, //required
                        htmlEditorOptions: HtmlEditorOptions(
                          spellCheck: true,
                          hint: "Message",
                        ),
                        htmlToolbarOptions: HtmlToolbarOptions(
                          defaultToolbarButtons: [
                            StyleButtons(),
                            FontSettingButtons(),
                            FontButtons(),
                            ColorButtons(),
                            ListButtons(),
                            ParagraphButtons(),
                            InsertButtons(),
                            OtherButtons(),
                          ],
                        ),
                        otherOptions: OtherOptions(
                          height: 400,
                        ),
                      )

                    ],
                  ),
                ),


                SizedBox(
                  height: MediaQuery.of(context).size.height / 15,
                ),
                // ignore: deprecated_member_use
                RaisedButton(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    onPressed: () {

                    },
                    // padding: const EdgeInsets.all(0.0),
                    child: Container(
                      color: Color.fromRGBO(55, 86, 223, 1),
                      width: width * 0.78,
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                      child: Row(
                        children: [
                          Spacer(),
                          Container(
                              margin: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                'Send'.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white),
                              )
                          ),
                          Spacer(),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Image(
                                image:
                                AssetImage("assets/images/arrowGray.png"),
                              )
                          ),
                        ],
                      ),
                    ),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0))),
                SizedBox(
                  height: 10,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }


}

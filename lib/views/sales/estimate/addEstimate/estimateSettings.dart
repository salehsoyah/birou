import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:ui';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/generated/l10n.dart';
import 'package:birou/models/bank.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/device.dart';
import 'package:birou/models/input.dart';
import 'package:birou/models/inputAdd.dart';
import 'package:birou/models/language.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/sales/delivery/addDelivery/add.dart';
import 'package:birou/views/sales/estimate/addEstimate/add.dart';
import 'package:birou/views/subscriptions/last_step.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EstimateSettings extends StatefulWidget {
  @override
  _EstimateSettingsState createState() => _EstimateSettingsState();

  bool conditions;
  bool stamp;
  bool billing;
  bool delivery;
  bool bank;
  String bankId;
  List<String> inputs;
  String language;
  int currency;
  String companyId;
  final List<Company> companies;

  EstimateSettings(
      {Key key,
      @required this.conditions,
      @required this.stamp,
      @required this.billing,
      @required this.delivery,
      @required this.bank,
      @required this.bankId,
      @required this.inputs,
      @required this.language,
      @required this.currency,
      @required this.companyId,
      @required this.companies});
}

class _EstimateSettingsState extends State<EstimateSettings> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();

  final _currencyController = TextEditingController();

  bool obscureText = true;
  String mobile_token;
  List<Country> countries = [];
  String country;
  String countryText;
  bool btnPressed = false;
  final _firebaseMessaging = FirebaseMessaging.instance;
  Future listCountries;
  bool condition = true;
  bool stamp = false;
  bool billing = true;
  bool delivery = true;
  bool bank = true;
  List<Bank> banks = [];
  String bankText;
  String bankId;
  Future banksList;
  List<String> inputsAdd = [];
  List<Input> inputs = [];
  String pdfLanguage;
  String pdfText;
  List<Language> languages = [
    Language("Français", "fr"),
    Language("English", "en"),
    Language("Arabic", "ar")
  ];
  Map data = {};

  void initState() {
    setState(() {
      _currencyController.text = '1';
    });

    setState(() {
      _currencyController.text = widget.currency.toString();
      billing = widget.billing;
      delivery = widget.delivery;
      condition = widget.conditions;
      inputsAdd = widget.inputs;
      bank = widget.bank;
    });

    setState(() {
      pdfLanguage = widget.language;
      for (var l in languages) {
        if (l.hashed_id == pdfLanguage) {
          setState(() {
            pdfText = l.title;
          });
        }
      }
    });

    banksList = bankList(widget.companyId);

    bankList(widget.companyId).then((value) {
      setState(() {
        banks = value;
        bankId = value[0].id;
        bankText = value[0].title;
      });
    });
    companyInputs().then((value) {
      setState(() {
        inputs = value;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: Icon(
          Icons.arrow_back,
          color: Colors.transparent,
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).estimate_settings,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context, false);
              },
              child: Icon(
                Icons.close_sharp,
                color: Colors.blue,
              ),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _form,
          child: Container(
              color: Color.fromRGBO(245, 246, 252, 1),
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: width / 39,
                    ),
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 5),
                          child: Text(
                            AppLocalizations.of(context).include_estimate,
                            style: TextStyle(fontSize: 16),
                          ),
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              AppLocalizations.of(context).bank_details,
                              style: TextStyle(
                                  fontSize: 15, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                          ],
                        ),
                        Switch(
                          value: bank,
                          onChanged: (value) {
                            setState(() {
                              bank = value;
                              print(bank);
                            });
                          },
                          activeTrackColor: Colors.lightBlueAccent,
                          activeColor: Colors.blue,
                        ),
                      ],
                    ),
                    bank == true
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    bankText != null
                                        ? bankText
                                        : AppLocalizations.of(context).bank,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  btnPressed && bankId == null
                                      ? Text(
                                          "Please choose a bank",
                                          style: TextStyle(
                                              color: Colors.red, fontSize: 12),
                                        )
                                      : Container()
                                ],
                              ),
                              FutureBuilder(
                                future: banksList,
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  if (!snapshot.hasData) {
                                    return Text(
                                        AppLocalizations.of(context).loading);
                                  }
                                  return PopupMenuButton<String>(
                                    icon: Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.blueAccent,
                                      size: 35,
                                    ),
                                    itemBuilder: (context) => snapshot.data
                                        .map<PopupMenuItem<String>>(
                                            (value) => PopupMenuItem<String>(
                                                  value: value.id,
                                                  child: Text(
                                                    value.title,
                                                  ),
                                                ))
                                        .toList(),
                                    onSelected: (value) {
                                      setState(() {
                                        bankId = value;
                                      });

                                      for (var i in banks) {
                                        if (value == i.id) {
                                          setState(() {
                                            bankText = i.title;
                                          });
                                        }
                                      }
                                    },
                                  );
                                },
                              ),
                            ],
                          )
                        : Container(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              AppLocalizations.of(context).billing_address,
                              style: TextStyle(
                                  fontSize: 15, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                          ],
                        ),
                        Switch(
                          value: billing,
                          onChanged: (value) {
                            setState(() {
                              billing = value;
                              print(billing);
                            });
                          },
                          activeTrackColor: Colors.lightBlueAccent,
                          activeColor: Colors.blue,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              AppLocalizations.of(context).delivery_address,
                              style: TextStyle(
                                  fontSize: 15, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                          ],
                        ),
                        Switch(
                          value: delivery,
                          onChanged: (value) {
                            setState(() {
                              delivery = value;
                              print(delivery);
                            });
                          },
                          activeTrackColor: Colors.lightBlueAccent,
                          activeColor: Colors.blue,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              AppLocalizations.of(context).general_conditions,
                              style: TextStyle(
                                  fontSize: 15, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                          ],
                        ),
                        Switch(
                          value: condition,
                          onChanged: (value) {
                            setState(() {
                              condition = value;
                              print(condition);
                            });
                          },
                          activeTrackColor: Colors.lightBlueAccent,
                          activeColor: Colors.blue,
                        ),
                      ],
                    ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   children: [
                    //     Column(
                    //       crossAxisAlignment: CrossAxisAlignment.start,
                    //       children: [
                    //         SizedBox(
                    //           height: width / 39,
                    //         ),
                    //         Text(
                    //           'Cache de votre société',
                    //           style: TextStyle(
                    //               fontSize: 15, color: Colors.grey[500]),
                    //         ),
                    //         SizedBox(
                    //           height: width / 70,
                    //         ),
                    //       ],
                    //     ),
                    //     Switch(
                    //       value: stamp,
                    //       onChanged: (value) {
                    //         setState(() {
                    //           stamp = value;
                    //           print(stamp);
                    //         });
                    //       },
                    //       activeTrackColor: Colors.lightBlueAccent,
                    //       activeColor: Colors.blue,
                    //     ),
                    //   ],
                    // ),

                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 5),
                          child: Text(
                            AppLocalizations.of(context).additional_inputs,
                            style: TextStyle(fontSize: 16),
                          ),
                        )),
                    Container(
                      height: 100,
                      child: ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: inputs.length,
                          itemBuilder: (ctx, index) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: width / 39,
                                    ),
                                    Text(
                                      inputs[index].title,
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.grey[500]),
                                    ),
                                    SizedBox(
                                      height: width / 70,
                                    ),
                                  ],
                                ),
                                Switch(
                                  value: inputsAdd.contains(jsonEncode(InputAdd(
                                          inputs[index].hashed_id,
                                          inputs[index].default_value)))
                                      ? true
                                      : false,
                                  onChanged: (value) {
                                    setState(() {
                                      if (inputsAdd.contains(jsonEncode(
                                              InputAdd(
                                                  inputs[index].hashed_id,
                                                  inputs[index]
                                                      .default_value))) ==
                                          false) {
                                        InputAdd inputAdd = InputAdd(
                                            inputs[index].hashed_id,
                                            inputs[index].default_value);
                                        inputsAdd.add(jsonEncode(inputAdd));
                                        print(inputsAdd.contains(inputsAdd));
                                      } else {
                                        inputsAdd.remove(jsonEncode(InputAdd(
                                            inputs[index].hashed_id,
                                            inputs[index].default_value)));
                                      }
                                      print(inputsAdd);
                                    });
                                  },
                                  activeTrackColor: Colors.lightBlueAccent,
                                  activeColor: Colors.blue,
                                ),
                              ],
                            );
                          }),
                    ),
                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppLocalizations.of(context).pdf_language,
                              style: TextStyle(
                                  fontSize: 15, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              pdfText != null
                                  ? pdfText
                                  : AppLocalizations.of(context).choose_lang,
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && pdfLanguage == null
                                ? Text(
                                    "Please choose a language",
                                    style: TextStyle(
                                        color: Colors.red, fontSize: 12),
                                  )
                                : Container()
                          ],
                        ),
                        PopupMenuButton<String>(
                          icon: Icon(
                            Icons.arrow_drop_down,
                            color: Colors.blueAccent,
                            size: 35,
                          ),
                          onSelected: (String result) {
                            setState(() {
                              pdfLanguage = result;
                            });

                            for (var i in languages) {
                              if (i.hashed_id == result) {
                                print(i.title);
                                setState(() {
                                  pdfText = i.title;
                                });
                              }
                            }
                          },
                          itemBuilder: (BuildContext context) =>
                              <PopupMenuEntry<String>>[
                            PopupMenuItem<String>(
                              value: languages[0].hashed_id,
                              child: Text(languages[0].title),
                            ),
                            PopupMenuItem<String>(
                              value: languages[1].hashed_id,
                              child: Text(languages[1].title),
                            ),
                            PopupMenuItem<String>(
                              value: languages[2].hashed_id,
                              child: Text(languages[2].title),
                            ),
                          ],
                        )
                      ],
                    ),
                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    Column(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              Text(
                                AppLocalizations.of(context).currency_rate,
                                style: TextStyle(
                                    fontSize: 15, color: Colors.grey[500]),
                              ),
                              Text(
                                ' To TND',
                                style: TextStyle(fontSize: 8, fontFeatures: [
                                  FontFeature.enable('sups'),
                                ]),
                              )
                            ],
                          ),
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          // initialValue: widget.item.quantity.toString(),
                          validator: (val) => val.isEmpty
                              ? AppLocalizations.of(context).enter_currency
                              : null,
                          controller: _currencyController,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey[300]),
                              ),
                              border: InputBorder.none,
                              hintText: 'Currency converter',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: RaisedButton(
                          color: Color.fromRGBO(55, 86, 223, 1),
                          onPressed: () {
                            setState(() {
                              btnPressed = true;
                            });

                            if (_form.currentState.validate() &&
                                pdfLanguage != null &&
                                _currencyController.text != "" &&
                                _currencyController.text != null) {
                              data['pdfLanguage'] = pdfLanguage;
                              data['condition'] = condition;
                              data['billing'] = billing;
                              data['currency'] =
                                  int.parse(_currencyController.text);
                              data['bank'] = bank;
                              data['bankId'] = bankId;
                              data['stamp'] = stamp;
                              data['delivery'] = delivery;
                              data['inputs'] = inputsAdd;
                              Navigator.pop(context, data);
                            }
                          },
                          // padding: const EdgeInsets.all(0.0),
                          child: Container(
                            color: Color.fromRGBO(55, 86, 223, 1),
                            width: width * 0.78,
                            padding:
                                const EdgeInsets.only(top: 20.0, bottom: 20),
                            child: Row(
                              children: [
                                Spacer(),
                                Container(
                                    margin: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .apply
                                          .toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white),
                                    )),
                                Spacer(),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Image(
                                      image: AssetImage(
                                          "assets/images/arrowGray.png"),
                                    )),
                              ],
                            ),
                          ),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                    ),
                  ],
                ),
              )),
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/device.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/subscriptions/last_step.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Invoice extends StatefulWidget {
  @override
  int type;
  String choice;

  Invoice({Key key, @required this.type, this.choice}) : super(key: key);

  _InvoiceState createState() => _InvoiceState();
}

class _InvoiceState extends State<Invoice> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();

  final _companyController = TextEditingController();
  final _addressController = TextEditingController();
  final _stateController = TextEditingController();
  final _zipCodeController = TextEditingController();
  final _countryController = TextEditingController();

  bool obscureText = true;
  String mobile_token;
  List<Country> countries = [];
  String country;
  String countryText;
  bool btnPressed = false;
  final _firebaseMessaging = FirebaseMessaging.instance;
  Future listCountries;

  void initState() {
    listCountries = countryList();
    countryList().then((value) {
      setState(() {
        countries = value;
      });
      // for (var u in value) {
      //   if (widget.country == u.hashed_id) {
      //     setState(() {
      //       countryText = u.title;
      //     });
      //   }
      // }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (BuildContext context) => Login()));
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              'New Invoice',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
          Icon(Icons.format_list_bulleted_rounded,
          color: Colors.blue,)
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(245, 246, 252, 1),
          child: Column(
            children: [
              SizedBox(
                height: width / 11,
              ),
              Row(
                children: [
                  SizedBox(
                    width: width / 11,
                  ),
                  Container(
                    width: width / 6,
                    height: width / 6,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 9.0, bottom: 2.0, top: 2.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(width: 4,),
                              Icon(Icons.circle, color: Colors.grey[300], size: 7,)
                            ],
                          ),
                          SizedBox(height: 3,),
                          Image.asset("assets/images/icon10.png", fit: BoxFit.contain,),
                          SizedBox(height: 2,),
                          Text('Draft')
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: width / 15  ,),
                  Container(
                    width: width / 6,
                    height: width / 6,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 9.0, bottom: 2.0, top: 2.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(width: 4,),
                              Icon(Icons.circle, color: Colors.green, size: 7,)
                            ],
                          ),
                          SizedBox(height: 3,),
                          Image.asset("assets/images/icon11.png", fit: BoxFit.contain,),
                          SizedBox(height: 2,),
                          Text('Send')
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.83,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: width / 39,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  'Date',
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text('30 / 03 / 2021',
                                  style: TextStyle(fontSize: 13),
                                ),
                              ],
                            ),
                            Icon(Icons.calendar_today, color: Colors.blueAccent,),
                          ],
                        ),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                'Conditions',
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                countryText != null
                                    ? countryText
                                    :"Payable upon receipt",
                                style: TextStyle(fontSize: 13),
                              ),
                              btnPressed && country == null
                                  ? Text(
                                "Payable upon receipt",
                                style: TextStyle(
                                    color: Colors.red, fontSize: 12),
                              )
                                  : Container()
                            ],
                          ),
                          FutureBuilder(
                            future: listCountries,
                            builder: (BuildContext context,
                                AsyncSnapshot snapshot) {
                              if (!snapshot.hasData) {
                                return Text("loading...");
                              }
                              return PopupMenuButton<String>(
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.blueAccent,
                                  size: 35,
                                ),
                                itemBuilder: (context) => snapshot.data
                                    .map<PopupMenuItem<String>>(
                                        (value) => PopupMenuItem<String>(
                                      value: value.hashed_id,
                                      child: Text(
                                        value.title,
                                      ),
                                    ))
                                    .toList(),
                                onSelected: (value) {
                                  setState(() {
                                    country = value;
                                  });

                                  for (var i in countries) {
                                    if (value == i.hashed_id) {
                                      setState(() {
                                        countryText = i.title;
                                      });
                                    }
                                  }
                                },
                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  'Due Date',
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text('30 / 03 / 2021',
                                  style: TextStyle(fontSize: 13),
                                ),
                              ],
                            ),
                            Icon(Icons.calendar_today, color: Colors.blueAccent,),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: width / 15,
                      )
                    ],
                  ),
                ),
              ),

              SizedBox(
                height: width / 25,
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text('Invoice N°',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold
                  ),
                  ),
                  Container(
                    width: width * 0.38,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]
                    ),
                    child:  Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'INV-2021-',
                            hintStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                    ),
                  ),
                ],
              ),


              SizedBox(
                height: width / 27,
              ),

              Container(
                width: width * 0.83,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: width / 39,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                'Client',
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                countryText != null
                                    ? countryText
                                    :"Foulen ben foulen",
                                style: TextStyle(fontSize: 13),
                              ),
                              btnPressed && country == null
                                  ? Text(
                                "Payable upon receipt",
                                style: TextStyle(
                                    color: Colors.red, fontSize: 12),
                              )
                                  : Container()
                            ],
                          ),
                          FutureBuilder(
                            future: listCountries,
                            builder: (BuildContext context,
                                AsyncSnapshot snapshot) {
                              if (!snapshot.hasData) {
                                return Text("loading...");
                              }
                              return PopupMenuButton<String>(
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.blueAccent,
                                  size: 35,
                                ),
                                itemBuilder: (context) => snapshot.data
                                    .map<PopupMenuItem<String>>(
                                        (value) => PopupMenuItem<String>(
                                      value: value.hashed_id,
                                      child: Text(
                                        value.title,
                                      ),
                                    ))
                                    .toList(),
                                onSelected: (value) {
                                  setState(() {
                                    country = value;
                                  });

                                  for (var i in countries) {
                                    if (value == i.hashed_id) {
                                      setState(() {
                                        countryText = i.title;
                                      });
                                    }
                                  }
                                },
                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Reference Number',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      SizedBox(
                        height: width / 15,
                      )
                    ],
                  ),
                ),
              ),


              SizedBox(
                height: width / 11,
              ),

              Container(
                width: width * 0.83,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: width / 39,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                'Item Price Are',
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                countryText != null
                                    ? countryText
                                    :"Without Taxes",
                                style: TextStyle(fontSize: 13),
                              ),
                              btnPressed && country == null
                                  ? Text(
                                "Payable upon receipt",
                                style: TextStyle(
                                    color: Colors.red, fontSize: 12),
                              )
                                  : Container()
                            ],
                          ),
                          FutureBuilder(
                            future: listCountries,
                            builder: (BuildContext context,
                                AsyncSnapshot snapshot) {
                              if (!snapshot.hasData) {
                                return Text("loading...");
                              }
                              return PopupMenuButton<String>(
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.blueAccent,
                                  size: 35,
                                ),
                                itemBuilder: (context) => snapshot.data
                                    .map<PopupMenuItem<String>>(
                                        (value) => PopupMenuItem<String>(
                                      value: value.hashed_id,
                                      child: Text(
                                        value.title,
                                      ),
                                    ))
                                    .toList(),
                                onSelected: (value) {
                                  setState(() {
                                    country = value;
                                  });

                                  for (var i in countries) {
                                    if (value == i.hashed_id) {
                                      setState(() {
                                        countryText = i.title;
                                      });
                                    }
                                  }
                                },
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: width / 15,
                      )
                    ],
                  ),
                ),
              ),

              SizedBox(
                height: width / 11,
              ),


              Padding(
                padding:
                 EdgeInsets.only(top: 0.0, left: width / 11, right: 30.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Items',
                      style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                    ),
                    Icon(
                      Icons.add,
                      size: width / 14,
                      color: Colors.grey[700],
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: width / 27,
              ),


              Container(
                width: width * 0.83,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: width / 39,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Item',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Quantity',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Unit Price',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                'Tax',
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                countryText != null
                                    ? countryText
                                    :"19%",
                                style: TextStyle(fontSize: 13),
                              ),
                              btnPressed && country == null
                                  ? Text(
                                "Payable upon receipt",
                                style: TextStyle(
                                    color: Colors.red, fontSize: 12),
                              )
                                  : Container()
                            ],
                          ),
                          FutureBuilder(
                            future: listCountries,
                            builder: (BuildContext context,
                                AsyncSnapshot snapshot) {
                              if (!snapshot.hasData) {
                                return Text("loading...");
                              }
                              return PopupMenuButton<String>(
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.blueAccent,
                                  size: 35,
                                ),
                                itemBuilder: (context) => snapshot.data
                                    .map<PopupMenuItem<String>>(
                                        (value) => PopupMenuItem<String>(
                                      value: value.hashed_id,
                                      child: Text(
                                        value.title,
                                      ),
                                    ))
                                    .toList(),
                                onSelected: (value) {
                                  setState(() {
                                    country = value;
                                  });

                                  for (var i in countries) {
                                    if (value == i.hashed_id) {
                                      setState(() {
                                        countryText = i.title;
                                      });
                                    }
                                  }
                                },
                              );
                            },
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Price',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      SizedBox(
                        height: width / 15,
                      )
                    ],
                  ),
                ),
              ),

              SizedBox(
                height: width / 11,
              ),


              Container(
                width: width * 0.83,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: width / 39,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'General Conditions',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      SizedBox(
                        height: width / 15,
                      )
                    ],
                  ),
                ),
              ),


              SizedBox(
                height: width / 11,
              ),


              Container(
                width: width * 0.83,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: width / 39,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Subtotal',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Discount',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Taxes',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Total',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Paid',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Balance',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      SizedBox(
                        height: width / 15,
                      )
                    ],
                  ),
                ),
              ),

              SizedBox(
                height: width / 11,
              ),


              Container(
                width: width * 0.83,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: width / 39,
                      ),
                      TextFormField(
                        controller: _addressController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Add Attachment',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 6.0),
                            child: Text('Add Attachment', style: TextStyle(
                              color: Colors.blueAccent
                            ),),
                          )
                        ],
                      ),
                      SizedBox(
                        height: width / 15,
                      )
                    ],
                  ),
                ),
              ),

              SizedBox(
                height: MediaQuery.of(context).size.height / 5.6,
              ),
              // ignore: deprecated_member_use
              RaisedButton(
                  color: Color.fromRGBO(55, 86, 223, 1),
                  onPressed: () {
                    setState(() {
                      btnPressed = true;
                    });
                    print(widget.type);
                    print(widget.choice);
                    print(country);

                    if (_form.currentState.validate()) {
                      // Navigator.of(context).push(MaterialPageRoute(
                      //     builder: (BuildContext context) => LastStep(
                      //       type: widget.type,
                      //       choice: widget.choice,
                      //       company: _companyController.text,
                      //       address: _addressController.text,
                      //       state: _stateController.text,
                      //       zipCode: _zipCodeController.text,
                      //       country: "none",
                      //     )));
                    }
                  },
                  // padding: const EdgeInsets.all(0.0),
                  child: Container(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    width: width * 0.78,
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        Container(
                            margin: const EdgeInsets.only(left: 10.0),
                            child: Text(
                              AppLocalizations.of(context).finish.toUpperCase(),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            )),
                        Spacer(),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: AssetImage("assets/images/arrowGray.png"),
                            )),
                      ],
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0))),
              SizedBox(height: 10,)
            ],
          ),
        ),

      ),
    );
  }
}

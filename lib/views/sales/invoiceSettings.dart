import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/device.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/subscriptions/last_step.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class InvoiceSettings extends StatefulWidget {
  @override
  int type;
  String choice;

  InvoiceSettings({Key key, @required this.type, this.choice}) : super(key: key);

  _InvoiceSettingsState createState() => _InvoiceSettingsState();
}

class _InvoiceSettingsState extends State<InvoiceSettings> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();

  final _companyController = TextEditingController();
  final _addressController = TextEditingController();
  final _stateController = TextEditingController();
  final _zipCodeController = TextEditingController();
  final _countryController = TextEditingController();

  bool obscureText = true;
  String mobile_token;
  List<Country> countries = [];
  String country;
  String countryText;
  bool btnPressed = false;
  final _firebaseMessaging = FirebaseMessaging.instance;
  Future listCountries;

  void initState() {
    listCountries = countryList();
    countryList().then((value) {
      setState(() {
        countries = value;
      });
      // for (var u in value) {
      //   if (widget.country == u.hashed_id) {
      //     setState(() {
      //       countryText = u.title;
      //     });
      //   }
      // }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),

        title: Row(
          children: [
            Spacer(
              flex: 2,
            ),
            Text(
              'Invoice Settings',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
            Icon(Icons.close_sharp,
              color: Colors.blue,)
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(245, 246, 252, 1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.83,
                child: Column(

                  children: [
                    SizedBox(
                      height: width / 39,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              'PDF Language',
                              style: TextStyle(
                                  fontSize: 17, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              countryText != null
                                  ? countryText
                                  :"Please select the language of your PDF",
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && country == null
                                ? Text(
                              "Payable upon receipt",
                              style: TextStyle(
                                  color: Colors.red, fontSize: 12),
                            )
                                : Container()
                          ],
                        ),
                        FutureBuilder(
                          future: listCountries,
                          builder: (BuildContext context,
                              AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Text("loading...");
                            }
                            return PopupMenuButton<String>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              itemBuilder: (context) => snapshot.data
                                  .map<PopupMenuItem<String>>(
                                      (value) => PopupMenuItem<String>(
                                    value: value.hashed_id,
                                    child: Text(
                                      value.title,
                                    ),
                                  ))
                                  .toList(),
                              onSelected: (value) {
                                setState(() {
                                  country = value;
                                });

                                for (var i in countries) {
                                  if (value == i.hashed_id) {
                                    setState(() {
                                      countryText = i.title;
                                    });
                                  }
                                }
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              'Currency Rate',
                              style: TextStyle(
                                  fontSize: 17, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              countryText != null
                                  ? countryText
                                  :"Currency Converter",
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && country == null
                                ? Text(
                              "Payable upon receipt",
                              style: TextStyle(
                                  color: Colors.red, fontSize: 12),
                            )
                                : Container()
                          ],
                        ),
                        FutureBuilder(
                          future: listCountries,
                          builder: (BuildContext context,
                              AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Text("loading...");
                            }
                            return PopupMenuButton<String>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              itemBuilder: (context) => snapshot.data
                                  .map<PopupMenuItem<String>>(
                                      (value) => PopupMenuItem<String>(
                                    value: value.hashed_id,
                                    child: Text(
                                      value.title,
                                    ),
                                  ))
                                  .toList(),
                              onSelected: (value) {
                                setState(() {
                                  country = value;
                                });

                                for (var i in countries) {
                                  if (value == i.hashed_id) {
                                    setState(() {
                                      countryText = i.title;
                                    });
                                  }
                                }
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              'This Invoice is Recurrent',
                              style: TextStyle(
                                  fontSize: 17, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              countryText != null
                                  ? countryText
                                  :"Yes",
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && country == null
                                ? Text(
                              "Payable upon receipt",
                              style: TextStyle(
                                  color: Colors.red, fontSize: 12),
                            )
                                : Container()
                          ],
                        ),
                        FutureBuilder(
                          future: listCountries,
                          builder: (BuildContext context,
                              AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Text("loading...");
                            }
                            return PopupMenuButton<String>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              itemBuilder: (context) => snapshot.data
                                  .map<PopupMenuItem<String>>(
                                      (value) => PopupMenuItem<String>(
                                    value: value.hashed_id,
                                    child: Text(
                                      value.title,
                                    ),
                                  ))
                                  .toList(),
                              onSelected: (value) {
                                setState(() {
                                  country = value;
                                });

                                for (var i in countries) {
                                  if (value == i.hashed_id) {
                                    setState(() {
                                      countryText = i.title;
                                    });
                                  }
                                }
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                'Starts By',
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text('30 / 03 / 2021',
                                style: TextStyle(fontSize: 13),
                              ),
                            ],
                          ),
                          Icon(Icons.calendar_today, color: Colors.blueAccent,),
                        ],
                      ),
                    ),
                    SizedBox(height: 20,),
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                'Ends By',
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text('30 / 03 / 2021',
                                style: TextStyle(fontSize: 13),
                              ),
                            ],
                          ),
                          Icon(Icons.calendar_today, color: Colors.blueAccent,),
                        ],
                      ),
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              'Each',
                              style: TextStyle(
                                  fontSize: 17, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              countryText != null
                                  ? countryText
                                  :"Payable upon receipt",
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && country == null
                                ? Text(
                              "Payable upon receipt",
                              style: TextStyle(
                                  color: Colors.red, fontSize: 12),
                            )
                                : Container()
                          ],
                        ),
                        FutureBuilder(
                          future: listCountries,
                          builder: (BuildContext context,
                              AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Text("loading...");
                            }
                            return PopupMenuButton<String>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              itemBuilder: (context) => snapshot.data
                                  .map<PopupMenuItem<String>>(
                                      (value) => PopupMenuItem<String>(
                                    value: value.hashed_id,
                                    child: Text(
                                      value.title,
                                    ),
                                  ))
                                  .toList(),
                              onSelected: (value) {
                                setState(() {
                                  country = value;
                                });

                                for (var i in countries) {
                                  if (value == i.hashed_id) {
                                    setState(() {
                                      countryText = i.title;
                                    });
                                  }
                                }
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              'Repetition',
                              style: TextStyle(
                                  fontSize: 17, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              countryText != null
                                  ? countryText
                                  :"Day",
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && country == null
                                ? Text(
                              "Payable upon receipt",
                              style: TextStyle(
                                  color: Colors.red, fontSize: 12),
                            )
                                : Container()
                          ],
                        ),
                        FutureBuilder(
                          future: listCountries,
                          builder: (BuildContext context,
                              AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Text("loading...");
                            }
                            return PopupMenuButton<String>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              itemBuilder: (context) => snapshot.data
                                  .map<PopupMenuItem<String>>(
                                      (value) => PopupMenuItem<String>(
                                    value: value.hashed_id,
                                    child: Text(
                                      value.title,
                                    ),
                                  ))
                                  .toList(),
                              onSelected: (value) {
                                setState(() {
                                  country = value;
                                });

                                for (var i in countries) {
                                  if (value == i.hashed_id) {
                                    setState(() {
                                      countryText = i.title;
                                    });
                                  }
                                }
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              'Billing Address',
                              style: TextStyle(
                                  fontSize: 17, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              countryText != null
                                  ? countryText
                                  :"Yes",
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && country == null
                                ? Text(
                              "Payable upon receipt",
                              style: TextStyle(
                                  color: Colors.red, fontSize: 12),
                            )
                                : Container()
                          ],
                        ),
                        FutureBuilder(
                          future: listCountries,
                          builder: (BuildContext context,
                              AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Text("loading...");
                            }
                            return PopupMenuButton<String>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              itemBuilder: (context) => snapshot.data
                                  .map<PopupMenuItem<String>>(
                                      (value) => PopupMenuItem<String>(
                                    value: value.hashed_id,
                                    child: Text(
                                      value.title,
                                    ),
                                  ))
                                  .toList(),
                              onSelected: (value) {
                                setState(() {
                                  country = value;
                                });

                                for (var i in countries) {
                                  if (value == i.hashed_id) {
                                    setState(() {
                                      countryText = i.title;
                                    });
                                  }
                                }
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              'Delivery Address',
                              style: TextStyle(
                                  fontSize: 17, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              countryText != null
                                  ? countryText
                                  :"Yes",
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && country == null
                                ? Text(
                              "Payable upon receipt",
                              style: TextStyle(
                                  color: Colors.red, fontSize: 12),
                            )
                                : Container()
                          ],
                        ),
                        FutureBuilder(
                          future: listCountries,
                          builder: (BuildContext context,
                              AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Text("loading...");
                            }
                            return PopupMenuButton<String>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              itemBuilder: (context) => snapshot.data
                                  .map<PopupMenuItem<String>>(
                                      (value) => PopupMenuItem<String>(
                                    value: value.hashed_id,
                                    child: Text(
                                      value.title,
                                    ),
                                  ))
                                  .toList(),
                              onSelected: (value) {
                                setState(() {
                                  country = value;
                                });

                                for (var i in countries) {
                                  if (value == i.hashed_id) {
                                    setState(() {
                                      countryText = i.title;
                                    });
                                  }
                                }
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              'General Conditions',
                              style: TextStyle(
                                  fontSize: 17, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              countryText != null
                                  ? countryText
                                  :"Yes",
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && country == null
                                ? Text(
                              "Payable upon receipt",
                              style: TextStyle(
                                  color: Colors.red, fontSize: 12),
                            )
                                : Container()
                          ],
                        ),
                        FutureBuilder(
                          future: listCountries,
                          builder: (BuildContext context,
                              AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Text("loading...");
                            }
                            return PopupMenuButton<String>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              itemBuilder: (context) => snapshot.data
                                  .map<PopupMenuItem<String>>(
                                      (value) => PopupMenuItem<String>(
                                    value: value.hashed_id,
                                    child: Text(
                                      value.title,
                                    ),
                                  ))
                                  .toList(),
                              onSelected: (value) {
                                setState(() {
                                  country = value;
                                });

                                for (var i in countries) {
                                  if (value == i.hashed_id) {
                                    setState(() {
                                      countryText = i.title;
                                    });
                                  }
                                }
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: width / 39,
                            ),
                            Text(
                              'Timbre Fiscal',
                              style: TextStyle(
                                  fontSize: 17, color: Colors.grey[500]),
                            ),
                            SizedBox(
                              height: width / 70,
                            ),
                            Text(
                              countryText != null
                                  ? countryText
                                  :"Yes",
                              style: TextStyle(fontSize: 13),
                            ),
                            btnPressed && country == null
                                ? Text(
                              "Payable upon receipt",
                              style: TextStyle(
                                  color: Colors.red, fontSize: 12),
                            )
                                : Container()
                          ],
                        ),
                        FutureBuilder(
                          future: listCountries,
                          builder: (BuildContext context,
                              AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Text("loading...");
                            }
                            return PopupMenuButton<String>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              itemBuilder: (context) => snapshot.data
                                  .map<PopupMenuItem<String>>(
                                      (value) => PopupMenuItem<String>(
                                    value: value.hashed_id,
                                    child: Text(
                                      value.title,
                                    ),
                                  ))
                                  .toList(),
                              onSelected: (value) {
                                setState(() {
                                  country = value;
                                });

                                for (var i in countries) {
                                  if (value == i.hashed_id) {
                                    setState(() {
                                      countryText = i.title;
                                    });
                                  }
                                }
                              },
                            );
                          },
                        ),
                      ],
                    ),
                    SizedBox(
                      height: width / 15,
                    )
                  ],
                ),
              ),




              SizedBox(
                height: MediaQuery.of(context).size.height / 5.6,
              ),
              // ignore: deprecated_member_use
              Center(
                child: RaisedButton(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    onPressed: () {
                      setState(() {
                        btnPressed = true;
                      });
                      print(widget.type);
                      print(widget.choice);
                      print(country);

                      if (_form.currentState.validate()) {
                        // Navigator.of(context).push(MaterialPageRoute(
                        //     builder: (BuildContext context) => LastStep(
                        //       type: widget.type,
                        //       choice: widget.choice,
                        //       company: _companyController.text,
                        //       address: _addressController.text,
                        //       state: _stateController.text,
                        //       zipCode: _zipCodeController.text,
                        //       country: "none",
                        //     )));
                      }
                    },
                    // padding: const EdgeInsets.all(0.0),
                    child: Container(
                      color: Color.fromRGBO(55, 86, 223, 1),
                      width: width * 0.78,
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                      child: Row(
                        children: [
                          Spacer(),
                          Container(
                              margin: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                'apply'.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white),
                              )),
                          Spacer(),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Image(
                                image: AssetImage("assets/images/arrowGray.png"),
                              )),
                        ],
                      ),
                    ),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0))),
              ),
              SizedBox(height: 10,)
            ],
          ),
        ),

      ),
    );
  }
}

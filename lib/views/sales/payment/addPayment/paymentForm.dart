import 'dart:convert';

import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/stockController.dart';
import 'package:birou/generated/l10n.dart';
import 'package:birou/models/article.dart';
import 'package:birou/models/client.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/item.dart';
import 'package:birou/models/paymentItem.dart';
import 'package:birou/models/tax.dart';
import 'package:birou/models/taxAdd.dart';
import 'package:birou/views/sales/delivery/addDelivery/add.dart';
import 'package:birou/views/sales/delivery/addDelivery/taxForm.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gson/gson.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

typedef PrintTax();

class PaymentForm extends StatefulWidget {
  final PaymentItem item;
  final state = _PaymentFormState();
  String companyId;
  final Function() notifyParent;
  final PrintTax printTax;
  final bool paymentEntry;

  PaymentForm({
    Key key,
    this.item,
    @required this.companyId,
    this.notifyParent,
    this.printTax,
    this.paymentEntry
  }) : super(key: key);

  @override
  _PaymentFormState createState() => state;

  bool isValid() => state.validate();
}

class _PaymentFormState extends State<PaymentForm> {
  final form = GlobalKey<FormState>();
  final titleController = TextEditingController();

  String mobile_token;
  bool btnPressed = false;
  final _invoiceController = TextEditingController();
  final _totalController = TextEditingController();
  final _toPayController = TextEditingController();
  final _paymentController = TextEditingController();

  String sum;

  void initState() {
    setState(() {
      _invoiceController.text = widget.item.invoice_id;
      _totalController.text = widget.item.total;
      _toPayController.text = widget.item.to_pay;
      _paymentController.text = widget.item.amount.toString();

    });

    super.initState();
  }

  void dispose() {
    _invoiceController.dispose();
    _totalController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(11),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.blue[100],
                spreadRadius: 0.5,
                blurRadius: 7,
                offset: Offset(0, 1), // changes position of shadow
              ),
            ]),
        child: Form(
          key: form,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.width / 39,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Column(
                        children: [
                          _invoiceController.text.isNotEmpty
                              ? Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    AppLocalizations.of(context).invoice,
                                    style: TextStyle(fontSize: 10),
                                  ),
                                )
                              : Container(),
                          TextFormField(
                            enabled: false,
                            controller: _invoiceController,
                            // onSaved: (val) => widget.item.invoice_hashed_id = val,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context).invoice,
                                hintStyle: TextStyle(
                                    color: Colors.grey[500],
                                    fontSize: 17,
                                    fontWeight: FontWeight.normal)),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Column(
                        children: [
                          _totalController.text.isNotEmpty
                              ? Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              AppLocalizations.of(context).total,
                              style: TextStyle(fontSize: 10),
                            ),
                          )
                              : Container(),
                          TextFormField(
                            enabled: false,
                            // initialValue: widget.item.quantity.toString(),
                            // onSaved: (val) => widget.item.qte = int.parse(val),
                            controller: _totalController,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context).total,
                                hintStyle: TextStyle(
                                    color: Colors.grey[500],
                                    fontSize: 17,
                                    fontWeight: FontWeight.normal)),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Column(
                        children: [
                          _toPayController.text.isNotEmpty
                              ? Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              AppLocalizations.of(context).to_pay,
                              style: TextStyle(fontSize: 10),
                            ),
                          )
                              : Container(),
                          TextFormField(
                            enabled: false,
                            controller: _toPayController,
                            // initialValue: widget.item.up.toString(),
                            // onSaved: (val) => widget.item.price = double.parse(val),
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context).to_pay,
                                hintStyle: TextStyle(
                                    color: Colors.grey[500],
                                    fontSize: 17,
                                    fontWeight: FontWeight.normal)),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[300],
                      thickness: 1.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Column(
                        children: [
                          _paymentController.text.isNotEmpty
                              ? Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              AppLocalizations.of(context).payment,
                              style: TextStyle(fontSize: 10),
                            ),
                          )
                              : Container(),
                          TextFormField(
                            enabled: widget.paymentEntry == true ? true : false,
                            controller: _paymentController,
                            onSaved: (val) =>
                                widget.item.amount = double.parse(val),
                            onChanged: (val) {
                              print(val);
                              if(val != null && val != ''){
                                setState(() {
                                  widget.item.amount = double.parse(val);
                                });
                              }else{
                                setState(() {
                                  widget.item.amount = 0;
                                });
                              }
                            },
                            inputFormatters: <TextInputFormatter>[
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.number,
                            validator: (val) =>
                            val.isEmpty ? AppLocalizations.of(context).enter_amount : null,                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context).payment,
                                hintStyle: TextStyle(
                                    color: Colors.grey[500],
                                    fontSize: 17,
                                    fontWeight: FontWeight.normal)),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.width / 39,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///form validator
  bool validate() {
    var valid = form.currentState.validate();
    if (valid) form.currentState.save();
    return valid;
  }

  int sumTotal(List<int> taxes) {
    int sum = 0;

    taxes.forEach((tax) => {tax != null ? sum = sum + tax : sum});

    return sum;
  }
}

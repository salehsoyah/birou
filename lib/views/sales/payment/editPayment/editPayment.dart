import 'dart:convert';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/contactsController.dart';
import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/controllers/deliveryNotesController.dart';
import 'package:birou/controllers/paymentController.dart';
import 'package:birou/models/bank.dart';
import 'package:birou/models/client.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/item.dart';
import 'package:birou/models/language.dart';
import 'package:birou/models/paymentItem.dart';
import 'package:birou/models/tax.dart';
import 'package:birou/views/dashboard/home.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:birou/views/sales/payment/addPayment/paymentForm.dart';
import 'package:birou/views/sales/payment/addPayment/paymentSettings.dart';
import 'package:birou/views/sales/payment/payments.dart';
import 'package:date_field/date_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EditPayment extends StatefulWidget {
  @override
  String companyId;
  final List<Company> companies;
  final String paymentId;

  EditPayment({Key key, @required this.companyId, @required this.companies, @required this.paymentId});

  _EditPaymentState createState() => _EditPaymentState();
}

class _EditPaymentState extends State<EditPayment> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();

  bool obscureText = true;
  String mobile_token;
  List<Country> countries = [];
  String country;
  String countryText;
  bool btnPressed = false;
  Future listCountries;
  Future companyTaxes;
  Future banksList;
  Future clientList;
  Future paymentMethods;

  List<Tax> taxes = [
    Tax("Hors Taxes", 1, "", 0),
    Tax("Taxes incluses", 2, "", 0),
  ];

  String discount = '0';

  String pdfText;
  List<Language> languages = [
    Language("Français", "fr"),
    Language("English", "en"),
    Language("Arabic", "ar")
  ];

  int taxPicked = 1;
  String taxText;

  List<Clientt> clients = [];
  String clientText;
  String client;

  String methodText;
  String method;

  String conditionText;
  List<Condition> conditions = [
    Condition("Oui", 1),
    Condition("Non", 0),
  ];

  DateTime date = DateTime.now();

  bool condition = true;
  bool stamp = false;
  bool billing = true;
  bool delivery = true;
  bool bank = true;
  int currency = 1;
  List<String> inputs = [];
  String pdfLanguage;
  String bankId;

  final _deliveryNumberController = TextEditingController();
  final _referenceController = TextEditingController();
  final _bankFeesController = TextEditingController();
  final _notesController = TextEditingController();
  final _totalController = TextEditingController();
  final _subtotalController = TextEditingController();
  final _taxesController = TextEditingController();

  List<PaymentForm> items = [];
  List<Bank> banks = [];

  var articles;

  String bankText;

  void initState() {

    getPayment(widget.companyId, widget.paymentId).then((values) {
      print(values);
      // for (var value in values) {
      //   var item = PaymentItem(
      //       value.invoice_hashed_id,
      //       value.disbursement_hashed_id,
      //       value.amount,
      //       value.invoice_id,
      //       value.total,
      //       value.to_pay);
      //   setState(() {
      //     items.add(PaymentForm(
      //       item: item,
      //       companyId: widget.companyId,
      //       notifyParent: refresh,
      //       printTax: printTax,
      //       paymentEntry: true,
      //     ));
      //   });
      // }
    });

    banksList = bankList(widget.companyId);
    clientList = clientsPaymentList(widget.companyId);
    paymentMethods = companyMethods(widget.companyId);

    companyMethods(widget.companyId).then((value){
      setState(() {
        method = value[0].hashed_id;
        for(var i in value){
          if(i.hashed_id == method){
            setState(() {
              methodText = i.title;
            });
          }
        }
      });
    });

    clientsList(widget.companyId).then((value) {
      setState(() {
        clients = value;
      });
    });

    bankList(widget.companyId).then((value) {
      setState(() {
        banks = value;
      });
    });

    nextPayment(widget.companyId).then((value) {
      setState(() {
        _deliveryNumberController.text = value['data']['nextSalesPaymentNumber'];
      });
    });

    companyDashboard(widget.companyId).then((result) {
      setState(() {
        pdfLanguage = result['data']['company']['language'];
      });
    });


    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              AppLocalizations.of(context).new_payment,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return PaymentSettings(
                        companyId: widget.companyId,
                        companies: widget.companies,
                        language: pdfLanguage,
                        currency: currency,
                      );
                    }).then((returnValue) {
                  print(returnValue);
                  if(returnValue != false){
                    setState(() {
                      pdfLanguage = returnValue['pdfLanguage'];
                      condition = returnValue['condition'];
                      billing = returnValue['billing'];
                      currency = returnValue['currency'];
                      bank = returnValue['bank'];
                      bankId = returnValue['bankId'];
                      stamp = returnValue['stamp'];
                      delivery = returnValue['delivery'];
                      inputs = returnValue['inputs'];

                    });
                  }
                });
              },
              child: Icon(
                Icons.format_list_bulleted_rounded,
                color: Colors.blue,
              ),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _form,
          child: Container(
            color: Color.fromRGBO(245, 246, 252, 1),
            child: Column(
              children: [
                SizedBox(
                  height: width / 11,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 25,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          btnPressed = true;
                        });
                        onSave();
                        // print(articles);
                        if (_form.currentState.validate() &&
                            client != null &&
                            articles != null && _bankFeesController.text.isNotEmpty) {
                          newPayment(
                              date.toString().substring(0, 10),
                              _deliveryNumberController.text,
                              _notesController.text,
                              _referenceController.text,
                              double.parse(_bankFeesController.text),
                              bankId,
                              method,
                              client,
                              currency,
                              pdfLanguage,
                              articles,
                              widget.companyId)
                              .then((value) {
                            print(value);
                            if (value['status']['code'] != 200) {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return DialogScreen(
                                      image: 0,
                                      text1: 'Error',
                                      text2: value['status']['message'],
                                    );
                                  });
                            } else {
                              Navigator.of(context)
                                  .pushReplacement(MaterialPageRoute(
                                  builder: (BuildContext context) => Home(
                                    companies: widget.companies,
                                    companyId: widget.companyId,
                                    currentScreen: Payments(
                                      companies: widget.companies,
                                      companyId: widget.companyId,
                                    ),
                                  )));
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return DialogScreen(
                                      image: 1,
                                      text1: 'Success',
                                      text2: value['status']['message'],
                                    );
                                  });
                            }
                          });
                        }
                      },
                      child: Container(
                        width: width / 6,
                        height: width / 6,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.blue[100],
                                spreadRadius: 0.5,
                                blurRadius: 7,
                                offset:
                                Offset(0, 1), // changes position of shadow
                              ),
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 9.0, bottom: 2.0, top: 2.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Icon(
                                    Icons.circle,
                                    color: Colors.grey[300],
                                    size: 7,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Image.asset(
                                "assets/images/icon10.png",
                                fit: BoxFit.contain,
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Text(AppLocalizations.of(context).draft, style: TextStyle(
                                  fontSize: 11
                              ),)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 11,
                ),

                Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: width / 39,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: width / 39,
                                  ),
                                  Text(
                                    AppLocalizations.of(context).client,
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.grey[500]),
                                  ),
                                  SizedBox(
                                    height: width / 70,
                                  ),
                                  Text(
                                    clientText != null
                                        ? clientText
                                        : "Foulen ben foulen",
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  btnPressed && client == null
                                      ? Text(
                                    AppLocalizations.of(context)
                                        .enter_client,
                                    style: TextStyle(
                                        color: Colors.red, fontSize: 12),
                                  )
                                      : Container()
                                ],
                              ),
                              FutureBuilder(
                                future: clientList,
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  if (!snapshot.hasData) {
                                    return Text(
                                        AppLocalizations.of(context).loading);
                                  }
                                  return PopupMenuButton<String>(
                                    icon: Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.blueAccent,
                                      size: 35,
                                    ),
                                    itemBuilder: (context) => snapshot.data
                                        .map<PopupMenuItem<String>>(
                                            (value) => PopupMenuItem<String>(
                                          value: value.id,
                                          child: Text(
                                            value.display_name,
                                          ),
                                        ))
                                        .toList(),
                                    onSelected: (value) {
                                      setState(() {
                                        client = value;
                                      });

                                      for (var i in clients) {
                                        if (value == i.id) {
                                          setState(() {
                                            clientText = i.display_name;
                                            setState(() {
                                              items.clear();
                                            });
                                            getClientPayments(widget.companyId, client).then((values) {
                                              print(values);
                                              for (var value in values) {
                                                var item = PaymentItem(
                                                    value.invoice_hashed_id,
                                                    value.disbursement_hashed_id,
                                                    value.amount,
                                                    value.invoice_id,
                                                    value.total,
                                                    value.to_pay);
                                                setState(() {
                                                  items.add(PaymentForm(
                                                    item: item,
                                                    companyId: widget.companyId,
                                                    notifyParent: refresh,
                                                    printTax: printTax,
                                                    paymentEntry: true,
                                                  ));
                                                });
                                              }
                                            });


                                          });
                                        }
                                      }

                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: width / 15,
                          )
                        ],
                      ),
                    ),
                  ),
                ),

                // SizedBox(
                //   height: width / 11,
                // ),
                // Padding(
                //   padding: const EdgeInsets.only(left: 25, right: 25),
                //   child: Container(
                //     decoration: BoxDecoration(
                //         color: Colors.white,
                //         borderRadius: BorderRadius.circular(20),
                //         boxShadow: [
                //           BoxShadow(
                //             color: Colors.blue[100],
                //             spreadRadius: 0.5,
                //             blurRadius: 7,
                //             offset: Offset(0, 1), // changes position of shadow
                //           ),
                //         ]),
                //     child: Padding(
                //       padding: const EdgeInsets.only(left: 25),
                //       child: Column(
                //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //         crossAxisAlignment: CrossAxisAlignment.center,
                //         children: [
                //           SizedBox(
                //             height: width / 39,
                //           ),
                //           TextFormField(
                //             controller: _referenceController,
                //             decoration: InputDecoration(
                //                 border: InputBorder.none,
                //                 hintText:
                //                 AppLocalizations.of(context).paid_amount,
                //                 hintStyle: TextStyle(
                //                     color: Colors.grey[500],
                //                     fontSize: 17,
                //                     fontWeight: FontWeight.normal)),
                //           ),
                //           SizedBox(
                //             height: width / 39,
                //           )
                //         ],
                //       ),
                //     ),
                //   ),
                // ),

                SizedBox(
                  height: width / 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      AppLocalizations.of(context).payment_nb,
                      style:
                      TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                    Container(
                      width: width * 0.38,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.blue[100],
                              spreadRadius: 0.5,
                              blurRadius: 7,
                              offset:
                              Offset(0, 1), // changes position of shadow
                            ),
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          validator: (val) => val.length < 1
                              ? 'Payment number is required'
                              : null,
                          controller: _deliveryNumberController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'PAY-2021-',
                              hintStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 27,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: width / 39,
                          ),
                          DateTimeFormField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintStyle: TextStyle(color: Colors.black45),
                              errorStyle: TextStyle(color: Colors.redAccent),
                              suffixIcon: Icon(Icons.event_note),
                              labelText: date != null
                                  ? date.toString().substring(0, 10)
                                  : AppLocalizations.of(context).date,
                            ),
                            firstDate:
                            DateTime.now().add(const Duration(days: 10)),
                            initialDate:
                            DateTime.now().add(const Duration(days: 10)),
                            autovalidateMode: AutovalidateMode.always,
                            validator: (DateTime e) =>
                            (date == null) ? 'Please enter date' : null,
                            onDateSelected: (DateTime value) {
                              print(value);
                              setState(() {
                                date = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: width / 50,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: width / 39,
                          ),
                          TextFormField(
                            controller: _referenceController,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText:
                                AppLocalizations.of(context).reference_nb,
                                hintStyle: TextStyle(
                                    color: Colors.grey[500],
                                    fontSize: 17,
                                    fontWeight: FontWeight.normal)),
                          ),
                          SizedBox(
                            height: width / 39,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: width / 39,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: width / 39,
                                  ),
                                  Text(
                                    AppLocalizations.of(context).payment_method,
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.grey[500]),
                                  ),
                                  SizedBox(
                                    height: width / 70,
                                  ),
                                  Text(
                                    methodText != null
                                        ? methodText
                                        : AppLocalizations.of(context).payment_method,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  btnPressed && method == null
                                      ? Text(
                                    AppLocalizations.of(context)
                                        .enter_payment_method,
                                    style: TextStyle(
                                        color: Colors.red, fontSize: 12),
                                  )
                                      : Container()
                                ],
                              ),
                              FutureBuilder(
                                future: paymentMethods,
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  if (!snapshot.hasData) {
                                    return Text(
                                        AppLocalizations.of(context).loading);
                                  }
                                  return PopupMenuButton<String>(
                                    icon: Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.blueAccent,
                                      size: 35,
                                    ),
                                    itemBuilder: (context) => snapshot.data
                                        .map<PopupMenuItem<String>>(
                                            (value) => PopupMenuItem<String>(
                                          value: value.hashed_id,
                                          child: Text(
                                            value.title,
                                          ),
                                        ))
                                        .toList(),
                                    onSelected: (value) {
                                      setState(() {
                                        client = value;
                                      });

                                      for (var i in clients) {
                                        if (value == i.id) {
                                          setState(() {
                                            clientText = i.display_name;
                                            print(i.display_name);
                                          });
                                        }
                                      }
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: width / 15,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: width / 39,
                          ),
                          TextFormField(
                            validator: (val) => val.length < 1
                                ? 'Bank fee is required'
                                : null,
                            controller: _bankFeesController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText:
                                AppLocalizations.of(context).bank_fees,
                                hintStyle: TextStyle(
                                    color: Colors.grey[500],
                                    fontSize: 17,
                                    fontWeight: FontWeight.normal)),
                          ),
                          SizedBox(
                            height: width / 39,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: width / 39,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: width / 39,
                                  ),
                                  Text(
                                    AppLocalizations.of(context).bank,
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.grey[500]),
                                  ),
                                  SizedBox(
                                    height: width / 70,
                                  ),
                                  Text(
                                    bankText != null
                                        ? bankText
                                        : AppLocalizations.of(context).bank,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  // btnPressed && bankId == null
                                  //     ? Text(
                                  //   AppLocalizations.of(context)
                                  //       .enter_bank,
                                  //   style: TextStyle(
                                  //       color: Colors.red, fontSize: 12),
                                  // )
                                  //     : Container()
                                ],
                              ),
                              FutureBuilder(
                                future: banksList,
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  if (!snapshot.hasData) {
                                    return Text(
                                        AppLocalizations.of(context).loading);
                                  }
                                  return PopupMenuButton<String>(
                                    icon: Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.blueAccent,
                                      size: 35,
                                    ),
                                    itemBuilder: (context) => snapshot.data
                                        .map<PopupMenuItem<String>>(
                                            (value) => PopupMenuItem<String>(
                                          value: value.id,
                                          child: Text(
                                            value.title,
                                          ),
                                        ))
                                        .toList(),
                                    onSelected: (value) {
                                      setState(() {
                                        client = value;
                                      });

                                      for (var i in clients) {
                                        if (value == i.id) {
                                          setState(() {
                                            clientText = i.display_name;
                                            print(i.display_name);
                                          });
                                        }
                                      }
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: width / 15,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 15,
                ),
                items.length > 0
                    ? ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  addAutomaticKeepAlives: true,
                  itemCount: items.length,
                  itemBuilder: (_, i) => Padding(
                    padding: const EdgeInsets.only(left: 15.0, right: 15),
                    child: items[i],
                  ),
                )
                    : Container(),
                Padding(
                  padding: const EdgeInsets.only(right: 25.0, left: 25, top: 10),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: width / 39,
                          ),
                          Column(
                            children: [
                              _subtotalController.text.isNotEmpty
                                  ? Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  AppLocalizations.of(context).received,
                                  style: TextStyle(fontSize: 10),
                                ),
                              )
                                  : Container(),
                              TextFormField(
                                controller: _subtotalController,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText:
                                    AppLocalizations.of(context).received,
                                    hintStyle: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 17,
                                        fontWeight: FontWeight.normal)),
                              ),
                            ],
                          ),
                          Divider(
                            color: Colors.grey[300],
                            thickness: 1.0,
                          ),
                          Column(
                            children: [
                              _taxesController.text.isNotEmpty
                                  ? Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  AppLocalizations.of(context).used,
                                  style: TextStyle(fontSize: 10),
                                ),
                              )
                                  : Container(),
                              TextFormField(
                                controller: _taxesController,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: sumSubTotal(items.map((it) => (it.item.amount).toString()).toList()).toString(),
                                    hintStyle: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 17,
                                        fontWeight: FontWeight.normal)),
                              ),
                            ],
                          ),
                          Divider(
                            color: Colors.grey[300],
                            thickness: 1.0,
                          ),
                          Column(
                            children: [
                              _totalController.text.isNotEmpty
                                  ? Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  AppLocalizations.of(context).available,
                                  style: TextStyle(fontSize: 10),
                                ),
                              )
                                  : Container(),
                              TextFormField(
                                controller: _totalController,
                                enabled: false,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText:
                                    AppLocalizations.of(context).available,
                                    hintStyle: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 17,
                                        fontWeight: FontWeight.normal)),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: width / 39,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 25.0, left: 25),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[100],
                            spreadRadius: 0.5,
                            blurRadius: 7,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: width / 39,
                          ),
                          TextFormField(
                            controller: _notesController,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context).notes,
                                hintStyle: TextStyle(
                                    color: Colors.grey[500],
                                    fontSize: 17,
                                    fontWeight: FontWeight.normal)),
                          ),
                          SizedBox(
                            height: width / 15,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                SizedBox(
                  height: width / 11,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onDelete(Item item) {
    setState(() {
      var find = items.firstWhere(
            (it) => it.item == item,
        orElse: () => null,
      );
      if (find != null) items.removeAt(items.indexOf(find));
    });
    printTax();
  }


  void refresh() {
    setState(() {});
  }

  void onSave() {
    if (items.length > 0) {
      var allValid = true;
      items.forEach((form) => {allValid = allValid && form.isValid()});
      if (allValid) {
        var data = items.map((it) => jsonEncode(it.item)).toList();
        setState(() {
          articles = data;
        });
      }
    }
  }

  double sumTotal(List<String> prices, String discount) {
    double sum = 0;
    double sumWD = -1;
    prices.forEach(
            (price) => {price != null ? sum = sum + double.parse(price) : sum});

    if (discount != '0' && discount != '') {
      sumWD = sum - double.parse(discount);
    }

    if (sumWD > 0) {
      return sumWD;
    } else {
      return sum;
    }
  }

  double sumSubTotal(List<String> prices) {
    double sum = 0;
    prices.forEach(
            (price) => {price != null ? sum = sum + double.parse(price) : sum});
    setState(() {
      _subtotalController.text = "0";
      _taxesController.text = sum.toString();
      _totalController.text = (0 - sum).toString();
    });
    return sum;
  }

  double sumTaxesTotal(List<String> prices) {
    double sum = 0;
    setState(() {
      prices.forEach((price) => {
        price != null ? sum = sum + double.parse(price) : sum,
      });
    });

    return sum;
  }

  void printTax() {
    // double taxes = 0;
    // double subtotal = 0;
    // double total = 0;
    //
    // items.forEach((item) {
    //   var qty = item.qty != null ? item.qty : 0;
    //   var pu = item.up != null ? item.up : 0;
    //
    //   var newPu = double.parse(pu.toString()) * qty;
    //
    //   if (taxPicked == 1) {
    //     item.state.taxForms.forEach((taxForm) {
    //       if (taxForm.special == 1) {
    //         var tax = taxForm.rate;
    //         newPu = newPu + double.parse((newPu * (tax / 100)).toString());
    //       }
    //     });
    //     item.state.taxForms.forEach((taxForm) {
    //       var tax = taxForm.rate;
    //       if (taxForm.special == 1) {
    //         taxes = taxes + ((pu * (tax / 100)) * (qty));
    //       } else {
    //         taxes = taxes + (newPu * (tax / 100));
    //       }
    //     });
    //   } else {
    //     item.state.taxForms.forEach((taxForm) {
    //       if (taxForm.special == 0) {
    //         var tax = taxForm.rate;
    //
    //         taxes = taxes + (((pu * (tax / 100)) * qty) / ((tax / 100) + 1));
    //
    //         newPu = newPu - taxes;
    //       }
    //     });
    //     item.state.taxForms.forEach((taxForm) {
    //       var tax = taxForm.rate;
    //       if (taxForm.special == 1) {
    //         taxes = taxes + ((newPu * (tax / 100)) / ((tax / 100) + 1));
    //       }
    //     });
    //   }
    //
    //   setState(() {
    //     total = total + newPu;
    //   });
    // });
    //
    // double discount = _discountController.text != ''
    //     ? double.parse(_discountController.text)
    //     : 0;
    //
    // if (discount < 0) {
    //   setState(() {
    //     discount = 0;
    //   });
    // }
    // setState(() {
    //   _subtotalController.text = (total).toStringAsFixed(2).toString();
    //   _taxesController.text = taxes.toStringAsFixed(2).toString();
    // });
    // if (taxPicked == 1) {
    //   if (((total + taxes) - discount) > 0) {
    //     setState(() {
    //       _totalController.text =
    //           ((total + taxes) - discount).toStringAsFixed(2).toString();
    //     });
    //   } else {
    //     setState(() {
    //       _totalController.text =
    //           ((total + taxes)).toStringAsFixed(2).toString();
    //     });
    //   }
    // } else {
    //   if (total - discount > 0) {
    //     setState(() {
    //       _totalController.text =
    //           (total - discount).toStringAsFixed(2).toString();
    //     });
    //   } else {
    //     setState(() {
    //       _totalController.text = total.toStringAsFixed(2).toString();
    //     });
    //   }
    // }
    // print('total  = ' + total.toString());
  }
}

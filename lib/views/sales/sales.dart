import 'dart:isolate';
import 'dart:math';
import 'dart:ui';

import 'package:birou/controllers/contactsController.dart';
import 'package:birou/controllers/dashboardController.dart';
import 'package:birou/controllers/deliveryNotesController.dart';
import 'package:birou/controllers/estimateController.dart';
import 'package:birou/controllers/invoiceController.dart';
import 'package:birou/controllers/paymentController.dart';
import 'package:birou/models/company.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/views/dashboard/home.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:birou/views/sales/delivery/addDelivery/add.dart';
import 'package:birou/views/sales/delivery/addDelivery/deliverySetting.dart';
import 'package:birou/views/sales/delivery/editDelivery/deliverySettingUpdate.dart';
import 'package:birou/views/sales/delivery/editDelivery/edit.dart';
import 'package:birou/views/sales/estimate/addEstimate/add.dart';
import 'package:birou/views/sales/estimate/addEstimate/estimateSettings.dart';
import 'package:birou/views/sales/estimate/editEstimate/edit.dart';
import 'package:birou/views/sales/estimate/editEstimate/estimateSettingUpdate.dart';
import 'package:birou/views/sales/invoice/addInvoice/add.dart';
import 'package:birou/views/sales/invoice/addInvoice/invoiceSetting.dart';
import 'package:birou/views/sales/invoice/editInvoice/edit.dart';
import 'package:birou/views/sales/invoice/editInvoice/invoiceSettingUpdate.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:bubbled_navigation_bar/bubbled_navigation_bar.dart';
import 'package:clay_containers/clay_containers.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ionicons/ionicons.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Sales extends StatefulWidget {
  @override
  String companyId;
  final List<Company> companies;
  bool estimate;
  bool invoice;
  bool delivery;

  Sales(
      {Key key,
      @required this.companies,
      @required this.companyId,
      @required this.estimate,
      @required this.invoice,
      @required this.delivery});

  _SalesState createState() => _SalesState();
}

class _SalesState extends State<Sales> {
  @override
  String full_name;

  bool estimatesList = true;
  bool invoicesList = false;
  bool deliveryList = false;

  Future estimates;
  Future invoices;
  Future deliveries;

  int itemCount = 4;
  double listBuilderHeightEstimate = 350.0;
  double listBuilderHeightInvoice = 350.0;
  double listBuilderHeightDelivery = 350.0;

  final spinkit = SpinKitFadingCircle(
    color: Colors.blueAccent,
    size: 50.0,
  );

  List<Condition> conditions = [
    Condition("Update", 1),
    Condition("Delete", 2),
    Condition("Download", 3),
    Condition("Invoice", 4),
  ];

  getUserName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("full_name");
  }

  int progress = 0;
  ReceivePort receivePort = ReceivePort();

  @override
  void initState() {
    setState(() {
      estimatesList = widget.estimate;
      invoicesList = widget.invoice;
      deliveryList = widget.delivery;
    });

    IsolateNameServer.registerPortWithName(
        receivePort.sendPort, 'downloadingPdf');

    receivePort.listen((message) {
      setState(() {
        progress = message;
      });
    });

    FlutterDownloader.registerCallback(downloadCallback);

    estimates = getEstimates(widget.companyId);
    invoices = getInvoices(widget.companyId);
    deliveries = getDeliveries(widget.companyId);

    getUserName().then((result) {
      setState(() {
        full_name = result;
      });
    });

    super.initState();
  }

  static downloadCallback(id, status, progress) {
    SendPort sendPort = IsolateNameServer.lookupPortByName('downloadingPdf');
    sendPort.send(progress);
  }

  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Color.fromRGBO(245, 246, 252, 1),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(18, 15, 18, 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => MainMenu(
                                  companies: widget.companies,
                                )));
                      },
                      child: Container(
                        width: 18,
                        height: 18,
                        child: Image.asset(
                          "assets/images/menu.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 30,
                        child: ClipOval(
                          child: Image.asset(
                            'assets/images/person.png',
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: Text(
                  AppLocalizations.of(context).sales,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: width / 15,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(18.0, 10, 18, 10),
                child: Center(
                  child: Container(
                    height: height / 12.5,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Card(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          FlatButton(
                              onPressed: () {
                                setState(() {
                                  estimatesList = true;
                                  invoicesList = false;
                                  deliveryList = false;
                                });
                              },
                              child: Container(
                                  child: Center(
                                child: Text(
                                  AppLocalizations.of(context).estimates,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.normal,
                                      color: estimatesList == true
                                          ? Colors.grey
                                          : Colors.black),
                                ),
                              ))),
                          FlatButton(
                              onPressed: () {
                                setState(() {
                                  estimatesList = false;
                                  invoicesList = true;
                                  deliveryList = false;
                                });
                              },
                              child: Container(
                                  child: Center(
                                child: Text(AppLocalizations.of(context).invoices,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.normal,
                                        color: invoicesList == true
                                            ? Colors.grey
                                            : Colors.black)),
                              ))),
                          Expanded(
                            child: FlatButton(
                                onPressed: () {
                                  setState(() {
                                    estimatesList = false;
                                    invoicesList = false;
                                    deliveryList = true;
                                  });
                                },
                                child: Container(
                                    child: Center(
                                  child: Text(AppLocalizations.of(context).deliveries,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.normal,
                                          color: deliveryList == true
                                              ? Colors.grey
                                              : Colors.black)),
                                ))),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 30.0, left: 18.0, right: 18.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      estimatesList == true
                          ? AppLocalizations.of(context).synthesis_estimates
                          : invoicesList == true
                              ? AppLocalizations.of(context).synthesis_invoices
                              : AppLocalizations.of(context)
                                  .synthesis_deliveries,
                      style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                    ),
                    InkWell(
                      onTap: () {
                        if (estimatesList == true) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  AddEstimate(
                                    companyId: widget.companyId,
                                    companies: widget.companies,
                                  )));
                        } else if (invoicesList == true) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  AddInvoice(
                                    companyId: widget.companyId,
                                    companies: widget.companies,
                                    // companies: widget.companies,
                                  )));
                        } else {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  AddDelivery(
                                    companyId: widget.companyId,
                                    companies: widget.companies,
                                    // companies: widget.companies,
                                  )));
                        }
                      },
                      child: Icon(
                        Icons.add,
                        size: width / 14,
                        color: Colors.grey[700],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                child: Center(
                  child: Container(
                    height: estimatesList
                        ? listBuilderHeightEstimate
                        : invoicesList
                            ? listBuilderHeightInvoice
                            : listBuilderHeightDelivery,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Card(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                FutureBuilder(
                                  future: deliveryList == true
                                      ? deliveries
                                      : estimatesList == true
                                          ? estimates
                                          : invoices,
                                  builder: (BuildContext context,
                                      AsyncSnapshot snapshot) {
                                    if (snapshot.connectionState ==
                                        ConnectionState.waiting) {
                                      return Container(
                                        height: width * 0.5,
                                        alignment: Alignment.center,
                                        child: Center(
                                          child: spinkit,
                                        ),
                                      );
                                    } else {
                                      return Expanded(
                                        flex: 1,
                                        child: ListView.builder(
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount: snapshot.data.length > 3
                                              ? itemCount
                                              : snapshot.data.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Client(
                                                snapshot.data[index].nb,
                                                snapshot.data[index].date
                                                    .substring(0, 10),
                                                (index + 1).toString(),
                                                snapshot.data[index].total,
                                                snapshot.data[index].status,
                                                snapshot.data[index].id);
                                          },
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                itemCount = itemCount + 2;
                                if (estimatesList) {
                                  listBuilderHeightEstimate =
                                      listBuilderHeightEstimate + 150;
                                } else if (invoicesList) {
                                  listBuilderHeightInvoice =
                                      listBuilderHeightInvoice + 150;
                                } else {
                                  listBuilderHeightDelivery =
                                      listBuilderHeightDelivery + 150;
                                }
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 8, 20, 8),
                              child: Text(
                                AppLocalizations.of(context).load_more,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.blueAccent),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Client(
      String name, company, index, total, int status, String hashed_id) {
    return Container(
        child: ListTile(
      leading: Text(
        "#" + index,
        style: TextStyle(fontSize: 14),
      ),
      title: Text(
        name,
        style: TextStyle(
            color: Colors.blueAccent,
            fontSize: 14,
            fontWeight: FontWeight.normal),
      ),
      subtitle: Text(
        company != null ? company : '',
        style: TextStyle(
          color: Colors.grey[700],
          fontSize: 11,
        ),
      ),
      trailing: Container(
        width: 160,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  total + ' TND',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                ),
                PopupMenuButton<int>(
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: Colors.blueAccent,
                    size: 35,
                  ),
                  onSelected: (int result) {
                    if (result == 1) {
                      if (estimatesList == true) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                EditEstimate(
                                  id: hashed_id,
                                  companyId: widget.companyId,
                                  companies: widget.companies,
                                )));
                      } else if (invoicesList == true) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                EditInvoice(
                                  id: hashed_id,
                                  companyId: widget.companyId,
                                  companies: widget.companies,
                                )));
                      } else {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                EditDelivery(
                                  id: hashed_id,
                                  companyId: widget.companyId,
                                  companies: widget.companies,
                                )));
                      }
                    } else if (result == 2) {
                      if (deliveryList == true) {
                        deleteDelivery(widget.companyId, hashed_id)
                            .then((result) {
                          print(result);
                          if (result['status']['code'] != 200) {
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                                    builder: (BuildContext context) => Home(
                                          companies: widget.companies,
                                          companyId: widget.companyId,
                                          currentScreen: Sales(
                                            companies: widget.companies,
                                            companyId: widget.companyId,
                                            invoice: false,
                                            estimate: false,
                                            delivery: true,
                                          ),
                                        )));
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 0,
                                    text1: 'Error',
                                    text2: result['message'],
                                  );
                                });
                          } else {
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                                    builder: (BuildContext context) => Home(
                                          companies: widget.companies,
                                          companyId: widget.companyId,
                                          currentScreen: Sales(
                                            companies: widget.companies,
                                            companyId: widget.companyId,
                                            invoice: false,
                                            estimate: false,
                                            delivery: true,
                                          ),
                                        )));
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 1,
                                    text1: 'Success',
                                    text2: result['message'],
                                  );
                                });
                          }
                        });
                      } else if (estimatesList == true) {
                        deleteEstimate(widget.companyId, hashed_id)
                            .then((result) {
                          print(result);
                          if (result['status']['code'] != 200) {
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                                    builder: (BuildContext context) => Home(
                                          companies: widget.companies,
                                          companyId: widget.companyId,
                                          currentScreen: Sales(
                                            companies: widget.companies,
                                            companyId: widget.companyId,
                                            invoice: false,
                                            estimate: true,
                                            delivery: false,
                                          ),
                                        )));
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 0,
                                    text1: 'Error',
                                    text2: result['message'],
                                  );
                                });
                          } else {
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                                    builder: (BuildContext context) => Home(
                                          companies: widget.companies,
                                          companyId: widget.companyId,
                                          currentScreen: Sales(
                                            companies: widget.companies,
                                            companyId: widget.companyId,
                                            invoice: false,
                                            estimate: true,
                                            delivery: false,
                                          ),
                                        )));
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 1,
                                    text1: 'Success',
                                    text2: result['message'],
                                  );
                                });
                          }
                        });
                      } else {
                        deleteInvoice(widget.companyId, hashed_id)
                            .then((result) {
                          print(result);
                          if (result['status']['code'] != 200) {
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                                    builder: (BuildContext context) => Home(
                                          companies: widget.companies,
                                          companyId: widget.companyId,
                                          currentScreen: Sales(
                                            companies: widget.companies,
                                            companyId: widget.companyId,
                                            invoice: true,
                                            estimate: false,
                                            delivery: false,
                                          ),
                                        )));
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 0,
                                    text1: 'Error',
                                    text2: result['message'],
                                  );
                                });
                          } else {
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                                    builder: (BuildContext context) => Home(
                                          companies: widget.companies,
                                          companyId: widget.companyId,
                                          currentScreen: Sales(
                                            companies: widget.companies,
                                            companyId: widget.companyId,
                                            invoice: true,
                                            estimate: false,
                                            delivery: false,
                                          ),
                                        )));
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 1,
                                    text1: 'Success',
                                    text2: result['message'],
                                  );
                                });
                          }
                        });
                      }
                    } else if (result == 3) {
                      if (deliveryList == true) {
                        downloadDelivery(widget.companyId, hashed_id, name);
                      } else if (estimatesList == true) {
                        downloadEstimate(widget.companyId, hashed_id, name);
                      } else if (invoicesList == true) {
                        downloadInvoice(widget.companyId, hashed_id, name);
                      }
                    } else if (result == 4) {
                      if (deliveryList == true) {
                        deliveryInvoice(widget.companyId, hashed_id)
                            .then((result) {
                          print(result);
                          if (result['status']['code'] != 200) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 0,
                                    text1: 'Error',
                                    text2: result['status']['message'],
                                  );
                                });
                          } else {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    InvoiceSettingsUpdate(
                                      id: result['data']['invoiceId'],
                                      companyId: widget.companyId,
                                      companies: widget.companies,
                                    )));
                          }
                        });
                      } else if (estimatesList == true) {
                        estimateInvoice(widget.companyId, hashed_id)
                            .then((result) {
                          print(result);
                          if (result['status']['code'] != 200) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 0,
                                    text1: 'Error',
                                    text2: result['status']['message'],
                                  );
                                });
                          } else {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    InvoiceSettingsUpdate(
                                      id: result['data']['invoiceId'],
                                      companyId: widget.companyId,
                                      companies: widget.companies,
                                    )));
                          }
                        });
                      }
                    }
                  },
                  itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
                    PopupMenuItem<int>(
                      value: conditions[0].condition,
                      child: Text(AppLocalizations.of(context).update),
                    ),
                    PopupMenuItem<int>(
                      value: conditions[1].condition,
                      child: Text(AppLocalizations.of(context).delete),
                    ),
                    PopupMenuItem<int>(
                      value: conditions[2].condition,
                      child: Text(AppLocalizations.of(context).download),
                    ),
                    PopupMenuItem<int>(
                      value: conditions[3].condition,
                      child: Text(AppLocalizations.of(context).invoice),
                    )
                  ],
                )
              ],
            ),
            Expanded(
              child: Container(
                  decoration: BoxDecoration(
                      color: status == 0
                          ? Colors.amberAccent
                          : status == 1
                              ? Colors.blueAccent
                              : Colors.greenAccent,
                      borderRadius: BorderRadius.circular(10)),
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(
                      status == 0
                          ? "Draft"
                          : status == 1
                              ? "Sent"
                              : "Invoiced",
                      style: TextStyle(color: Colors.white, fontSize: 10),
                    ),
                  )),
            ),
          ],
        ),
      ),
    ));
  }
}

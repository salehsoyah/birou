import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/companyController.dart';
import 'package:birou/controllers/contactsController.dart';
import 'package:birou/controllers/deliveryNotesController.dart';
import 'package:birou/controllers/estimateController.dart';
import 'package:birou/controllers/stockController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/controllers/voucherController.dart';
import 'package:birou/models/bank.dart';
import 'package:birou/models/client.dart';
import 'package:birou/models/condition.dart';
import 'package:birou/models/country.dart';
import 'package:birou/models/device.dart';
import 'package:birou/models/input.dart';
import 'package:birou/models/item.dart';
import 'package:birou/models/language.dart';
import 'package:birou/models/tax.dart';
import 'package:birou/models/inputAdd.dart';

import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/dialog/alert.dart';
import 'package:birou/views/sales/delivery/addDelivery/form.dart';
import 'package:birou/views/subscriptions/last_step.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:date_field/date_field.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddExit extends StatefulWidget {
  @override
  bool conditions;
  bool stamp;
  bool billing;
  bool delivery;
  bool bank;
  String bankId;
  List<String> inputs;
  String language;
  int currency;
  String companyId;

  AddExit(
      {Key key,
      @required this.conditions,
      @required this.stamp,
      @required this.billing,
      @required this.delivery,
      @required this.bank,
      @required this.bankId,
      @required this.inputs,
      @required this.language,
      @required this.currency,
      @required this.companyId});

  _AddExitState createState() => _AddExitState();
}

class _AddExitState extends State<AddExit> {
  GlobalKey<FormState> _form = GlobalKey<FormState>();

  bool obscureText = true;
  String mobile_token;
  List<Country> countries = [];
  String country;
  String countryText;
  bool btnPressed = false;
  final _firebaseMessaging = FirebaseMessaging.instance;
  Future listCountries;
  Future companyTaxes;
  Future banksList;
  Future clientList;

  List<Tax> taxes = [
    Tax("Hors Taxes", 1, "", 0),
    Tax("Taxes incluses", 2, "", 0),
  ];

  String pdfLanguage;
  String pdfText;
  List<Language> languages = [
    Language("Français", "fr"),
    Language("English", "en"),
    Language("Arabic", "ar")
  ];

  int taxPicked;
  String taxText;

  List<Clientt> clients = [];
  String clientText;
  String client;

  String conditionText;
  List<Condition> conditions = [
    Condition("Oui", 1),
    Condition("Non", 0),
  ];

  DateTime date;
  DateTime dueDate;

  bool condition = false;
  bool stamp = false;
  bool billing = false;
  bool delivery = false;
  bool bank = false;
  final _deliveryNumberController = TextEditingController();
  final _referenceController = TextEditingController();
  final _generalConditionsController = TextEditingController();
  final _notesController = TextEditingController();
  final _discountController = TextEditingController();

  List<ItemForm> items = [];
  List<Bank> banks = [];

  var articles;
  List<String> inputsAdd = [];

  String bankText;
  String bankId;

  List<Input> inputs = [];

  int choice;

  void initState() {
    companyTaxes = companyTax();
    banksList = bankList(widget.companyId);
    clientList = clientsList(widget.companyId);

    clientsList(widget.companyId).then((value) {
      setState(() {
        clients = value;
      });
    });

    bankList(widget.companyId).then((value) {
      setState(() {
        banks = value;
        bankId = value[0].id;
        bankText = value[0].title;
      });
    });

    nextVoucher(widget.companyId).then((value) {
      print(value);
      setState(() {
        _deliveryNumberController.text = value['data']['nextExitVoucherNumber'];
      });
    });

    companyInputs().then((value) {
      setState(() {
        inputs = value;
      });
    });

    setState(() {
      choice = 0;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (BuildContext context) => Login()));
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              'New Exit voucher',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 2,
            ),
            Icon(
              Icons.format_list_bulleted_rounded,
              color: Colors.blue,
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _form,
          child: Container(
            color: Color.fromRGBO(245, 246, 252, 1),
            child: Column(
              children: [
                SizedBox(
                  height: width / 11,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: width / 11,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          choice = 0;
                          print(choice);
                        });
                      },
                      child: Container(
                        width: width / 6,
                        height: width / 6,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.blue[100],
                                spreadRadius: 0.5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 1), // changes position of shadow
                              ),
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 9.0, bottom: 2.0, top: 2.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Icon(
                                    Icons.circle,
                                    color: choice == 0
                                        ? Colors.green
                                        : Colors.grey[300],
                                    size: 7,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Image.asset(
                                "assets/images/icon10.png",
                                fit: BoxFit.contain,
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Text('Draft')
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: width / 15,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          choice = 1;
                          print(choice);
                        });
                      },
                      child: Container(
                        width: width / 6,
                        height: width / 6,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.blue[100],
                                spreadRadius: 0.5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 1), // changes position of shadow
                              ),
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 9.0, bottom: 2.0, top: 2.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Icon(
                                    Icons.circle,
                                    color: choice == 1
                                        ? Colors.green
                                        : Colors.grey[300],
                                    size: 7,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Image.asset(
                                "assets/images/icon11.png",
                                fit: BoxFit.contain,
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Text('Send')
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        DateTimeFormField(
                          decoration: const InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            hintStyle: TextStyle(color: Colors.black45),
                            errorStyle: TextStyle(color: Colors.redAccent),
                            suffixIcon: Icon(Icons.event_note),
                            labelText: 'Date',
                          ),
                          firstDate:
                              DateTime.now().add(const Duration(days: 10)),
                          initialDate:
                              DateTime.now().add(const Duration(days: 10)),
                          autovalidateMode: AutovalidateMode.always,
                          validator: (DateTime e) =>
                              (e?.day == null) ? 'Please enter date' : null,
                          onDateSelected: (DateTime value) {
                            print(value);
                            setState(() {
                              date = value;
                            });
                          },
                        ),
                        SizedBox(
                          height: width / 50,
                        )
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: width / 25,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      'Exit Voucher N°',
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                    Container(
                      width: width * 0.38,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.blue[100],
                              spreadRadius: 0.5,
                              blurRadius: 7,
                              offset:
                                  Offset(0, 1), // changes position of shadow
                            ),
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          validator: (val) => val.length < 1
                              ? 'Exit voucher number is required'
                              : null,
                          controller: _deliveryNumberController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'EV-2021-',
                              hintStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: width / 27,
                ),

                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  'Client',
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  clientText != null
                                      ? clientText
                                      : "Foulen ben foulen",
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && client == null
                                    ? Text(
                                        "Please choose client",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 12),
                                      )
                                    : Container()
                              ],
                            ),
                            FutureBuilder(
                              future: clientList,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (!snapshot.hasData) {
                                  return Text("loading...");
                                }
                                return PopupMenuButton<String>(
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.blueAccent,
                                    size: 35,
                                  ),
                                  itemBuilder: (context) => snapshot.data
                                      .map<PopupMenuItem<String>>(
                                          (value) => PopupMenuItem<String>(
                                                value: value.id,
                                                child: Text(
                                                  value.display_name,
                                                ),
                                              ))
                                      .toList(),
                                  onSelected: (value) {
                                    setState(() {
                                      client = value;
                                    });

                                    for (var i in clients) {
                                      if (value == i.id) {
                                        setState(() {
                                          clientText = i.display_name;
                                        });
                                      }
                                    }
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          controller: _referenceController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Reference Number',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: width / 11,
                ),

                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: width / 39,
                                ),
                                Text(
                                  'Item Price Are',
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.grey[500]),
                                ),
                                SizedBox(
                                  height: width / 70,
                                ),
                                Text(
                                  taxText != null ? taxText : "Choose tax",
                                  style: TextStyle(fontSize: 13),
                                ),
                                btnPressed && taxPicked == null
                                    ? Text(
                                        "Choose tax",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 12),
                                      )
                                    : Container()
                              ],
                            ),
                            PopupMenuButton<int>(
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.blueAccent,
                                size: 35,
                              ),
                              onSelected: (int result) {
                                setState(() {
                                  taxPicked = result;
                                });

                                for (var i in taxes) {
                                  if (i.rate == taxPicked) {
                                    print(i.title);
                                    setState(() {
                                      taxText = i.title;
                                    });
                                  }
                                }
                              },
                              itemBuilder: (BuildContext context) =>
                                  <PopupMenuEntry<int>>[
                                PopupMenuItem<int>(
                                  value: taxes[0].rate,
                                  child: Text(taxes[0].title),
                                ),
                                PopupMenuItem<int>(
                                  value: taxes[1].rate,
                                  child: Text(taxes[1].title),
                                ),
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: width / 11,
                ),

                Padding(
                  padding:
                      EdgeInsets.only(top: 0.0, left: width / 11, right: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Items',
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                      GestureDetector(
                        onTap: () {
                          onAddForm();
                        },
                        child: Icon(
                          Icons.add,
                          size: width / 14,
                          color: Colors.grey[700],
                        ),
                      ),
                    ],
                  ),
                ),
                btnPressed == true && items.length < 1
                    ? Padding(
                        padding: EdgeInsets.only(
                            top: 0.0, left: width / 11, right: 30.0),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Add Item(s)",
                              style: TextStyle(color: Colors.red, fontSize: 12),
                            )),
                      )
                    : Container(),
                SizedBox(
                  height: width / 27,
                ),

                items.length > 0
                    ? ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        addAutomaticKeepAlives: true,
                        itemCount: items.length,
                        itemBuilder: (_, i) => Padding(
                          padding: const EdgeInsets.only(left: 15.0, right: 15),
                          child: items[i],
                        ),
                      )
                    : Container(),

                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Subtotal',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: _discountController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Discount',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Taxes',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 1.0,
                        ),
                        TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Total',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        TextFormField(
                          controller: _generalConditionsController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'General Conditions',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: width / 11,
                ),
                Container(
                  width: width * 0.83,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blue[100],
                          spreadRadius: 0.5,
                          blurRadius: 7,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: width / 39,
                        ),
                        TextFormField(
                          maxLines: 6,
                          controller: _notesController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Notes',
                              hintStyle: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal)),
                        ),
                        SizedBox(
                          height: width / 15,
                        )
                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: width / 11,
                ),

                SizedBox(
                  height: MediaQuery.of(context).size.height / 5.6,
                ),
                // ignore: deprecated_member_use
                RaisedButton(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    onPressed: () {
                      setState(() {
                        btnPressed = true;
                      });
                      onSave();
                      print(articles);
                      if (_form.currentState.validate() &&
                          taxPicked != null &&
                          client != null &&
                          articles != null) {
                        newVoucher(
                                date.toString().substring(0, 10),
                                _deliveryNumberController.text,
                                _generalConditionsController.text,
                                _referenceController.text,
                                _notesController.text,
                                _discountController.text == ''
                                    ? null
                                    : double.parse(_discountController.text),
                                taxPicked,
                                client,
                                widget.conditions == true ? 1 : 0,
                                widget.stamp == true ? 1 : 0,
                                widget.billing == true ? 1 : 0,
                                widget.delivery == true ? 1 : 0,
                                widget.bank == true ? 1 : 0,
                                widget.bankId,
                                1,
                                choice,
                                widget.currency,
                                widget.language,
                                articles,
                                widget.inputs,
                                widget.companyId)
                            .then((value) {
                          if (value['status']['code'] != 200) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 0,
                                    text1: 'Error',
                                    text2: value['status']['message'],
                                  );
                                });
                          } else {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogScreen(
                                    image: 1,
                                    text1: 'Success',
                                    text2: value['status']['message'],
                                  );
                                });
                          }
                          print(value);
                        });
                      }
                    },
                    // padding: const EdgeInsets.all(0.0),
                    child: Container(
                      color: Color.fromRGBO(55, 86, 223, 1),
                      width: width * 0.78,
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                      child: Row(
                        children: [
                          Spacer(),
                          Container(
                              margin: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)
                                    .finish
                                    .toUpperCase(),
                                style: TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white),
                              )),
                          Spacer(),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Image(
                                image:
                                    AssetImage("assets/images/arrowGray.png"),
                              )),
                        ],
                      ),
                    ),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0))),
                SizedBox(
                  height: 10,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onDelete(Item item) {
    setState(() {
      var find = items.firstWhere(
        (it) => it.item == item,
        orElse: () => null,
      );
      if (find != null) items.removeAt(items.indexOf(find));
    });
  }

  void onAddForm() {
    setState(() {
      var item = Item();
      items.add(ItemForm(
        item: item,
        onDelete: () => onDelete(item),
      ));
    });
  }

  void onSave() {
    if (items.length > 0) {
      var allValid = true;
      items.forEach((form) => {allValid = allValid && form.isValid()});
      if (allValid) {
        var data = items.map((it) => jsonEncode(it.item)).toList();
        setState(() {
          articles = data;
        });
      }
    }
  }
}

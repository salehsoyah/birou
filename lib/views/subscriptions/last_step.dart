import 'dart:io';

import 'package:birou/controllers/authController.dart';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/models/device.dart';
import 'package:birou/views/auth/emailValidation.dart';
import 'package:birou/views/auth/login.dart';
import 'package:birou/views/subscriptions/webviewpayment.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LastStep extends StatefulWidget {
  @override
  _LastStepState createState() => _LastStepState();

  final int type;
  final String choice;
  final String company;
  final String address;
  final String state;
  final String zipCode;
  final String country;

  LastStep(
      {Key key,
      @required this.type,
      this.choice,
      this.company,
      this.address,
      this.state,
      this.zipCode,
      this.country})
      : super(key: key);
}

class _LastStepState extends State<LastStep> {

  final _emailController = TextEditingController();
  String finalChoice;
  String plan_month;
  String plan_year;
  bool obscureText = true;
  String mobile_token;
  int _selection;
  String quantity = "1";
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double height =
        MediaQuery.of(context).size.height - padding.top - padding.bottom;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (BuildContext context) => Login()));
          },
          child: Icon(
            Icons.arrow_back,
            color: Color.fromRGBO(55, 86, 223, 1),
          ),
        ),
        title: Row(
          children: [
           Spacer(flex: 1,),
            Text(
              "Subscription Plans",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(flex: 2,),
            Icon(
              Icons.logout,
              color: Colors.blueAccent,
            ),
          ],
        ),
      ),
      body: loading == false ? SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(245, 246, 252, 1),
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              SizedBox(
                height: width / 11,
              ),
              Row(
                children: [
                  SizedBox(
                    width: width / 11.5,
                  ),
                  Text(
                    "Last Step",
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey[600],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.84,
                height: width * 0.88,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                quantity,
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                "Month / Year",
                                style: TextStyle(fontSize: 13),
                              )
                            ],
                          ),
                          PopupMenuButton<int>(
                            icon: Icon(
                              Icons.arrow_drop_down,
                              color: Colors.blueAccent,
                              size: 35,
                            ),
                            onSelected: (int result) {
                              setState(() {
                                _selection = result;
                                quantity = result.toString();
                              });
                            },
                            itemBuilder: (BuildContext context) =>
                            <PopupMenuEntry<int>>[
                              const PopupMenuItem<int>(
                                value: 1,
                                child: Text('1'),
                              ),
                              const PopupMenuItem<int>(
                                value: 2,
                                child: Text('2'),
                              ),
                              const PopupMenuItem<int>(
                                value: 3,
                                child: Text('3'),
                              ),
                              const PopupMenuItem<int>(
                              value: 4,
                              child: Text('4'),
                            ),
                              const PopupMenuItem<int>(
                              value: 5,
                              child: Text('5'),
                            ),
                              const PopupMenuItem<int>(
                                value: 6,
                                child: Text('6'),
                              ),
                              const PopupMenuItem<int>(
                                value: 7,
                                child: Text('7'),
                              ),
                              const PopupMenuItem<int>(
                                value: 8,
                                child: Text('8'),
                              ),
                              const PopupMenuItem<int>(
                                value: 9,
                                child: Text('9'),
                              ),
                              const PopupMenuItem<int>(
                                value: 10,
                                child: Text('10'),
                              ),
                              const PopupMenuItem<int>(
                                value: 11,
                                child: Text('11'),
                              ),
                              const PopupMenuItem<int>(
                                value: 12,
                                child: Text('12'),
                              ),
                            ],
                          )
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                "VAT",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                "71.84 DT",
                                style: TextStyle(fontSize: 13),
                              )
                            ],
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                "Sub-Total",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                "378.09 DT",
                                style: TextStyle(fontSize: 13),
                              )
                            ],
                          ),
                        ],
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      TextFormField(
                        controller: _emailController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Discount',
                            hintStyle: TextStyle(
                                color: Colors.grey[500],
                                fontSize: 17,
                                fontWeight: FontWeight.normal)),
                      ),
                      Divider(
                        color: Colors.grey[300],
                        thickness: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: width / 39,
                              ),
                              Text(
                                "Total",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.grey[500]),
                              ),
                              SizedBox(
                                height: width / 70,
                              ),
                              Text(
                                "449.93 DT",
                                style: TextStyle(fontSize: 13),
                              )
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: width / 30,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: width / 15,
              ),
              Center(
                child: Text(
                  "You will be redirected to our payment processor\nPayMee on which you can pay your subscription \nat Birou Platform using your credit card.",
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey[600],
                  ),
                ),
              ),
              SizedBox(
                height: width / 7,
              ),
              // ignore: deprecated_member_use
              RaisedButton(
                  color: Color.fromRGBO(55, 86, 223, 1),
                  onPressed: () async {

                    SharedPreferences sharedPreferences =
                        await SharedPreferences.getInstance();

                    //plan_month
                    if(widget.choice == "Free" && widget.type == 0)
                      {
                        setState(() {
                          plan_month = "Dl";
                          plan_year ="";
                        });
                      }
                    else if(widget.choice == "Startup" && widget.type == 0){
                      setState(() {
                        plan_month = "Er";
                        plan_year ="";
                      });
                    }
                    else if (widget.choice == "Professional" && widget.type == 0){
                      setState(() {
                        plan_month = "GZ";
                        plan_year ="";
                      });
                    }
                    //

                    //plan_year
                    if(widget.choice == "Free" && widget.type == 1)
                    {
                      setState(() {
                        plan_year = "Dl";
                        plan_month = "";
                      });
                    }
                    else if(widget.choice == "Startup" && widget.type == 1){
                      setState(() {
                        plan_year = "Er";
                        plan_month = "";
                      });
                    }
                    else if (widget.choice == "Professional" && widget.type == 1){
                      setState(() {
                        plan_year = "GZ";
                        plan_month = "";
                      });
                    }
                    //

                    String new_old ;

                    if(sharedPreferences.getString("new_old") != ""){
                      setState(() {
                        new_old = "Dl";
                      });
                    }
                    else {
                      setState(() {
                        new_old = "Er";
                      });
                    }

                    mobile_token = sharedPreferences.getString("access_token");


                    print("access token = $mobile_token");
                    print(widget.type.toString());
                    print("quantity : $quantity");
                    print("operation : $new_old");
                    print("plan_month : $plan_month");
                    print("plan_year : $plan_year");
                    print(widget.company);
                    print(widget.address);
                    print(widget.state);
                    print(widget.zipCode);
                    print(widget.country);

                    setState(() {
                      loading = true;
                    });
                    subscriptionPlan(
                            widget.type,
                            int.parse(quantity),
                            new_old,
                            plan_month,
                            plan_year,
                            "-1",
                        widget.company,
                        widget.address,
                        widget.state,
                        widget.zipCode,
                        widget.country)
                        .then((result) {
                      setState(() {
                        loading = false;
                      });
                      print(result);
                      String token = result['data']['token'];
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => MyWebView(
                                title: "Payment",
                                selectedUrl:
                                    "https://sandbox.paymee.tn/gateway/$token",
                                token: token,
                              )));
                    });
                  },
                  child: Container(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    width: width * 0.8,
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        Container(
                            margin: const EdgeInsets.only(left: 10.0),
                            child: Text(
                              "go to payment".toUpperCase(),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            )),
                        Spacer(),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: AssetImage("assets/images/arrowGray.png"),
                            )),
                      ],
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0))),
            ],
          ),
        ),
      ) : Container(
        height: height,
        child: SpinKitFadingCircle(
          itemBuilder: (BuildContext context, int index) {
            return DecoratedBox(
              decoration: BoxDecoration(
                color: index.isEven ? Colors.yellow : Colors.blue,
              ),
            );
          },
        ),
      ),
    );
  }
}


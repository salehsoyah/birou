import 'dart:convert';
import 'dart:io';
import 'package:birou/controllers/authController.dart';
import 'package:birou/models/device.dart';
import 'package:birou/views/auth/registration.dart';
import 'package:birou/views/subscriptions/billing.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlanChoice extends StatefulWidget {
  @override
  _PlanChoiceState createState() => _PlanChoiceState();
}

class _PlanChoiceState extends State<PlanChoice> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  final _firebaseMessaging = FirebaseMessaging.instance;
  MenuOption _selection;
  bool free = false;
  bool startup = false;
  bool professional = false;

  bool buttonPressed = false;

  String mobile_token;

  String dropdowntitle = 'Annually';

  String priceStartup = '259.90';
  String priceProfessional = '449.93';

  int type;
  String choice;

  @override
  void initState() {
    _firebaseMessaging.getToken().then((String token) async {
      assert(token != null);
      setState(() {
        mobile_token = token;
      });
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color.fromRGBO(245, 246, 252, 1),
        centerTitle: true,
        title: Row(
          children: [
            Spacer(
              flex: 3,
            ),
            Text(
              "Subscription Plans",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
            ),
            Spacer(
              flex: 3,
            ),
            Icon(
              Icons.logout,
              color: Colors.blueAccent,
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(245, 246, 252, 1),
          height: MediaQuery.of(context).size.height * 1.05,
          child: Column(
            children: [
              SizedBox(
                height: width / 11,
              ),
              Center(
                  child: Text(
                "Please choose the plan that fulfills your business\nneeds.",
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey[600],
                ),
              )),
              Container(
                margin: EdgeInsets.only(right: 20, top: 10),
                child: Row(
                  children: [
                    Spacer(),
                    Container(
                      alignment: Alignment.bottomLeft,
                      width: width * 0.63,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: ListTile(
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 8),
                          child: Text(dropdowntitle),
                        ),
                        subtitle: Text(
                          "Subscribe annually or monthly",
                          style: TextStyle(fontSize: 12),
                        ),
                        trailing: PopupMenuButton<MenuOption>(
                          icon: Icon(
                            Icons.arrow_drop_down,
                            color: Colors.blueAccent,
                            size: 35,
                          ),
                          onSelected: (MenuOption result) {
                            setState(() {
                              _selection = result;
                            });
                            if (result == MenuOption.annually) {
                              setState(() {
                                dropdowntitle = "Annually";
                                priceStartup = "259.90";
                                priceProfessional = "449.93";
                              });
                            } else {
                              setState(() {
                                dropdowntitle = "Monthly";
                                priceStartup = "25.99";
                                priceProfessional = "49.99";
                              });
                            }
                          },
                          itemBuilder: (BuildContext context) =>
                              <PopupMenuEntry<MenuOption>>[
                            const PopupMenuItem<MenuOption>(
                              value: MenuOption.monthly,
                              child: Text('Monthly'),
                            ),
                            const PopupMenuItem<MenuOption>(
                              value: MenuOption.annually,
                              child: Text('Annually'),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.85,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(22),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(left: 2, top: 15, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ListTile(
                        leading: RawMaterialButton(
                          onPressed: () {
                            if (free == false &&
                                startup == false &&
                                professional == false) {
                              setState(() {
                                free = true;
                              });
                            } else if (free == false &&
                                startup == true &&
                                professional == false) {
                              setState(() {
                                free = true;
                                startup = false;
                              });
                            } else if (free == false &&
                                startup == false &&
                                professional == true) {
                              setState(() {
                                free = true;
                                professional = false;
                              });
                            } else if (free == true &&
                                startup == false &&
                                professional == false) {
                              setState(() {
                                free = false;
                              });
                            }
                          },
                          elevation: 2.0,
                          fillColor: Colors.white,
                          child: free
                              ? Icon(
                                  Icons.check,
                                  size: 35.0,
                                  color: Colors.blueAccent,
                                )
                              : Text(
                                  "",
                                  style: TextStyle(fontSize: 30),
                                ),
                          padding: EdgeInsets.all(15.0),
                          shape: CircleBorder(
                              side: BorderSide(
                                  width: 3, color: Colors.blueAccent)),
                        ),
                        title: Text(
                          "FREE",
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w500,
                              color: Colors.blueAccent),
                        ),
                        subtitle: Text(
                          "0.00 DT / Annually",
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: width / 39,
                      ),
                      Text(
                        "The best plan to try everything at Birou",
                        style: TextStyle(color: Colors.grey[600]),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.85,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(22),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(left: 2, top: 15, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ListTile(
                        leading: RawMaterialButton(
                          onPressed: () {
                            if (free == false &&
                                startup == false &&
                                professional == false) {
                              setState(() {
                                startup = true;
                              });
                            } else if (free == true &&
                                startup == false &&
                                professional == false) {
                              setState(() {
                                startup = true;
                                free = false;
                              });
                            } else if (free == false &&
                                startup == false &&
                                professional == true) {
                              setState(() {
                                startup = true;
                                professional = false;
                              });
                            } else if (free == false &&
                                startup == true &&
                                professional == false) {
                              setState(() {
                                startup = false;
                              });
                            }
                          },
                          elevation: 2.0,
                          fillColor: Colors.white,
                          child: startup
                              ? Icon(
                                  Icons.check,
                                  size: 35.0,
                                  color: Colors.blueAccent,
                                )
                              : Text(
                                  "",
                                  style: TextStyle(fontSize: 30),
                                ),
                          padding: EdgeInsets.all(15.0),
                          shape: CircleBorder(
                              side: BorderSide(
                                  width: 3, color: Colors.blueAccent)),
                        ),
                        title: Text(
                          "STARTUP",
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w500,
                              color: Colors.blueAccent),
                        ),
                        subtitle: Text(
                          "$priceStartup DT / Annually",
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: width / 39,
                      ),
                      Text(
                        "The most popular subscription at Birou",
                        style: TextStyle(color: Colors.grey[600]),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: width / 11,
              ),
              Container(
                width: width * 0.85,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(22),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blue[100],
                        spreadRadius: 0.5,
                        blurRadius: 7,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(left: 2, top: 15, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ListTile(
                        leading: RawMaterialButton(
                          onPressed: () {
                            if (free == false &&
                                startup == false &&
                                professional == false) {
                              setState(() {
                                professional = true;
                              });
                            } else if (free == true &&
                                startup == false &&
                                professional == false) {
                              setState(() {
                                professional = true;
                                free = false;
                              });
                            } else if (free == false &&
                                startup == true &&
                                professional == false) {
                              setState(() {
                                professional = true;
                                startup = false;
                              });
                            } else if (free == false &&
                                startup == false &&
                                professional == true) {
                              setState(() {
                                professional = false;
                              });
                            }
                          },
                          elevation: 2.0,
                          fillColor: Colors.white,
                          child: professional
                              ? Icon(
                                  Icons.check,
                                  size: 35.0,
                                  color: Colors.blueAccent,
                                )
                              : Text(
                                  "",
                                  style: TextStyle(fontSize: 30),
                                ),
                          padding: EdgeInsets.all(15.0),
                          shape: CircleBorder(
                              side: BorderSide(
                                  width: 3, color: Colors.blueAccent)),
                        ),
                        title: Text(
                          "PROFESSIONAL",
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w500,
                              color: Colors.blueAccent),
                        ),
                        subtitle: Text(
                          "$priceProfessional DT / Annually",
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: width / 39,
                      ),
                      Text(
                        "Here, you have everything you may need",
                        style: TextStyle(color: Colors.grey[600]),
                      ),

                    ],
                  ),
                ),
              ),
              SizedBox(
                height: width / 30,
              ),
              (free == false &&
                  startup == false &&
                  professional == false &&
                  buttonPressed == true)
                  ? Text("Please choose a subscription plan",
                  style: TextStyle(color: Colors.red, fontSize: 12))
                  : Container(),
              SizedBox(
                height: width / 11,
              ),
              // ignore: deprecated_member_use
              RaisedButton(
                  color: Color.fromRGBO(55, 86, 223, 1),
                  onPressed: () {
                    setState(() {
                     buttonPressed = true;
                    });

                    if (dropdowntitle == "Annually") {
                      setState(() {
                        type = 1;
                      });
                    } else {
                      setState(() {
                        type = 0;
                      });
                    }
                    if (free == true) {
                      setState(() {
                        choice = "Free";
                      });
                    }
                    if (startup == true) {
                      setState(() {
                        choice = "Startup";
                      });
                    }
                    if (professional == true) {
                      setState(() {
                        choice = "Professional";
                      });
                    }

                    print("type = $type");
                    print("choice = $choice");
                   if(free != false || startup != false || professional != false){
                     Navigator.of(context).pushReplacement(MaterialPageRoute(
                         builder: (BuildContext context) => Billing(
                           type: type,
                           choice: choice,
                         )));
                   }
                  },
                  // padding: const EdgeInsets.all(0.0),
                  child: Container(
                    color: Color.fromRGBO(55, 86, 223, 1),
                    width: width * 0.8,
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        Container(
                            margin: const EdgeInsets.only(left: 10.0),
                            child: Text(
                              "next".toUpperCase(),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            )),
                        Spacer(),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: AssetImage("assets/images/arrowGray.png"),
                            )),
                      ],
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0))),
            ],
          ),
        ),
      ),
    );
  }
}

enum MenuOption { annually, monthly }

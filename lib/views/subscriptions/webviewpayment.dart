import 'dart:async';
import 'package:birou/controllers/subscriptionController.dart';
import 'package:birou/views/menu/mainMenu.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MyWebView extends StatelessWidget {
  final String title;
  final String selectedUrl;
  final String token;
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  MyWebView({
    @required this.token,
    @required this.title,
    @required this.selectedUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: WebView(
          initialUrl: selectedUrl,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
          onPageFinished: (String url) async {
            print('Page finished loading: $url');
            print("el url = $url");
            if (url.contains("/loader")) {
              SharedPreferences sharedPreferences =
                  await SharedPreferences.getInstance();
              subscriptionCallBack("Dl", token, "1").then((result) {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => MainMenu()));
              });
            }
          },
        ));
  }
}
